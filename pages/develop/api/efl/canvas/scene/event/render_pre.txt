~~Title: Efl.Canvas.Scene: render,pre~~

===== Description =====

%%Called when pre render happens%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:scene:event:render_pre:description&nouser&nolink&nodate}}

===== Signature =====

<code>
render,pre;
</code>

===== C information =====

<code c>
EFL_CANVAS_SCENE_EVENT_RENDER_PRE(void)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_scene_event_render_pre(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_SCENE_EVENT_RENDER_PRE, on_efl_canvas_scene_event_render_pre, d);
}

</code>
