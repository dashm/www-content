~~Title: Efl.Canvas.Scene: scene,focus,in~~

===== Description =====

%%Called when scene got focus%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:scene:event:scene_focus_in:description&nouser&nolink&nodate}}

===== Signature =====

<code>
scene,focus,in;
</code>

===== C information =====

<code c>
EFL_CANVAS_SCENE_EVENT_SCENE_FOCUS_IN(void)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_scene_event_scene_focus_in(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_SCENE_EVENT_SCENE_FOCUS_IN, on_efl_canvas_scene_event_scene_focus_in, d);
}

</code>
