~~Title: Efl.Canvas.Scene: device,removed~~

===== Description =====

%%Called when input device was removed%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:scene:event:device_removed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
device,removed @beta: Efl.Input.Device;
</code>

===== C information =====

<code c>
EFL_CANVAS_SCENE_EVENT_DEVICE_REMOVED(Efl_Input_Device *, @beta)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_scene_event_device_removed(void *data, const Efl_Event *event)
{
    Efl_Input_Device *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_SCENE_EVENT_DEVICE_REMOVED, on_efl_canvas_scene_event_device_removed, d);
}

</code>
