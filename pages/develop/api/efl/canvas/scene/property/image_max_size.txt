~~Title: Efl.Canvas.Scene.image_max_size~~
====== Efl.Canvas.Scene.image_max_size ======

===== Values =====

  * **max** - %%The maximum image size (in pixels).%%


\\ {{page>:develop:api-include:efl:canvas:scene:property:image_max_size:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property image_max_size @pure_virtual {
    get {
        return: bool;
    }
    values {
        max: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_canvas_scene_image_max_size_get(const Eo *obj, Eina_Size2D *max);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:scene:property:image_max_size|Efl.Canvas.Scene.image_max_size]]
  * [[:develop:api:efl:ui:win:property:image_max_size|Efl.Ui.Win.image_max_size]]

