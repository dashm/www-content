~~Title: Efl.Canvas.Pointer.pointer_inside~~
====== Efl.Canvas.Pointer.pointer_inside ======

===== Keys =====

  * **seat** - %%The seat to consider, if %%''null''%% then the default seat will be used.%%
===== Values =====

  * **inside** - %%%%''true''%% if the mouse pointer is inside the canvas, %%''false''%% otherwise%%


\\ {{page>:develop:api-include:efl:canvas:pointer:property:pointer_inside:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property pointer_inside @beta @pure_virtual {
    get {
        keys {
            seat: Efl.Input.Device @optional;
        }
    }
    values {
        inside: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_canvas_pointer_inside_get(const Eo *obj, Efl_Input_Device *seat);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:pointer:property:pointer_inside|Efl.Canvas.Pointer.pointer_inside]]
  * [[:develop:api:efl:canvas:object:property:pointer_inside|Efl.Canvas.Object.pointer_inside]]
  * [[:develop:api:efl:ui:win:property:pointer_inside|Efl.Ui.Win.pointer_inside]]

