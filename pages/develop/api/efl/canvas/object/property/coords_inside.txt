~~Title: Efl.Canvas.Object.coords_inside~~
====== Efl.Canvas.Object.coords_inside ======

===== Keys =====

  * **pos** - %%The coordinates in pixels.%%
===== Values =====

  * **return** - %%%%''true''%% if the coordinates are inside the object, %%''false''%% otherwise%%


\\ {{page>:develop:api-include:efl:canvas:object:property:coords_inside:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property coords_inside {
    get {
        keys {
            pos: Eina.Position2D;
        }
    }
    values {
        return: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_canvas_object_coords_inside_get(const Eo *obj, Eina_Position2D pos);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:coords_inside|Efl.Canvas.Object.coords_inside]]

