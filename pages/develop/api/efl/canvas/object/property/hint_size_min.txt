~~Title: Efl.Canvas.Object.hint_size_min~~
====== Efl.Canvas.Object.hint_size_min ======

===== Description =====

%%Hints on the object's minimum size.%%

%%This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate. The object container is in charge of fetching this property and placing the object accordingly.%%

%%Value 0 will be treated as unset hint components, when queried by managers.%%

<note>
%%This property is meant to be set by applications and not by EFL itself. Use this to request a specific size (treated as minimum size).%%
</note>

<note>
%%It is an error for the %%[[:develop:api:efl:gfx:hint:property:hint_size_max|Efl.Gfx.Hint.hint_size_max]]%% to be smaller in either axis than %%[[:develop:api:efl:gfx:hint:property:hint_size_min|Efl.Gfx.Hint.hint_size_min]]%%. In this scenario, the max size hint will be prioritized over the user min size hint.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:hint_size_min:description&nouser&nolink&nodate}}

===== Values =====

  * **sz** - %%Minimum size (hint) in pixels.%%

//Overridden from [[:develop:api:efl:gfx:hint:property:hint_size_min|Efl.Gfx.Hint.hint_size_min]] **(get, set)**.//===== Signature =====

<code>
@property hint_size_min @pure_virtual {
    get {}
    set {}
    values {
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_hint_size_min_get(const Eo *obj);
void efl_gfx_hint_size_min_set(Eo *obj, Eina_Size2D sz);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_size_min|Efl.Gfx.Hint.hint_size_min]]
  * [[:develop:api:efl:canvas:object:property:hint_size_min|Efl.Canvas.Object.hint_size_min]]

