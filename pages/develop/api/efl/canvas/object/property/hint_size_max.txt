~~Title: Efl.Canvas.Object.hint_size_max~~
====== Efl.Canvas.Object.hint_size_max ======

===== Description =====

%%Hints on the object's maximum size.%%

%%This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate.%%

%%The object container is in charge of fetching this property and placing the object accordingly.%%

%%Values -1 will be treated as unset hint components, when queried by managers.%%

<note>
%%Smart objects (such as elementary) can have their own hint policy. So calling this API may or may not affect the size of smart objects.%%
</note>

<note>
%%It is an error for the %%[[:develop:api:efl:gfx:hint:property:hint_size_max|Efl.Gfx.Hint.hint_size_max]]%% to be smaller in either axis than %%[[:develop:api:efl:gfx:hint:property:hint_size_min|Efl.Gfx.Hint.hint_size_min]]%%. In this scenario, the max size hint will be prioritized over the user min size hint.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:hint_size_max:description&nouser&nolink&nodate}}

===== Values =====

  * **sz** - %%Maximum size (hint) in pixels, (-1, -1) by default for canvas objects).%%

//Overridden from [[:develop:api:efl:gfx:hint:property:hint_size_max|Efl.Gfx.Hint.hint_size_max]] **(get, set)**.//===== Signature =====

<code>
@property hint_size_max @pure_virtual {
    get {}
    set {}
    values {
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_hint_size_max_get(const Eo *obj);
void efl_gfx_hint_size_max_set(Eo *obj, Eina_Size2D sz);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_size_max|Efl.Gfx.Hint.hint_size_max]]
  * [[:develop:api:efl:canvas:object:property:hint_size_max|Efl.Canvas.Object.hint_size_max]]
  * [[:develop:api:efl:ui:win:property:hint_size_max|Efl.Ui.Win.hint_size_max]]

