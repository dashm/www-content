~~Title: Efl.Canvas.Object.seat_focus_add~~
====== Efl.Canvas.Object.seat_focus_add ======

===== Description =====

%%Add a seat to the focus list.%%

%%Evas allows the Efl.Canvas.Object to be focused by multiple seats at the same time. This function adds a new seat to the focus list. In other words, after the seat is added to the list this object will now be also focused by this new seat.%%

<note>
%%The old focus APIs still work, however they will only act on the default seat.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:method:seat_focus_add:description&nouser&nolink&nodate}}

===== Signature =====

<code>
seat_focus_add @beta {
    params {
        @in seat: Efl.Input.Device;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_canvas_object_seat_focus_add(Eo *obj, Efl_Input_Device *seat);
</code>

===== Parameters =====

  * **seat** //(in)// - %%The seat that should be added to the focus list. Use %%''null''%% for the default seat.%%

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:method:seat_focus_add|Efl.Canvas.Object.seat_focus_add]]

