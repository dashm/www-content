~~Title: Efl.Model.child_del~~
====== Efl.Model.child_del ======

===== Description =====

%%Remove a child.%%

%%Remove a child of a internal keeping. When the child is effectively removed the event %%[[:develop:api:efl:model:event:child,removed|Efl.Model.child,removed]]%% is then raised to give a chance for listeners to perform any cleanup and/or update references.%%

//Since 1.23//
{{page>:develop:api-include:efl:model:method:child_del:description&nouser&nolink&nodate}}

===== Signature =====

<code>
child_del @pure_virtual {
    params {
        @in child: Efl.Object;
    }
}
</code>

===== C signature =====

<code c>
void efl_model_child_del(Eo *obj, Efl_Object *child);
</code>

===== Parameters =====

  * **child** //(in)// - %%Child to be removed%%

===== Implemented by =====

  * [[:develop:api:efl:model:method:child_del|Efl.Model.child_del]]
  * [[:develop:api:eldbus:model:method:child_del|Eldbus.Model.child_del]]
  * [[:develop:api:efl:composite_model:method:child_del|Efl.Composite_Model.child_del]]
  * [[:develop:api:efl:io:model:method:child_del|Efl.Io.Model.child_del]]
  * [[:develop:api:efl:generic_model:method:child_del|Efl.Generic_Model.child_del]]

