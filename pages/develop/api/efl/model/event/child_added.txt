~~Title: Efl.Model: child,added~~

===== Description =====

%%Event dispatched when new child is added.%%

//Since 1.23//

{{page>:develop:api-include:efl:model:event:child_added:description&nouser&nolink&nodate}}

===== Signature =====

<code>
child,added @beta: Efl.Model_Children_Event;
</code>

===== C information =====

<code c>
EFL_MODEL_EVENT_CHILD_ADDED(Efl_Model_Children_Event, @beta)
</code>

===== C usage =====

<code c>
static void
on_efl_model_event_child_added(void *data, const Efl_Event *event)
{
    Efl_Model_Children_Event info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_MODEL_EVENT_CHILD_ADDED, on_efl_model_event_child_added, d);
}

</code>
