~~Title: Efl.Task.priority~~
====== Efl.Task.priority ======

===== Description =====

%%The priority of this task.%%

//Since 1.22//
{{page>:develop:api-include:efl:task:property:priority:description&nouser&nolink&nodate}}

===== Values =====

  * **priority** - %%Desired priority.%%

===== Signature =====

<code>
@property priority {
    get {}
    set {}
    values {
        priority: Efl.Task_Priority;
    }
}
</code>

===== C signature =====

<code c>
Efl_Task_Priority efl_task_priority_get(const Eo *obj);
void efl_task_priority_set(Eo *obj, Efl_Task_Priority priority);
</code>

===== Implemented by =====

  * [[:develop:api:efl:task:property:priority|Efl.Task.priority]]
  * [[:develop:api:efl:app:property:priority|Efl.App.priority]]
  * [[:develop:api:efl:exe:property:priority|Efl.Exe.priority]]

