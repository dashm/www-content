~~Title: Efl.Loop_Model~~
====== Efl.Loop_Model (class) ======

===== Description =====

%%The Efl Loop Model class%%

//Since 1.23//

{{page>:develop:api-include:efl:loop_model:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)// => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)//
    * [[:develop:api:efl:object|Efl.Object]] //(class)//
  * [[:develop:api:efl:model|Efl.Model]] //(interface)//


++++
===== Members =====

**[[:develop:api:efl:loop_model:method:invalidate|invalidate]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to perform special actions when your object loses its parent, if you need to.%%
<code c>
void efl_invalidate(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_model:property:property|property]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:model|Efl.Model]]]//\\
> %%No description supplied.%%
<code c>
Eina_Value *efl_model_property_get(const Eo *obj, const char *property);
Eina_Future *efl_model_property_set(Eo *obj, const char *property, Eina_Value *value);
</code>
\\
**[[:develop:api:efl:loop_model:method:property_ready_get|property_ready_get]]**// [Overridden from [[:develop:api:efl:model|Efl.Model]]]//\\
> %%Get a future value when it changes to something that is not error:EAGAIN%%
<code c>
Eina_Future *efl_model_property_ready_get(Eo *obj, const char *property);
</code>
\\
**[[:develop:api:efl:loop_model:method:volatile_make|volatile_make]]**\\
> %%To be called when a Child model is created by %%[[:develop:api:efl:model:method:children_slice_get|Efl.Model.children_slice_get]]%% by the one creating the child object.%%
<code c>
void efl_loop_model_volatile_make(Eo *obj);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] ^^^
|  | **[[:develop:api:efl:loop_consumer:method:future_rejected|future_rejected]]** | %%Creates a new future that is already rejected to a specified error using the %%[[:develop:api:efl:loop_consumer:property:loop|Efl.Loop_Consumer.loop.get]]%%.%% |
|  | **[[:develop:api:efl:loop_consumer:method:future_resolved|future_resolved]]** | %%Creates a new future that is already resolved to a value.%% |
|  | **[[:develop:api:efl:loop_consumer:property:loop|loop]]** //**(get)**// |  |
|  | **[[:develop:api:efl:loop_consumer:property:parent|parent]]** //**(get, set)**// | %%The parent of an object.%% |
|  | **[[:develop:api:efl:loop_consumer:method:promise_new|promise_new]]** | %%Create a new promise with the scheduler coming from the loop provided by this object.%% |
^ [[:develop:api:efl:model|Efl.Model]] ^^^
|  | **[[:develop:api:efl:model:method:child_add|child_add]]** | %%Add a new child.%% |
|  | **[[:develop:api:efl:model:method:child_del|child_del]]** | %%Remove a child.%% |
|  | **[[:develop:api:efl:model:property:children_count|children_count]]** //**(get)**// |  |
|  | **[[:develop:api:efl:model:method:children_slice_get|children_slice_get]]** | %%Get children slice OR full range.%% |
|  | **[[:develop:api:efl:model:property:properties|properties]]** //**(get)**// |  |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:constructor|constructor]]** | %%Implement this method to provide optional initialization code for your object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:destructor|destructor]]** | %%Implement this method to provide deinitialization code for your object if you need it.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:method:finalize|finalize]]** | %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |

===== Events =====

==== Inherited ====

^ [[:develop:api:efl:model|Efl.Model]] ^^^
|  | **[[:develop:api:efl:model:event:child_added|child,added]]** | %%Event dispatched when new child is added.%% |
|  | **[[:develop:api:efl:model:event:child_removed|child,removed]]** | %%Event dispatched when child is removed.%% |
|  | **[[:develop:api:efl:model:event:children_count_changed|children,count,changed]]** | %%Event dispatched when children count is finished.%% |
|  | **[[:develop:api:efl:model:event:properties_changed|properties,changed]]** | %%Event dispatched when properties list is available.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
