~~Title: Efl.Callback_Priority~~

===== Description =====

%%Callback priority. Range is -32k - 32k. The lower the number, the higher the priority.%%

%%This is used to insert an event handler relative to the existing stack of sorted event handlers according to that priority. All event handlers always have a priority. If not specified %%[[:develop:api:efl:callback_priority_default|Efl.Callback_Priority_Default]]%% is to be assumed.%%

%%See %%[[:develop:api:efl:callback_priority_before|Efl.Callback_Priority_Before]]%% %%[[:develop:api:efl:callback_priority_default|Efl.Callback_Priority_Default]]%%  %%[[:develop:api:efl:callback_priority_after|Efl.Callback_Priority_After]]%%%%

//Since 1.22//

{{page>:develop:api-include:efl:callback_priority:description&nouser&nolink&nodate}}

===== Signature =====

<code>
type Efl.Callback_Priority: short;
</code>

===== C signature =====

<code c>
typedef short Efl_Callback_Priority;
</code>
