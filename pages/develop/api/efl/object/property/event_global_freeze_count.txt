~~Title: Efl.Object.event_global_freeze_count~~
====== Efl.Object.event_global_freeze_count ======

===== Values =====

  * **fcount** - %%The global event freeze count.%%


\\ {{page>:develop:api-include:efl:object:property:event_global_freeze_count:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property event_global_freeze_count @static {
    get {}
    values {
        fcount: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_event_global_freeze_count_get();
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:property:event_global_freeze_count|Efl.Object.event_global_freeze_count]]

