~~Title: Efl.Object.event_global_freeze~~
====== Efl.Object.event_global_freeze ======

===== Description =====

%%Globally freeze events for ALL EFL OBJECTS.%%

%%Prevents event callbacks from being called for all EFL objects. Enable events again using %%[[:develop:api:efl:object:method:event_global_thaw|Efl.Object.event_global_thaw]]%%. Events marked %%''hot''%% cannot be stopped.%%

<note>
%%USE WITH CAUTION.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:object:method:event_global_freeze:description&nouser&nolink&nodate}}

===== Signature =====

<code>
event_global_freeze @static {}
</code>

===== C signature =====

<code c>
void efl_event_global_freeze();
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:method:event_global_freeze|Efl.Object.event_global_freeze]]

