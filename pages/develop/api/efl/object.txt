~~Title: Efl.Object~~
====== Efl.Object (class) ======

===== Description =====

%%Abstract EFL object class.%%

%%All EFL objects inherit from this class, which provides basic functionality like naming, debugging, hierarchy traversal, event emission and life cycle management.%%

%%Life Cycle Objects are created with efl_add() and mostly disposed of with efl_del(). As an optimization, efl_add() accepts a list of initialization functions which the programmer can use to further customize the object before it is fully constructed. Also, objects can have a parent which will keep them alive as long as the parent is alive, so the programmer does not need to keep track of references. (See the %%[[:develop:api:efl:object:property:parent|Efl.Object.parent]]%% property for details). Due to the above characteristics, EFL objects undergo the following phases during their Life Cycle: - Construction: The %%[[:develop:api:efl:object:method:constructor|Efl.Object.constructor]]%% method is called. Afterwards, any user-supplied initialization methods are called. - Finalization: The %%[[:develop:api:efl:object:method:finalize|Efl.Object.finalize]]%% method is called and %%[[:develop:api:efl:object:property:finalized|Efl.Object.finalized]]%% is set to %%''true''%% when it returns. Object is usable at this point. - Invalidation: The object has lost its parent. The %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%% method is called so all the object's relationships can be terminated. %%[[:develop:api:efl:object:property:invalidated|Efl.Object.invalidated]]%% is set to %%''true''%%. - Destruction: The object has no parent and it can be destroyed. The %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%% method is called, use it to return any resources the object might have gathered during its life.%%

//Since 1.22//

{{page>:develop:api-include:efl:object:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**//\\
> %%Allow an object to be deleted by unref even if it has a parent.%%
<code c>
Eina_Bool efl_allow_parent_unref_get(const Eo *obj);
void efl_allow_parent_unref_set(Eo *obj, Eina_Bool allow);
</code>
\\
**[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]**\\
> %%Get an iterator on all children.%%
<code c>
Eina_Iterator *efl_children_iterator_new(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**//\\
> %%A human readable comment for the object.%%
<code c>
const char *efl_comment_get(const Eo *obj);
void efl_comment_set(Eo *obj, const char *comment);
</code>
\\
**[[:develop:api:efl:object:method:composite_attach|composite_attach]]**\\
> %%Make an object a composite object of another.%%
<code c>
Eina_Bool efl_composite_attach(Eo *obj, Efl_Object *comp_obj);
</code>
\\
**[[:develop:api:efl:object:method:composite_detach|composite_detach]]**\\
> %%Detach a composite object from another object.%%
<code c>
Eina_Bool efl_composite_detach(Eo *obj, Efl_Object *comp_obj);
</code>
\\
**[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]**\\
> %%Check if an object is part of a composite object.%%
<code c>
Eina_Bool efl_composite_part_is(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:constructor|constructor]]**\\
> %%Implement this method to provide optional initialization code for your object.%%
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]**\\
> %%Build a read-only name for this object used for debugging.%%
<code c>
void efl_debug_name_override(Eo *obj, Eina_Strbuf *sb);
</code>
\\
**[[:develop:api:efl:object:method:destructor|destructor]]**\\
> %%Implement this method to provide deinitialization code for your object if you need it.%%
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]**\\
> %%Remove an event callback forwarder for a specified event and object.%%
<code c>
void efl_event_callback_forwarder_del(Eo *obj, const Efl_Event_Description desc, Efl_Object *new_obj);
</code>
\\
**[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]**\\
> %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%%
<code c>
void efl_event_callback_forwarder_priority_add(Eo *obj, const Efl_Event_Description desc, Efl_Callback_Priority priority, Efl_Object *source);
</code>
\\
**[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]**\\
> %%Stop the current callback call.%%
<code c>
void efl_event_callback_stop(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:event_freeze|event_freeze]]**\\
> %%Freeze events of this object.%%
<code c>
void efl_event_freeze(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**//\\
> 
<code c>
int efl_event_freeze_count_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** ''static''\\
> %%Globally freeze events for ALL EFL OBJECTS.%%
<code c>
void efl_event_global_freeze();
</code>
\\
**[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// ''static''\\
> 
<code c>
int efl_event_global_freeze_count_get();
</code>
\\
**[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** ''static''\\
> %%Globally thaw events for ALL EFL OBJECTS.%%
<code c>
void efl_event_global_thaw();
</code>
\\
**[[:develop:api:efl:object:method:event_thaw|event_thaw]]**\\
> %%Thaw events of object.%%
<code c>
void efl_event_thaw(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:finalize|finalize]]**\\
> %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%%
<code c>
Efl_Object *efl_finalize(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_finalized_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:object:method:invalidate|invalidate]]**\\
> %%Implement this method to perform special actions when your object loses its parent, if you need to.%%
<code c>
void efl_invalidate(Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_invalidated_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_invalidating_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:object:property:name|name]]** //**(get, set)**//\\
> %%The name of the object.%%
<code c>
const char *efl_name_get(const Eo *obj);
void efl_name_set(Eo *obj, const char *name);
</code>
\\
**[[:develop:api:efl:object:method:name_find|name_find]]**\\
> %%Find a child object with the given name and return it.%%
<code c>
Efl_Object *efl_name_find(const Eo *obj, const char *search);
</code>
\\
**[[:develop:api:efl:object:property:parent|parent]]** //**(get, set)**//\\
> %%The parent of an object.%%
<code c>
Efl_Object *efl_parent_get(const Eo *obj);
void efl_parent_set(Eo *obj, Efl_Object *parent);
</code>
\\
**[[:develop:api:efl:object:method:provider_find|provider_find]]**\\
> %%Searches upwards in the object tree for a provider which knows the given class/interface.%%
<code c>
Efl_Object *efl_provider_find(const Eo *obj, const Efl_Class *klass);
</code>
\\
**[[:develop:api:efl:object:method:provider_register|provider_register]]**\\
> %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%%
<code c>
Eina_Bool efl_provider_register(Eo *obj, const Efl_Class *klass, const Efl_Object *provider);
</code>
\\
**[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]**\\
> %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%%
<code c>
Eina_Bool efl_provider_unregister(Eo *obj, const Efl_Class *klass, const Efl_Object *provider);
</code>
\\

===== Events =====

**[[:develop:api:efl:object:event:del|del]]**\\
> %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%%
<code c>
EFL_EVENT_DEL(void, @hot)
</code>
\\ **[[:develop:api:efl:object:event:destruct|destruct]]**\\
> %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%%
<code c>
EFL_EVENT_DESTRUCT(void, @hot)
</code>
\\ **[[:develop:api:efl:object:event:invalidate|invalidate]]**\\
> %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%%
<code c>
EFL_EVENT_INVALIDATE(void, @hot)
</code>
\\ **[[:develop:api:efl:object:event:noref|noref]]**\\
> %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%%
<code c>
EFL_EVENT_NOREF(void, @hot)
</code>
\\ **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]**\\
> %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%%
<code c>
EFL_EVENT_OWNERSHIP_SHARED(void, @hot)
</code>
\\ **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]**\\
> %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%%
<code c>
EFL_EVENT_OWNERSHIP_UNIQUE(void, @hot)
</code>
\\ 