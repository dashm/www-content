~~Title: Efl.Loop~~
====== Efl.Loop (class) ======

===== Description =====

%%The Efl Main Loop%%

%%The Efl main loop provides a clean and tiny event loop library with many modules to do lots of convenient things for a programmer, saving time and effort. It's lean and designed to work on anything from embedded systems all the way up to large and powerful multi-cpu workstations. The main loop has a number of primitives you can use. It serializes these and allows for greater responsiveness without the need for threads (or any other concurrency). However you can provide these if you need to.%%

//Since 1.22//

{{page>:develop:api-include:efl:loop:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:task|Efl.Task]] //(class)// => [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)// => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:task|Efl.Task]] //(class)//
    * [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)//
      * [[:develop:api:efl:object|Efl.Object]] //(class)//


++++
===== Members =====

**[[:develop:api:efl:loop:method:begin|begin]]**\\
> %%Runs the application main loop.%%
<code c>
Eina_Value *efl_loop_begin(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:constructor|constructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide optional initialization code for your object.%%
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide deinitialization code for your object if you need it.%%
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:end|end]]**// [Overridden from [[:develop:api:efl:task|Efl.Task]]]//\\
> %%Request the task end (may send a signal or interrupt signal resulting in a terminate event being triggered in the target task loop).%%
<code c>
void efl_task_end(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:idle|idle]]**\\
> %%A future promise that will be resolved from a clean main loop context as soon as the main loop is idle.%%
<code c>
Eina_Future *efl_loop_idle(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:invalidate|invalidate]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to perform special actions when your object loses its parent, if you need to.%%
<code c>
void efl_invalidate(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:iterate|iterate]]**\\
> %%Runs a single iteration of the main loop to process everything on the queue.%%
<code c>
void efl_loop_iterate(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:iterate_may_block|iterate_may_block]]**\\
> %%Runs a single iteration of the main loop to process everything on the queue with block/non-blocking status.%%
<code c>
int efl_loop_iterate_may_block(Eo *obj, int may_block);
</code>
\\
**[[:develop:api:efl:loop:method:job|job]]**\\
> %%A future promise that will be resolved from a clean main loop context as soon as possible.%%
<code c>
Eina_Future *efl_loop_job(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:method:quit|quit]]**\\
> %%Quits the main loop once all the events currently on the queue have been processed.%%
<code c>
void efl_loop_quit(Eo *obj, Eina_Value exit_code);
</code>
\\
**[[:develop:api:efl:loop:method:run|run]]**// [Overridden from [[:develop:api:efl:task|Efl.Task]]]//\\
> %%Actually run the task.%%
<code c>
Eina_Bool efl_task_run(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop:property:throttle|throttle]]** //**(get, set)**//\\
> %%Slow down the loop execution by forcing sleep for a small period of time every time the loop iterates/loops.%%
<code c>
double efl_loop_throttle_get(const Eo *obj);
void efl_loop_throttle_set(Eo *obj, double amount);
</code>
\\
**[[:develop:api:efl:loop:property:time|time]]** //**(get, set)**// ''protected set''\\
> %%Retrieves the time at which the last loop stopped waiting for timeouts or events.%%
<code c>
double efl_loop_time_get(const Eo *obj);
void efl_loop_time_set(Eo *obj, double timepoint);
</code>
\\
**[[:develop:api:efl:loop:method:timeout|timeout]]**\\
> %%A future promise that will be resolved from a clean main loop context after %%''time''%% seconds.%%
<code c>
Eina_Future *efl_loop_timeout(Eo *obj, double time);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] ^^^
|  | **[[:develop:api:efl:loop_consumer:method:future_rejected|future_rejected]]** | %%Creates a new future that is already rejected to a specified error using the %%[[:develop:api:efl:loop_consumer:property:loop|Efl.Loop_Consumer.loop.get]]%%.%% |
|  | **[[:develop:api:efl:loop_consumer:method:future_resolved|future_resolved]]** | %%Creates a new future that is already resolved to a value.%% |
|  | **[[:develop:api:efl:loop_consumer:property:loop|loop]]** //**(get)**// |  |
|  | **[[:develop:api:efl:loop_consumer:method:promise_new|promise_new]]** | %%Create a new promise with the scheduler coming from the loop provided by this object.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:method:finalize|finalize]]** | %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
^ [[:develop:api:efl:task|Efl.Task]] ^^^
|  | **[[:develop:api:efl:task:property:exit_code|exit_code]]** //**(get)**// |  |
|  | **[[:develop:api:efl:task:property:flags|flags]]** //**(get, set)**// | %%Flags to further customize task's behavior.%% |
|  | **[[:develop:api:efl:task:property:parent|parent]]** //**(get, set)**// | %%The parent of an object.%% |
|  | **[[:develop:api:efl:task:property:priority|priority]]** //**(get, set)**// | %%The priority of this task.%% |

===== Events =====

**[[:develop:api:efl:loop:event:arguments|arguments]]**\\
> %%Event happens when args are provided to the loop by args_add().%%
<code c>
EFL_LOOP_EVENT_ARGUMENTS(Efl_Loop_Arguments)
</code>
\\ **[[:develop:api:efl:loop:event:idle|idle]]**\\
> %%Event occurs once the main loop is idle. If you keep listening on this event it may increase the burden on your CPU.%%
<code c>
EFL_LOOP_EVENT_IDLE(void, @restart)
</code>
\\ **[[:develop:api:efl:loop:event:idle_enter|idle,enter]]**\\
> %%Event occurs once the main loop enters the idle state.%%
<code c>
EFL_LOOP_EVENT_IDLE_ENTER(void, @restart)
</code>
\\ **[[:develop:api:efl:loop:event:idle_exit|idle,exit]]**\\
> %%Event occurs once the main loop exits the idle state.%%
<code c>
EFL_LOOP_EVENT_IDLE_EXIT(void, @restart)
</code>
\\ **[[:develop:api:efl:loop:event:poll_high|poll,high]]**\\
> %%Event occurs multiple times per second. The exact tick is undefined and can be adjusted system-wide.%%
<code c>
EFL_LOOP_EVENT_POLL_HIGH(void)
</code>
\\ **[[:develop:api:efl:loop:event:poll_low|poll,low]]**\\
> %%Event occurs multiple times every 15 minutes. The exact tick is undefined and can be adjusted system-wide.%%
<code c>
EFL_LOOP_EVENT_POLL_LOW(void)
</code>
\\ **[[:develop:api:efl:loop:event:poll_medium|poll,medium]]**\\
> %%Event occurs multiple times per minute. The exact tick is undefined and can be adjusted system-wide.%%
<code c>
EFL_LOOP_EVENT_POLL_MEDIUM(void)
</code>
\\ **[[:develop:api:efl:loop:event:quit|quit]]**\\
> %%Event occurs when the loop was requested to quit externally e.g. by a ctrl+c signal or a request from a parent loop/thread to have the child exit.%%
<code c>
EFL_LOOP_EVENT_QUIT(void)
</code>
\\ ==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
^ [[:develop:api:efl:task|Efl.Task]] ^^^
|  | **[[:develop:api:efl:task:event:exit|exit]]** | %%Called when the task exits. You can pick up any information you need at this point such as exit_code etc.%% |
