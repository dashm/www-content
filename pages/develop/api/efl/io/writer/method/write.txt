~~Title: Efl.Io.Writer.write~~
====== Efl.Io.Writer.write ======

===== Description =====

%%Writes data from a pre-populated buffer.%%

%%This operation will be executed immediately and may or may not block the caller thread for some time. The details of blocking behavior is defined by the implementation and may be subject to other parameters such as non-blocking flags, maximum timeout or even retry attempts.%%

%%You can understand this method as write(2) libc function.%%

//Since 1.22//
{{page>:develop:api-include:efl:io:writer:method:write:description&nouser&nolink&nodate}}

===== Signature =====

<code>
write @pure_virtual {
    params {
        @inout slice: slice<ubyte>;
        @out remaining: slice<ubyte> @optional;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_io_writer_write(Eo *obj, Eina_Slice slice, Eina_Slice *remaining);
</code>

===== Parameters =====

  * **slice** //(inout)// - %%Provides a pre-populated memory to be used up to slice.len. The returned slice will be adapted as length will be set to the actually used amount of bytes, which can be smaller than the request.%%
  * **remaining** //(out)// - %%Convenience to output the remaining parts of slice that were not written. If the full slice was written, this will be a slice of zero-length.%%

===== Implemented by =====

  * [[:develop:api:efl:io:writer:method:write|Efl.Io.Writer.write]]
  * [[:develop:api:efl:io:writer_fd:method:write|Efl.Io.Writer_Fd.write]]
  * [[:develop:api:efl:io:file:method:write|Efl.Io.File.write]]
  * [[:develop:api:efl:io:stderr:method:write|Efl.Io.Stderr.write]]
  * [[:develop:api:efl:io:stdout:method:write|Efl.Io.Stdout.write]]
  * [[:develop:api:efl:net:socket_fd:method:write|Efl.Net.Socket_Fd.write]]
  * [[:develop:api:efl:net:socket_udp:method:write|Efl.Net.Socket_Udp.write]]
  * [[:develop:api:efl:io:buffered_stream:method:write|Efl.Io.Buffered_Stream.write]]
  * [[:develop:api:efl:appthread:method:write|Efl.Appthread.write]]
  * [[:develop:api:efl:exe:method:write|Efl.Exe.write]]
  * [[:develop:api:efl:net:socket_ssl:method:write|Efl.Net.Socket_Ssl.write]]
  * [[:develop:api:efl:net:server_udp_client:method:write|Efl.Net.Server_Udp_Client.write]]
  * [[:develop:api:efl:net:dialer_websocket:method:write|Efl.Net.Dialer_Websocket.write]]
  * [[:develop:api:efl:net:dialer_http:method:write|Efl.Net.Dialer_Http.write]]
  * [[:develop:api:efl:net:socket_windows:method:write|Efl.Net.Socket_Windows.write]]
  * [[:develop:api:efl:thread:method:write|Efl.Thread.write]]
  * [[:develop:api:efl:io:buffer:method:write|Efl.Io.Buffer.write]]
  * [[:develop:api:efl:io:queue:method:write|Efl.Io.Queue.write]]

