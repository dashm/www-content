~~Title: Efl.App: resume~~

===== Description =====

%%Called before a window is rendered after a pause event%%

//Since 1.22//

{{page>:develop:api-include:efl:app:event:resume:description&nouser&nolink&nodate}}

===== Signature =====

<code>
resume;
</code>

===== C information =====

<code c>
EFL_APP_EVENT_RESUME(void)
</code>

===== C usage =====

<code c>
static void
on_efl_app_event_resume(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_APP_EVENT_RESUME, on_efl_app_event_resume, d);
}

</code>
