~~Title: Efl.App: signal,hup~~

===== Description =====

%%System specific, but on unix maps to SIGHUP signal to the process - only called on main loop object%%

//Since 1.22//

{{page>:develop:api-include:efl:app:event:signal_hup:description&nouser&nolink&nodate}}

===== Signature =====

<code>
signal,hup;
</code>

===== C information =====

<code c>
EFL_APP_EVENT_SIGNAL_HUP(void)
</code>

===== C usage =====

<code c>
static void
on_efl_app_event_signal_hup(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_APP_EVENT_SIGNAL_HUP, on_efl_app_event_signal_hup, d);
}

</code>
