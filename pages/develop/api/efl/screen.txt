~~Title: Efl.Screen~~
====== Efl.Screen (interface) ======

===== Description =====

%%Efl screen interface%%

//Since 1.22//

{{page>:develop:api-include:efl:screen:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:screen:property:screen_dpi|screen_dpi]]** //**(get)**//\\
> 
<code c>
void efl_screen_dpi_get(const Eo *obj, int *xdpi, int *ydpi);
</code>
\\
**[[:develop:api:efl:screen:property:screen_rotation|screen_rotation]]** //**(get)**//\\
> 
<code c>
int efl_screen_rotation_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:screen:property:screen_scale_factor|screen_scale_factor]]** //**(get)**//\\
> 
<code c>
float efl_screen_scale_factor_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:screen:property:screen_size_in_pixels|screen_size_in_pixels]]** //**(get)**//\\
> 
<code c>
Eina_Size2D efl_screen_size_in_pixels_get(const Eo *obj);
</code>
\\

===== Events =====

