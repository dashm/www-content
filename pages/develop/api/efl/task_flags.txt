~~Title: Efl.Task_Flags~~

===== Description =====

%%Flags to further customize task's behavior.%%

//Since 1.22//

{{page>:develop:api-include:efl:task_flags:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:task_flags:fields&nouser&nolink&nodate}}

  * **none** - %%No special flags.%%
  * **use_stdin** - %%Task will require console input.%%
  * **use_stdout** - %%Task will require console output.%%
  * **no_exit_code_error** - %%Task will not produce an exit code upon termination.%%
  * **exit_with_parent** - %%Exit when parent exits.%%

===== Signature =====

<code>
enum Efl.Task_Flags {
    none: 0,
    use_stdin: 1,
    use_stdout: 2,
    no_exit_code_error: 4,
    exit_with_parent: 8
}
</code>

===== C signature =====

<code c>
typedef enum {
    EFL_TASK_FLAGS_NONE = 0,
    EFL_TASK_FLAGS_USE_STDIN = 1,
    EFL_TASK_FLAGS_USE_STDOUT = 2,
    EFL_TASK_FLAGS_NO_EXIT_CODE_ERROR = 4,
    EFL_TASK_FLAGS_EXIT_WITH_PARENT = 8
} Efl_Task_Flags;
</code>
