~~Title: Efl.Layout.Calc.calc_force~~
====== Efl.Layout.Calc.calc_force ======

===== Description =====

%%Forces a Size/Geometry calculation.%%

%%Forces the object to recalculate its layout regardless of freeze/thaw. This API should be used carefully.%%

%%See also %%[[:develop:api:efl:layout:calc:method:calc_freeze|Efl.Layout.Calc.calc_freeze]]%% and %%[[:develop:api:efl:layout:calc:method:calc_thaw|Efl.Layout.Calc.calc_thaw]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:layout:calc:method:calc_force:description&nouser&nolink&nodate}}

===== Signature =====

<code>
calc_force @protected @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_layout_calc_force(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:calc:method:calc_force|Efl.Layout.Calc.calc_force]]
  * [[:develop:api:efl:canvas:layout:method:calc_force|Efl.Canvas.Layout.calc_force]]
  * [[:develop:api:efl:ui:image:method:calc_force|Efl.Ui.Image.calc_force]]
  * [[:develop:api:efl:ui:layout_base:method:calc_force|Efl.Ui.Layout_Base.calc_force]]

