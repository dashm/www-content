~~Title: Efl.Loop.throttle~~
====== Efl.Loop.throttle ======

===== Description =====

%%Slow down the loop execution by forcing sleep for a small period of time every time the loop iterates/loops.%%

//Since 1.22//
{{page>:develop:api-include:efl:loop:property:throttle:description&nouser&nolink&nodate}}

===== Values =====

  * **amount** - %%Time to sleep for each "loop iteration"%%

===== Signature =====

<code>
@property throttle {
    get {}
    set {}
    values {
        amount: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_loop_throttle_get(const Eo *obj);
void efl_loop_throttle_set(Eo *obj, double amount);
</code>

===== Implemented by =====

  * [[:develop:api:efl:loop:property:throttle|Efl.Loop.throttle]]

