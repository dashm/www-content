~~Title: Efl.Loop: quit~~

===== Description =====

%%Event occurs when the loop was requested to quit externally e.g. by a ctrl+c signal or a request from a parent loop/thread to have the child exit.%%

//Since 1.22//

{{page>:develop:api-include:efl:loop:event:quit:description&nouser&nolink&nodate}}

===== Signature =====

<code>
quit;
</code>

===== C information =====

<code c>
EFL_LOOP_EVENT_QUIT(void)
</code>

===== C usage =====

<code c>
static void
on_efl_loop_event_quit(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_LOOP_EVENT_QUIT, on_efl_loop_event_quit, d);
}

</code>
