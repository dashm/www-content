~~Title: Efl.Ui.Collection_View.factory~~
====== Efl.Ui.Collection_View.factory ======

===== Description =====

%%Define the factory used to create all the items.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection_view:property:factory:description&nouser&nolink&nodate}}

===== Values =====

  * **factory** - %%The factory.%%

===== Signature =====

<code>
@property factory {
    get {}
    set {}
    values {
        factory: Efl.Ui.Factory;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Factory *efl_ui_collection_view_factory_get(const Eo *obj);
void efl_ui_collection_view_factory_set(Eo *obj, Efl_Ui_Factory *factory);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:collection_view:property:factory|Efl.Ui.Collection_View.factory]]

