~~Title: Efl.Ui.Radio_Group: value,changed~~

===== Description =====

%%Emitted each time the %%''selected_value''%% changes. The event information contains the %%[[:develop:api:efl:ui:radio:property:state_value|Efl.Ui.Radio.state_value]]%% of the newly selected button or -1 if no button is now selected.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:radio_group:event:value_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
value,changed: int;
</code>

===== C information =====

<code c>
EFL_UI_RADIO_GROUP_EVENT_VALUE_CHANGED(int)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_radio_group_event_value_changed(void *data, const Efl_Event *event)
{
    int info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_RADIO_GROUP_EVENT_VALUE_CHANGED, on_efl_ui_radio_group_event_value_changed, d);
}

</code>
