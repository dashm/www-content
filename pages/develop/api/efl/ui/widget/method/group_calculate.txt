~~Title: Efl.Ui.Widget.group_calculate~~
====== Efl.Ui.Widget.group_calculate ======

===== Description =====

%%Triggers an immediate recalculation of this object's geometry.%%

%%This will also reset the flag %%[[:develop:api:efl:canvas:group:property:group_need_recalculate|Efl.Canvas.Group.group_need_recalculate]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:group_calculate:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:canvas:group:method:group_calculate|Efl.Canvas.Group.group_calculate]].//===== Signature =====

<code>
group_calculate {}
</code>

===== C signature =====

<code c>
void efl_canvas_group_calculate(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:group:method:group_calculate|Efl.Canvas.Group.group_calculate]]
  * [[:develop:api:efl:canvas:event_grabber:method:group_calculate|Efl.Canvas.Event_Grabber.group_calculate]]
  * [[:develop:api:efl:canvas:layout:method:group_calculate|Efl.Canvas.Layout.group_calculate]]
  * [[:develop:api:efl:ui:widget:method:group_calculate|Efl.Ui.Widget.group_calculate]]
  * [[:develop:api:efl:ui:table:method:group_calculate|Efl.Ui.Table.group_calculate]]
  * [[:develop:api:efl:ui:box:method:group_calculate|Efl.Ui.Box.group_calculate]]
  * [[:develop:api:efl:ui:layout_base:method:group_calculate|Efl.Ui.Layout_Base.group_calculate]]
  * [[:develop:api:efl:ui:clock:method:group_calculate|Efl.Ui.Clock.group_calculate]]
  * [[:develop:api:efl:ui:text:method:group_calculate|Efl.Ui.Text.group_calculate]]
  * [[:develop:api:efl:ui:panel:method:group_calculate|Efl.Ui.Panel.group_calculate]]
  * [[:develop:api:efl:ui:layout:method:group_calculate|Efl.Ui.Layout.group_calculate]]
  * [[:develop:api:efl:ui:textpath:method:group_calculate|Efl.Ui.Textpath.group_calculate]]
  * [[:develop:api:efl:ui:scroller:method:group_calculate|Efl.Ui.Scroller.group_calculate]]
  * [[:develop:api:efl:ui:internal_text_scroller:method:group_calculate|Efl.Ui.Internal_Text_Scroller.group_calculate]]
  * [[:develop:api:efl:ui:panes:method:group_calculate|Efl.Ui.Panes.group_calculate]]
  * [[:develop:api:efl:ui:calendar:method:group_calculate|Efl.Ui.Calendar.group_calculate]]
  * [[:develop:api:efl:ui:frame:method:group_calculate|Efl.Ui.Frame.group_calculate]]
  * [[:develop:api:efl:ui:popup:method:group_calculate|Efl.Ui.Popup.group_calculate]]
  * [[:develop:api:efl:ui:video:method:group_calculate|Efl.Ui.Video.group_calculate]]
  * [[:develop:api:efl:ui:relative_layout:method:group_calculate|Efl.Ui.Relative_Layout.group_calculate]]
  * [[:develop:api:efl:ui:pan:method:group_calculate|Efl.Ui.Pan.group_calculate]]
  * [[:develop:api:efl:ui:image_zoomable_pan:method:group_calculate|Efl.Ui.Image_Zoomable_Pan.group_calculate]]

