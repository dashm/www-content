~~Title: Efl.Ui.Widget.scroll_freeze_pop~~
====== Efl.Ui.Widget.scroll_freeze_pop ======

===== Description =====

%%Pop scroller freeze%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:scroll_freeze_pop:description&nouser&nolink&nodate}}

===== Signature =====

<code>
scroll_freeze_pop @beta {}
</code>

===== C signature =====

<code c>
void efl_ui_widget_scroll_freeze_pop(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:scroll_freeze_pop|Efl.Ui.Widget.scroll_freeze_pop]]

