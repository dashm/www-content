~~Title: Efl.Ui.Widget.focus_state_apply~~
====== Efl.Ui.Widget.focus_state_apply ======

===== Description =====

%%Apply a new focus state on the widget.%%

%%This method is called internally by %%[[:develop:api:efl:ui:widget|Efl.Ui.Widget]]%%. Override it to change how a widget interacts with its focus manager. If a widget desires to change the applied configuration, it has to modify %%''configured_state''%% in addition to any internal changes.%%

%%The default implementation (when this method is not overridden) applies %%''configured_state''%% using the %%''manager''%% contained inside.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:focus_state_apply:description&nouser&nolink&nodate}}

===== Signature =====

<code>
focus_state_apply @protected {
    params {
        @in current_state: Efl.Ui.Widget_Focus_State;
        @inout configured_state: Efl.Ui.Widget_Focus_State;
        @in redirect: Efl.Ui.Widget;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_focus_state_apply(Eo *obj, Efl_Ui_Widget_Focus_State current_state, Efl_Ui_Widget_Focus_State configured_state, Efl_Ui_Widget *redirect);
</code>

===== Parameters =====

  * **current_state** //(in)// - %%The current focus configuration of the widget.%%
  * **configured_state** //(inout)// - %%The new configuration being set on the widget.%%
  * **redirect** //(in)// - %%A redirect object if there is any%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:focus_state_apply|Efl.Ui.Widget.focus_state_apply]]
  * [[:develop:api:efl:ui:collection_view:method:focus_state_apply|Efl.Ui.Collection_View.focus_state_apply]]
  * [[:develop:api:efl:ui:scroller:method:focus_state_apply|Efl.Ui.Scroller.focus_state_apply]]
  * [[:develop:api:efl:ui:collection:method:focus_state_apply|Efl.Ui.Collection.focus_state_apply]]

