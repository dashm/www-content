~~Title: Efl.Ui.Widget.widget_sub_object_del~~
====== Efl.Ui.Widget.widget_sub_object_del ======

===== Description =====

%%Virtual function customizing sub objects being removed.%%

%%When a widget is removed as a sub-object from another widget (%%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%%, %%[[:develop:api:efl:content:method:content_unset|Efl.Content.content_unset]]%%, for example) some of its properties are automatically adjusted.(like focus, access, tree dump) Override this method if you want to customize differently sub-objects being removed to this object.%%

%%Sub objects can be any canvas object, not necessarily widgets.%%

%%See also %%[[:develop:api:efl:ui:widget:property:widget_parent|Efl.Ui.Widget.widget_parent]]%% and %%[[:develop:api:efl:ui:widget:method:widget_sub_object_add|Efl.Ui.Widget.widget_sub_object_add]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:widget_sub_object_del:description&nouser&nolink&nodate}}

===== Signature =====

<code>
widget_sub_object_del @protected {
    params {
        @in sub_obj: Efl.Canvas.Object;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_sub_object_del(Eo *obj, Efl_Canvas_Object *sub_obj);
</code>

===== Parameters =====

  * **sub_obj** //(in)// - %%Sub object to be removed. Should be a child of this widget.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:widget_sub_object_del|Efl.Ui.Widget.widget_sub_object_del]]
  * [[:develop:api:efl:ui:flip:method:widget_sub_object_del|Efl.Ui.Flip.widget_sub_object_del]]
  * [[:develop:api:efl:ui:layout_base:method:widget_sub_object_del|Efl.Ui.Layout_Base.widget_sub_object_del]]

