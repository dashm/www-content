~~Title: Efl.Ui.Widget.scroll_hold_pop~~
====== Efl.Ui.Widget.scroll_hold_pop ======

===== Description =====

%%Pop scroller hold%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:scroll_hold_pop:description&nouser&nolink&nodate}}

===== Signature =====

<code>
scroll_hold_pop @beta {}
</code>

===== C signature =====

<code c>
void efl_ui_widget_scroll_hold_pop(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:scroll_hold_pop|Efl.Ui.Widget.scroll_hold_pop]]

