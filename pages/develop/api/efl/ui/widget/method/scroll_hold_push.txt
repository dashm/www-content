~~Title: Efl.Ui.Widget.scroll_hold_push~~
====== Efl.Ui.Widget.scroll_hold_push ======

===== Description =====

%%Push scroll hold%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:scroll_hold_push:description&nouser&nolink&nodate}}

===== Signature =====

<code>
scroll_hold_push @beta {}
</code>

===== C signature =====

<code c>
void efl_ui_widget_scroll_hold_push(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:scroll_hold_push|Efl.Ui.Widget.scroll_hold_push]]

