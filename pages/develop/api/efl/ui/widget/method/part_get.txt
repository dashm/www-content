~~Title: Efl.Ui.Widget.part_get~~
====== Efl.Ui.Widget.part_get ======

===== Description =====

%%Returns %%[[:develop:api:efl:ui:widget_part|Efl.Ui.Widget_Part]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:part_get:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:part:method:part_get|Efl.Part.part_get]].//===== Signature =====

<code>
part_get @protected @const @pure_virtual {
    params {
        @in name: string;
    }
    return: Efl.Object;
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_part_get(const Eo *obj, const char *name);
</code>

===== Parameters =====

  * **name** //(in)// - %%The part name.%%

===== Implemented by =====

  * [[:develop:api:efl:part:method:part_get|Efl.Part.part_get]]
  * [[:develop:api:efl:canvas:layout:method:part_get|Efl.Canvas.Layout.part_get]]
  * [[:develop:api:efl:ui:widget:method:part_get|Efl.Ui.Widget.part_get]]
  * [[:develop:api:efl:ui:win:method:part_get|Efl.Ui.Win.part_get]]
  * [[:develop:api:efl:ui:flip:method:part_get|Efl.Ui.Flip.part_get]]
  * [[:develop:api:efl:ui:layout_base:method:part_get|Efl.Ui.Layout_Base.part_get]]
  * [[:develop:api:efl:ui:text:method:part_get|Efl.Ui.Text.part_get]]
  * [[:develop:api:efl:ui:default_item:method:part_get|Efl.Ui.Default_Item.part_get]]
  * [[:develop:api:efl:ui:list_placeholder_item:method:part_get|Efl.Ui.List_Placeholder_Item.part_get]]
  * [[:develop:api:efl:ui:textpath:method:part_get|Efl.Ui.Textpath.part_get]]
  * [[:develop:api:efl:ui:panes:method:part_get|Efl.Ui.Panes.part_get]]
  * [[:develop:api:efl:ui:progressbar:method:part_get|Efl.Ui.Progressbar.part_get]]
  * [[:develop:api:efl:ui:popup:method:part_get|Efl.Ui.Popup.part_get]]
  * [[:develop:api:efl:ui:alert_popup:method:part_get|Efl.Ui.Alert_Popup.part_get]]
  * [[:develop:api:efl:ui:navigation_bar:method:part_get|Efl.Ui.Navigation_Bar.part_get]]
  * [[:develop:api:efl:ui:widget_factory:method:part_get|Efl.Ui.Widget_Factory.part_get]]

