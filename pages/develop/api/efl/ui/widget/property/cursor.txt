~~Title: Efl.Ui.Widget.cursor~~
====== Efl.Ui.Widget.cursor ======

===== Description =====

%%The cursor to be shown when mouse is over the object%%

%%This is the cursor that will be displayed when mouse is over the object. The object can have only one cursor set to it so if %%[[:develop:api:efl:ui:widget:property:cursor|Efl.Ui.Widget.cursor.set]]%% is called twice for an object, the previous set will be unset.%%

%%If using X cursors, a definition of all the valid cursor names is listed on Elementary_Cursors.h. If an invalid name is set the default cursor will be used.%%

%%If %%''cursor''%% is set to %%''null''%% it will be reset to the default one.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:cursor:description&nouser&nolink&nodate}}

===== Values =====

  * **cursor** - %%The cursor name, defined either by the display system or the theme.%%

===== Signature =====

<code>
@property cursor @beta {
    get {}
    set {
        return: bool;
    }
    values {
        cursor: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_widget_cursor_get(const Eo *obj);
Eina_Bool efl_ui_widget_cursor_set(Eo *obj, const char *cursor);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:cursor|Efl.Ui.Widget.cursor]]

