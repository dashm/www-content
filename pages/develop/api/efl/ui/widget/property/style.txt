~~Title: Efl.Ui.Widget.style~~
====== Efl.Ui.Widget.style ======

===== Description =====

%%The widget style to use.%%

%%Styles define different look and feel for widgets, and may provide different parts for layout-based widgets. Styles vary from widget to widget and may be defined by other themes by means of extensions and overlays.%%

%%The style can only be set before %%[[:develop:api:efl:object:method:finalize|Efl.Object.finalize]]%%, which means at construction time of the object (inside %%''efl_add''%% in C).%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:style:description&nouser&nolink&nodate}}

===== Values =====

  * **style** - %%Name of the style to use. Refer to each widget's documentation for the available style names, or to the themes in use.%%

===== Signature =====

<code>
@property style {
    get {}
    set {
        return: Eina.Error;
    }
    values {
        style: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_widget_style_get(const Eo *obj);
Eina_Error efl_ui_widget_style_set(Eo *obj, const char *style);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:style|Efl.Ui.Widget.style]]

