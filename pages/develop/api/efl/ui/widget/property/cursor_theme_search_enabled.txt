~~Title: Efl.Ui.Widget.cursor_theme_search_enabled~~
====== Efl.Ui.Widget.cursor_theme_search_enabled ======

===== Description =====

%%Whether the cursor may be looked in the theme or not.%%

%%If %%''false''%%, the cursor may only come from the render engine, i.e. from the display manager.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:cursor_theme_search_enabled:description&nouser&nolink&nodate}}

===== Values =====

  * **allow** - %%Whether to use theme cursors.%%

===== Signature =====

<code>
@property cursor_theme_search_enabled @beta {
    get {}
    set {
        return: bool;
    }
    values {
        allow: bool (true);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_cursor_theme_search_enabled_get(const Eo *obj);
Eina_Bool efl_ui_widget_cursor_theme_search_enabled_set(Eo *obj, Eina_Bool allow);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:cursor_theme_search_enabled|Efl.Ui.Widget.cursor_theme_search_enabled]]

