~~Title: Efl.Ui.Widget.size~~
====== Efl.Ui.Widget.size ======

===== Description =====

%%The 2D size of a canvas object.%%

//Since 1.22//


{{page>:develop:api-include:efl:ui:widget:property:size:description&nouser&nolink&nodate}}

===== Values =====

  * **size** - %%A 2D size in pixel units.%%
==== Getter ====

%%Retrieves the (rectangular) size of the given Evas object.%%

//Since 1.22//


{{page>:develop:api-include:efl:ui:widget:property:size:getter_description&nouser&nolink&nodate}}

==== Setter ====

%%Changes the size of the given object.%%

%%Note that setting the actual size of an object might be the job of its container, so this function might have no effect. Look at %%[[:develop:api:efl:gfx:hint|Efl.Gfx.Hint]]%% instead, when manipulating widgets.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:size:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:canvas:object:property:size|Efl.Gfx.Entity.size]] **(set)**.//===== Signature =====

<code>
@property size @pure_virtual {
    get {}
    set {}
    values {
        size: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_entity_size_get(const Eo *obj);
void efl_gfx_entity_size_set(Eo *obj, Eina_Size2D size);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:entity:property:size|Efl.Gfx.Entity.size]]
  * [[:develop:api:efl:canvas:vg:node:property:size|Efl.Canvas.Vg.Node.size]]
  * [[:develop:api:efl:ui:win:property:size|Efl.Ui.Win.size]]
  * [[:develop:api:efl:ui:image:property:size|Efl.Ui.Image.size]]
  * [[:develop:api:efl:ui:image_zoomable:property:size|Efl.Ui.Image_Zoomable.size]]
  * [[:develop:api:efl:ui:widget:property:size|Efl.Ui.Widget.size]]
  * [[:develop:api:efl:ui:table:property:size|Efl.Ui.Table.size]]
  * [[:develop:api:efl:ui:box:property:size|Efl.Ui.Box.size]]
  * [[:develop:api:efl:ui:animation_view:property:size|Efl.Ui.Animation_View.size]]
  * [[:develop:api:efl:ui:text:property:size|Efl.Ui.Text.size]]
  * [[:develop:api:efl:ui:panel:property:size|Efl.Ui.Panel.size]]
  * [[:develop:api:efl:ui:textpath:property:size|Efl.Ui.Textpath.size]]
  * [[:develop:api:efl:ui:popup:property:size|Efl.Ui.Popup.size]]
  * [[:develop:api:efl:ui:relative_layout:property:size|Efl.Ui.Relative_Layout.size]]
  * [[:develop:api:efl:canvas:object:property:size|Efl.Canvas.Object.size]]
  * [[:develop:api:efl:canvas:video:property:size|Efl.Canvas.Video.size]]
  * [[:develop:api:efl:canvas:event_grabber:property:size|Efl.Canvas.Event_Grabber.size]]
  * [[:develop:api:efl:canvas:layout:property:size|Efl.Canvas.Layout.size]]
  * [[:develop:api:efl:ui:pan:property:size|Efl.Ui.Pan.size]]
  * [[:develop:api:efl:ui:image_zoomable_pan:property:size|Efl.Ui.Image_Zoomable_Pan.size]]

