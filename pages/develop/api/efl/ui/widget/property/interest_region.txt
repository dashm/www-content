~~Title: Efl.Ui.Widget.interest_region~~
====== Efl.Ui.Widget.interest_region ======

===== Values =====

  * **region** - %%The relative region to show. If width or height is <= 0 it will be ignored, and no action will be taken.%%


\\ {{page>:develop:api-include:efl:ui:widget:property:interest_region:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property interest_region {
    get @protected {}
    values {
        region: Eina.Rect;
    }
}
</code>

===== C signature =====

<code c>
Eina_Rect efl_ui_widget_interest_region_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:interest_region|Efl.Ui.Widget.interest_region]]
  * [[:develop:api:efl:ui:text:property:interest_region|Efl.Ui.Text.interest_region]]
  * [[:develop:api:efl:ui:panel:property:interest_region|Efl.Ui.Panel.interest_region]]

