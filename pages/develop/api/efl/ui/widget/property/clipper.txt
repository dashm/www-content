~~Title: Efl.Ui.Widget.clipper~~
====== Efl.Ui.Widget.clipper ======

===== Description =====

%%Clip one object to another.%%

%%This property will clip the object %%''obj''%% to the area occupied by the object %%''clip''%%. This means the object %%''obj''%% will only be visible within the area occupied by the clipping object (%%''clip''%%).%%

%%The color of the object being clipped will be multiplied by the color of the clipping one, so the resulting color for the former will be "RESULT = (OBJ * CLIP) / (255 * 255)", per color element (red, green, blue and alpha).%%

%%Clipping is recursive, so clipping objects may be clipped by others, and their color will in term be multiplied. You may not set up circular clipping lists (i.e. object 1 clips object 2, which clips object 1): the behavior of Evas is undefined in this case.%%

%%Objects which do not clip others are visible in the canvas as normal; those that clip one or more objects become invisible themselves, only affecting what they clip. If an object ceases to have other objects being clipped by it, it will become visible again.%%

%%The visibility of an object affects the objects that are clipped by it, so if the object clipping others is not shown (as in %%[[:develop:api:efl:gfx:entity:property:visible|Efl.Gfx.Entity.visible]]%%), the objects clipped by it will not be shown  either.%%

%%If %%''obj''%% was being clipped by another object when this function is  called, it gets implicitly removed from the old clipper's domain and is made now to be clipped by its new clipper.%%

%%If %%''clip''%% is %%''null''%%, this call will disable clipping for the object i.e. its visibility and color get detached from the previous clipper. If it wasn't, this has no effect.%%

<note>
%%Only rectangle and image (masks) objects can be used as clippers. Anything else will result in undefined behaviour.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:clipper:description&nouser&nolink&nodate}}

===== Values =====

  * **clipper** - %%The object to clip %%''obj''%% by.%%

//Overridden from [[:develop:api:efl:canvas:group:property:clipper|Efl.Canvas.Object.clipper]] **(set)**.//===== Signature =====

<code>
@property clipper {
    get {}
    set {}
    values {
        clipper: Efl.Canvas.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Canvas_Object *efl_canvas_object_clipper_get(const Eo *obj);
void efl_canvas_object_clipper_set(Eo *obj, Efl_Canvas_Object *clipper);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:clipper|Efl.Canvas.Object.clipper]]
  * [[:develop:api:efl:canvas:group:property:clipper|Efl.Canvas.Group.clipper]]
  * [[:develop:api:efl:ui:widget:property:clipper|Efl.Ui.Widget.clipper]]
  * [[:develop:api:efl:ui:image:property:clipper|Efl.Ui.Image.clipper]]

