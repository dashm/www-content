~~Title: Efl.Ui.Box.pack_begin~~
====== Efl.Ui.Box.pack_begin ======

===== Description =====

%%Prepend an object at the beginning of this container.%%

%%This is the same as %%[[:develop:api:efl:pack_linear:method:pack_at|Efl.Pack_Linear.pack_at]]%% with a %%''0''%% index.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:box:method:pack_begin:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_begin|Efl.Pack_Linear.pack_begin]].//===== Signature =====

<code>
pack_begin @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_begin(Eo *obj, Efl_Gfx_Entity *subobj);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%Object to pack at the beginning.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_begin|Efl.Pack_Linear.pack_begin]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_begin|Efl.Ui.Tab_Bar.pack_begin]]
  * [[:develop:api:efl:ui:flip:method:pack_begin|Efl.Ui.Flip.pack_begin]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_begin|Efl.Canvas.Layout_Part_Box.pack_begin]]
  * [[:develop:api:efl:ui:box:method:pack_begin|Efl.Ui.Box.pack_begin]]
  * [[:develop:api:efl:ui:radio_box:method:pack_begin|Efl.Ui.Radio_Box.pack_begin]]
  * [[:develop:api:efl:ui:group_item:method:pack_begin|Efl.Ui.Group_Item.pack_begin]]
  * [[:develop:api:efl:ui:collection:method:pack_begin|Efl.Ui.Collection.pack_begin]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_begin|Efl.Ui.Layout_Part_Box.pack_begin]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_begin|Efl.Canvas.Layout_Part_Invalid.pack_begin]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_begin|Efl.Ui.Spotlight.Container.pack_begin]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_begin|Efl.Ui.Tab_Pager.pack_begin]]

