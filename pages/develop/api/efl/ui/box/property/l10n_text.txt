~~Title: Efl.Ui.Box.l10n_text~~
====== Efl.Ui.Box.l10n_text ======

===== Description =====

%%A unique string to be translated.%%

%%Often this will be a human-readable string (e.g. in English) but it can also be a unique string identifier that must then be translated to the current locale with %%''dgettext''%%() or any similar mechanism.%%

%%Setting this property will enable translation for this object or part.%%


{{page>:develop:api-include:efl:ui:box:property:l10n_text:description&nouser&nolink&nodate}}

===== Values =====

==== Getter ====

  * **domain** - %%A translation domain. If %%''null''%% this means the default domain is used.%%
==== Setter ====

  * **label** - %%A unique (untranslated) string.%%
  * **domain** - %%A translation domain. If %%''null''%% this uses the default domain (eg. set by %%''textdomain''%%()).%%
==== Setter ====

%%Sets the new untranslated string and domain for this object.%%
{{page>:develop:api-include:efl:ui:box:property:l10n_text:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:ui:l10n:property:l10n_text|Efl.Ui.L10n.l10n_text]] **(get, set)**.//===== Signature =====

<code>
@property l10n_text @pure_virtual {
    get {
        values {
            domain: string @optional;
        }
        return: string;
    }
    set {
        values {
            label: string;
            domain: string @optional;
        }
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_l10n_text_get(const Eo *obj, const char **domain);
void efl_ui_l10n_text_set(Eo *obj, const char *label, const char *domain);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:l10n:property:l10n_text|Efl.Ui.L10n.l10n_text]]
  * [[:develop:api:efl:ui:default_item:property:l10n_text|Efl.Ui.Default_Item.l10n_text]]
  * [[:develop:api:efl:ui:layout_part_legacy:property:l10n_text|Efl.Ui.Layout_Part_Legacy.l10n_text]]
  * [[:develop:api:elm:notify:part:property:l10n_text|Elm.Notify.Part.l10n_text]]
  * [[:develop:api:efl:ui:layout_part_text:property:l10n_text|Efl.Ui.Layout_Part_Text.l10n_text]]
  * [[:develop:api:efl:ui:win:property:l10n_text|Efl.Ui.Win.l10n_text]]
  * [[:develop:api:efl:ui:table:property:l10n_text|Efl.Ui.Table.l10n_text]]
  * [[:develop:api:efl:ui:box:property:l10n_text|Efl.Ui.Box.l10n_text]]
  * [[:develop:api:efl:ui:layout_base:property:l10n_text|Efl.Ui.Layout_Base.l10n_text]]
  * [[:develop:api:efl:ui:check:property:l10n_text|Efl.Ui.Check.l10n_text]]
  * [[:develop:api:efl:ui:frame:property:l10n_text|Efl.Ui.Frame.l10n_text]]
  * [[:develop:api:efl:ui:progressbar:property:l10n_text|Efl.Ui.Progressbar.l10n_text]]
  * [[:develop:api:efl:ui:button:property:l10n_text|Efl.Ui.Button.l10n_text]]
  * [[:develop:api:efl:ui:navigation_bar:property:l10n_text|Efl.Ui.Navigation_Bar.l10n_text]]

