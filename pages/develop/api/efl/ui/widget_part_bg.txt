~~Title: Efl.Ui.Widget_Part_Bg~~
====== Efl.Ui.Widget_Part_Bg (class) ======

===== Description =====

%%Elementary widget internal part background class%%

%%This part will proxy the calls on it to the %%[[:develop:api:efl:ui:bg|Efl.Ui.Bg]]%% internal object of this widget. This internal object is stacked below the %%[[:develop:api:efl:ui:widget:property:resize_object|Efl.Ui.Widget.resize_object]]%% and co-located with the widget.%%

%%All %%[[:develop:api:efl:ui:widget|Efl.Ui.Widget]]%% objects have this part, allowing the background of the widget to be customized.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:widget_part_bg:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:ui:widget_part|Efl.Ui.Widget_Part]] //(class)// => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:ui:widget_part|Efl.Ui.Widget_Part]] //(class)//
    * [[:develop:api:efl:object|Efl.Object]] //(class)//
    * [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]] //(interface)//
  * [[:develop:api:efl:file|Efl.File]] //(mixin)//
  * [[:develop:api:efl:gfx:color|Efl.Gfx.Color]] //(mixin)//
  * [[:develop:api:efl:gfx:image|Efl.Gfx.Image]] //(interface)//


++++
===== Members =====

**[[:develop:api:efl:ui:widget_part_bg:property:color|color]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:gfx:color|Efl.Gfx.Color]]]//\\
> %%The general/main color of the given Evas object.%%
<code c>
void efl_gfx_color_get(const Eo *obj, int *r, int *g, int *b, int *a);
void efl_gfx_color_set(Eo *obj, int r, int g, int b, int a);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:property:file|file]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:file|Efl.File]]]//\\
> %%The file path from where an object will fetch the data.%%
<code c>
const char *efl_file_get(const Eo *obj);
Eina_Error efl_file_set(Eo *obj, const char *file);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:method:finalize|finalize]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%%
<code c>
Efl_Object *efl_finalize(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:property:key|key]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:file|Efl.File]]]//\\
> %%The key which corresponds to the target data within a file.%%
<code c>
const char *efl_file_key_get(const Eo *obj);
void efl_file_key_set(Eo *obj, const char *key);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:method:load|load]]**// [Overridden from [[:develop:api:efl:file|Efl.File]]]//\\
> %%Perform all necessary operations to open and load file data into the object using the %%[[:develop:api:efl:file:property:file|Efl.File.file]]%% (or %%[[:develop:api:efl:file:property:mmap|Efl.File.mmap]]%%) and %%[[:develop:api:efl:file:property:key|Efl.File.key]]%% properties.%%
<code c>
Eina_Error efl_file_load(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:property:mmap|mmap]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:file|Efl.File]]]//\\
> %%The mmaped file from where an object will fetch the real data (it must be an %%[[:develop:api:eina:file|Eina.File]]%%).%%
<code c>
const Eina_File efl_file_mmap_get(const Eo *obj);
Eina_Error efl_file_mmap_set(Eo *obj, const Eina_File f);
</code>
\\
**[[:develop:api:efl:ui:widget_part_bg:method:unload|unload]]**// [Overridden from [[:develop:api:efl:file|Efl.File]]]//\\
> %%Perform all necessary operations to unload file data from the object.%%
<code c>
void efl_file_unload(Eo *obj);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:file|Efl.File]] ^^^
|  | **[[:develop:api:efl:file:property:loaded|loaded]]** //**(get)**// |  |
^ [[:develop:api:efl:gfx:color|Efl.Gfx.Color]] ^^^
|  | **[[:develop:api:efl:gfx:color:property:color_code|color_code]]** //**(get, set)**// | %%Hexadecimal color code of given Evas object (#RRGGBBAA).%% |
^ [[:develop:api:efl:gfx:image|Efl.Gfx.Image]] ^^^
|  | **[[:develop:api:efl:gfx:image:property:border_insets|border_insets]]** //**(get, set)**// | %%Dimensions of this image's border, a region that does not scale with the center area.%% |
|  | **[[:develop:api:efl:gfx:image:property:border_insets_scale|border_insets_scale]]** //**(get, set)**// | %%Scaling factor applied to the image borders.%% |
|  | **[[:develop:api:efl:gfx:image:property:can_downscale|can_downscale]]** //**(get, set)**// | %%If %%''true''%%, the image may be scaled to a smaller size. If %%''false''%%, the image will never be resized smaller than its native size.%% |
|  | **[[:develop:api:efl:gfx:image:property:can_upscale|can_upscale]]** //**(get, set)**// | %%If %%''true''%%, the image may be scaled to a larger size. If %%''false''%%, the image will never be resized larger than its native size.%% |
|  | **[[:develop:api:efl:gfx:image:property:center_fill_mode|center_fill_mode]]** //**(get, set)**// | %%Specifies how the center part of the object (not the borders) should be drawn when EFL is rendering it.%% |
|  | **[[:develop:api:efl:gfx:image:property:content_hint|content_hint]]** //**(get, set)**// | %%Content hint setting for the image. These hints might be used by EFL to enable optimizations.%% |
|  | **[[:develop:api:efl:gfx:image:property:content_region|content_region]]** //**(get)**// |  |
|  | **[[:develop:api:efl:gfx:image:property:image_load_error|image_load_error]]** //**(get)**// |  |
|  | **[[:develop:api:efl:gfx:image:property:image_size|image_size]]** //**(get)**// |  |
|  | **[[:develop:api:efl:gfx:image:property:ratio|ratio]]** //**(get)**// |  |
|  | **[[:develop:api:efl:gfx:image:property:scale_hint|scale_hint]]** //**(get, set)**// | %%The scale hint of a given image of the canvas.%% |
|  | **[[:develop:api:efl:gfx:image:property:scale_method|scale_method]]** //**(get, set)**// | %%Determine how the image is scaled at render time.%% |
|  | **[[:develop:api:efl:gfx:image:property:smooth_scale|smooth_scale]]** //**(get, set)**// | %%Whether to use high-quality image scaling algorithm for this image.%% |
|  | **[[:develop:api:efl:gfx:image:property:stretch_region|stretch_region]]** //**(get, set)**// | %%This property defines the stretchable pixels region of an image.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:constructor|constructor]]** | %%Implement this method to provide optional initialization code for your object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:method:invalidate|invalidate]]** | %%Implement this method to perform special actions when your object loses its parent, if you need to.%% |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:property:parent|parent]]** //**(get, set)**// | %%The parent of an object.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
^ [[:develop:api:efl:ui:widget_part|Efl.Ui.Widget_Part]] ^^^
|  | **[[:develop:api:efl:ui:widget_part:method:destructor|destructor]]** | %%Implement this method to provide deinitialization code for your object if you need it.%% |
|  | **[[:develop:api:efl:ui:widget_part:method:property_bind|property_bind]]** | %%bind property data with the given key string. when the data is ready or changed, bind the data to the key action and process promised work.%% |

===== Events =====

==== Inherited ====

^ [[:develop:api:efl:gfx:image|Efl.Gfx.Image]] ^^^
|  | **[[:develop:api:efl:gfx:image:event:image_preload_state_changed|image,preload_state,changed]]** | %%If %%''true''%%, image data has been preloaded and can be displayed. If %%''false''%%, the image data has been unloaded and can no longer be displayed.%% |
|  | **[[:develop:api:efl:gfx:image:event:image_resized|image,resized]]** | %%Image was resized (its pixel data). The event data is the image's new size.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
^ [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]] ^^^
|  | **[[:develop:api:efl:ui:property_bind:event:properties_changed|properties,changed]]** | %%Event dispatched when a property on the object has changed due to a user interaction on the object that a model could be interested in.%% |
|  | **[[:develop:api:efl:ui:property_bind:event:property_bound|property,bound]]** | %%Event dispatched when a property on the object is bound to a model. This is useful to avoid generating too many events.%% |
