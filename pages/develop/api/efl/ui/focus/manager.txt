~~Title: Efl.Ui.Focus.Manager~~
====== Efl.Ui.Focus.Manager (interface) ======

===== Description =====

%%Interface for managing focus objects.%%

%%This interface is built in order to support movement of the focus property in a set of widgets. The movement of the focus property can happen in a tree manner, or a graph manner. The movement is also keeping track of the history of focused elements. The tree interpretation differentiates between logical and regular widgets: Logical widgets (typically containers) cannot receive focus, whereas Regular ones (like buttons) can.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:manager:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:focus:manager:property:border_elements|border_elements]]** //**(get)**//\\
> 
<code c>
Eina_Iterator *efl_ui_focus_manager_border_elements_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:dirty_logic_freeze|dirty_logic_freeze]]**\\
> %%Disables the cache invalidation when an object is moved.%%
<code c>
void efl_ui_focus_manager_dirty_logic_freeze(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:dirty_logic_unfreeze|dirty_logic_unfreeze]]**\\
> %%Enables the cache invalidation when an object is moved.%%
<code c>
void efl_ui_focus_manager_dirty_logic_unfreeze(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:fetch|fetch]]**\\
> %%Fetches the data from a registered node.%%
<code c>
Efl_Ui_Focus_Relations *efl_ui_focus_manager_fetch(Eo *obj, Efl_Ui_Focus_Object *child);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:logical_end|logical_end]]**\\
> %%Returns the last logical object.%%
<code c>
Efl_Ui_Focus_Manager_Logical_End_Detail efl_ui_focus_manager_logical_end(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:property:manager_focus|manager_focus]]** //**(get, set)**//\\
> %%The element which is currently focused by this manager.%%
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_focus_get(const Eo *obj);
void efl_ui_focus_manager_focus_set(Eo *obj, Efl_Ui_Focus_Object *focus);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:move|move]]**\\
> %%Moves the focus in the given direction to the next regular widget.%%
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_move(Eo *obj, Efl_Ui_Focus_Direction direction);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:pop_history_stack|pop_history_stack]]**\\
> %%Removes the uppermost history element, and focuses the previous one.%%
<code c>
void efl_ui_focus_manager_pop_history_stack(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:property:redirect|redirect]]** //**(get, set)**//\\
> %%Add another manager to serve the move requests.%%
<code c>
Efl_Ui_Focus_Manager *efl_ui_focus_manager_redirect_get(const Eo *obj);
void efl_ui_focus_manager_redirect_set(Eo *obj, Efl_Ui_Focus_Manager *redirect);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:request_move|request_move]]**\\
> %%Returns the object in the %%''direction''%% from %%''child''%%.%%
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_request_move(Eo *obj, Efl_Ui_Focus_Direction direction, Efl_Ui_Focus_Object *child, Eina_Bool logical);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:request_subchild|request_subchild]]**\\
> %%Returns the widget in the direction next.%%
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_request_subchild(Eo *obj, Efl_Ui_Focus_Object *root);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:reset_history|reset_history]]**\\
> %%Resets the history stack of this manager object. This means the uppermost element will be unfocused, and all other elements will be removed from the remembered list.%%
<code c>
void efl_ui_focus_manager_reset_history(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:property:root|root]]** //**(get, set)**//\\
> %%Root node for all logical sub-trees.%%
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_root_get(const Eo *obj);
Eina_Bool efl_ui_focus_manager_root_set(Eo *obj, Efl_Ui_Focus_Object *root);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:method:setup_on_first_touch|setup_on_first_touch]]**\\
> %%Called when this manager is set as redirect.%%
<code c>
void efl_ui_focus_manager_setup_on_first_touch(Eo *obj, Efl_Ui_Focus_Direction direction, Efl_Ui_Focus_Object *entry);
</code>
\\
**[[:develop:api:efl:ui:focus:manager:property:viewport_elements|viewport_elements]]** //**(get)**//\\
> 
<code c>
Eina_Iterator *efl_ui_focus_manager_viewport_elements_get(const Eo *obj, Eina_Rect viewport);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:focus:manager:event:coords_dirty|coords,dirty]]**\\
> %%Cached relationship calculation results have been invalidated.%%
<code c>
EFL_UI_FOCUS_MANAGER_EVENT_COORDS_DIRTY(void)
</code>
\\ **[[:develop:api:efl:ui:focus:manager:event:dirty_logic_freeze_changed|dirty_logic_freeze,changed]]**\\
> %%Called when this focus manager is frozen or thawed, even_info being %%''true''%% indicates that it is now frozen, %%''false''%% indicates that it is thawed.%%
<code c>
EFL_UI_FOCUS_MANAGER_EVENT_DIRTY_LOGIC_FREEZE_CHANGED(Eina_Bool)
</code>
\\ **[[:develop:api:efl:ui:focus:manager:event:flush_pre|flush,pre]]**\\
> %%After this event, the manager object will calculate relations in the graph. Can be used to add / remove children in a lazy fashion.%%
<code c>
EFL_UI_FOCUS_MANAGER_EVENT_FLUSH_PRE(void)
</code>
\\ **[[:develop:api:efl:ui:focus:manager:event:manager_focus_changed|manager_focus,changed]]**\\
> %%The manager_focus property has changed. The previously focused object is passed as an event argument.%%
<code c>
EFL_UI_FOCUS_MANAGER_EVENT_MANAGER_FOCUS_CHANGED(Efl_Ui_Focus_Object *)
</code>
\\ **[[:develop:api:efl:ui:focus:manager:event:redirect_changed|redirect,changed]]**\\
> %%Redirect object has changed, the old manager is passed as an event argument.%%
<code c>
EFL_UI_FOCUS_MANAGER_EVENT_REDIRECT_CHANGED(Efl_Ui_Focus_Manager *)
</code>
\\ 