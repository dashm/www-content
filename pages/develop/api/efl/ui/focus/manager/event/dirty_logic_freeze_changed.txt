~~Title: Efl.Ui.Focus.Manager: dirty_logic_freeze,changed~~

===== Description =====

%%Called when this focus manager is frozen or thawed, even_info being %%''true''%% indicates that it is now frozen, %%''false''%% indicates that it is thawed.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:manager:event:dirty_logic_freeze_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
dirty_logic_freeze,changed: bool;
</code>

===== C information =====

<code c>
EFL_UI_FOCUS_MANAGER_EVENT_DIRTY_LOGIC_FREEZE_CHANGED(Eina_Bool)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_focus_manager_event_dirty_logic_freeze_changed(void *data, const Efl_Event *event)
{
    Eina_Bool info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_FOCUS_MANAGER_EVENT_DIRTY_LOGIC_FREEZE_CHANGED, on_efl_ui_focus_manager_event_dirty_logic_freeze_changed, d);
}

</code>
