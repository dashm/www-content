~~Title: Efl.Ui.Focus.Manager.logical_end~~
====== Efl.Ui.Focus.Manager.logical_end ======

===== Description =====

%%Returns the last logical object.%%

%%The returned object is the last object that would be returned if you start at the root and move in the "next" direction.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:logical_end:description&nouser&nolink&nodate}}

===== Signature =====

<code>
logical_end @pure_virtual {
    return: Efl.Ui.Focus.Manager_Logical_End_Detail;
}
</code>

===== C signature =====

<code c>
Efl_Ui_Focus_Manager_Logical_End_Detail efl_ui_focus_manager_logical_end(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:logical_end|Efl.Ui.Focus.Manager.logical_end]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:logical_end|Efl.Ui.Focus.Manager_Calc.logical_end]]
  * [[:develop:api:efl:ui:focus:manager_root_focus:method:logical_end|Efl.Ui.Focus.Manager_Root_Focus.logical_end]]

