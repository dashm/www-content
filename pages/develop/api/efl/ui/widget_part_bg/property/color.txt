~~Title: Efl.Ui.Widget_Part_Bg.color~~
====== Efl.Ui.Widget_Part_Bg.color ======

===== Description =====

%%The general/main color of the given Evas object.%%

%%Represents the main color's RGB component (and alpha channel) values, which range from 0 to 255. For the alpha channel, which defines the object's transparency level, 0 means totally transparent, while 255 means opaque. These color values are premultiplied by the alpha value.%%

%%Usually you'll use this attribute for text and rectangle objects, where the main color is the only color. If set for objects which themselves have colors, like the images one, those colors get modulated by this one.%%

%%All newly created Evas rectangles get the default color values of 255 255 255 255 (opaque white).%%

%%When reading this property, use %%''NULL''%% pointers on the components you're not interested in and they'll be ignored by the function.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget_part_bg:property:color:description&nouser&nolink&nodate}}

===== Values =====

  * **r** - No description supplied.
  * **g** - No description supplied.
  * **b** - No description supplied.
  * **a** - No description supplied.

//Overridden from [[:develop:api:efl:gfx:color:property:color|Efl.Gfx.Color.color]] **(get, set)**.//===== Signature =====

<code>
@property color @pure_virtual {
    get {}
    set {}
    values {
        r: int;
        g: int;
        b: int;
        a: int;
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_color_get(const Eo *obj, int *r, int *g, int *b, int *a);
void efl_gfx_color_set(Eo *obj, int r, int g, int b, int a);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:color:property:color|Efl.Gfx.Color.color]]
  * [[:develop:api:efl:canvas:vg:node:property:color|Efl.Canvas.Vg.Node.color]]
  * [[:develop:api:efl:ui:win_part:property:color|Efl.Ui.Win_Part.color]]
  * [[:develop:api:efl:ui:widget_part_shadow:property:color|Efl.Ui.Widget_Part_Shadow.color]]
  * [[:develop:api:efl:canvas:object:property:color|Efl.Canvas.Object.color]]
  * [[:develop:api:efl:canvas:group:property:color|Efl.Canvas.Group.color]]
  * [[:develop:api:efl:ui:widget:property:color|Efl.Ui.Widget.color]]
  * [[:develop:api:efl:ui:image:property:color|Efl.Ui.Image.color]]
  * [[:develop:api:efl:ui:bg:property:color|Efl.Ui.Bg.color]]
  * [[:develop:api:efl:ui:widget_part_bg:property:color|Efl.Ui.Widget_Part_Bg.color]]

