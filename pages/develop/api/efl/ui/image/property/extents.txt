~~Title: Efl.Ui.Image.extents~~
====== Efl.Ui.Image.extents ======

===== Description =====

%%Geometry of accessible widget.%%
{{page>:develop:api-include:efl:ui:image:property:extents:description&nouser&nolink&nodate}}

===== Keys =====

  * **screen_coords** - %%If %%''true''%% x and y values will be relative to screen origin, otherwise relative to canvas%%
===== Values =====

  * **rect** - %%The geometry.%%

//Overridden from [[:develop:api:efl:access:component:property:extents|Efl.Access.Component.extents]] **(get)**.//===== Signature =====

<code>
@property extents @beta @protected {
    get {}
    set {
        return: bool;
    }
    keys {
        screen_coords: bool;
    }
    values {
        rect: Eina.Rect;
    }
}
</code>

===== C signature =====

<code c>
Eina_Rect efl_access_component_extents_get(const Eo *obj, Eina_Bool screen_coords);
Eina_Bool efl_access_component_extents_set(Eo *obj, Eina_Bool screen_coords, Eina_Rect rect);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:component:property:extents|Efl.Access.Component.extents]]
  * [[:develop:api:efl:ui:win:property:extents|Efl.Ui.Win.extents]]
  * [[:develop:api:efl:ui:image:property:extents|Efl.Ui.Image.extents]]

