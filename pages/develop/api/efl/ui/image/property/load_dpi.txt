~~Title: Efl.Ui.Image.load_dpi~~
====== Efl.Ui.Image.load_dpi ======

===== Description =====

%%The DPI resolution of an image object's source image.%%

%%Most useful for the SVG image loader.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:load_dpi:description&nouser&nolink&nodate}}

===== Values =====

  * **dpi** - %%The DPI resolution.%%

//Overridden from [[:develop:api:efl:gfx:image_load_controller:property:load_dpi|Efl.Gfx.Image_Load_Controller.load_dpi]] **(get, set)**.//===== Signature =====

<code>
@property load_dpi @pure_virtual {
    get {}
    set {}
    values {
        dpi: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_gfx_image_load_controller_load_dpi_get(const Eo *obj);
void efl_gfx_image_load_controller_load_dpi_set(Eo *obj, double dpi);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image_load_controller:property:load_dpi|Efl.Gfx.Image_Load_Controller.load_dpi]]
  * [[:develop:api:efl:canvas:image:property:load_dpi|Efl.Canvas.Image.load_dpi]]
  * [[:develop:api:efl:ui:image:property:load_dpi|Efl.Ui.Image.load_dpi]]

