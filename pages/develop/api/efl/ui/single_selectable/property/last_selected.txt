~~Title: Efl.Ui.Single_Selectable.last_selected~~
====== Efl.Ui.Single_Selectable.last_selected ======

===== Values =====

  * **selectable** - %%The latest selected item.%%


\\ {{page>:develop:api-include:efl:ui:single_selectable:property:last_selected:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property last_selected @pure_virtual {
    get {}
    values {
        selectable: Efl.Ui.Selectable;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Selectable *efl_ui_selectable_last_selected_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:single_selectable:property:last_selected|Efl.Ui.Single_Selectable.last_selected]]
  * [[:develop:api:efl:ui:collection:property:last_selected|Efl.Ui.Collection.last_selected]]
  * [[:develop:api:efl:ui:select_model:property:last_selected|Efl.Ui.Select_Model.last_selected]]
  * [[:develop:api:efl:ui:tab_bar:property:last_selected|Efl.Ui.Tab_Bar.last_selected]]
  * [[:develop:api:efl:ui:radio_group_impl:property:last_selected|Efl.Ui.Radio_Group_Impl.last_selected]]

