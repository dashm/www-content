~~Title: Efl.Ui.Item.selected~~
====== Efl.Ui.Item.selected ======

===== Description =====

%%The selected state of this object%%

%%A change to this property emits the changed event.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:item:property:selected:description&nouser&nolink&nodate}}

===== Values =====

  * **selected** - %%The selected state of this object.%%

//Overridden from [[:develop:api:efl:ui:selectable:property:selected|Efl.Ui.Selectable.selected]] **(get, set)**.//===== Signature =====

<code>
@property selected @pure_virtual {
    get {}
    set {}
    values {
        selected: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_selectable_selected_get(const Eo *obj);
void efl_ui_selectable_selected_set(Eo *obj, Eina_Bool selected);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:selectable:property:selected|Efl.Ui.Selectable.selected]]
  * [[:develop:api:efl:ui:check:property:selected|Efl.Ui.Check.selected]]
  * [[:develop:api:efl:ui:radio:property:selected|Efl.Ui.Radio.selected]]
  * [[:develop:api:efl:ui:select_model:property:selected|Efl.Ui.Select_Model.selected]]
  * [[:develop:api:efl:ui:item:property:selected|Efl.Ui.Item.selected]]

