~~Title: Efl.Ui.View_Model.property_logic_add~~
====== Efl.Ui.View_Model.property_logic_add ======

===== Description =====

%%Add callbacks that will be triggered when someone ask for the specified property name when getting or setting a property.%%

%%A get or set should at least be provided for this call to succeed.%%

%%See %%[[:develop:api:efl:ui:view_model:method:property_logic_del|Efl.Ui.View_Model.property_logic_del]]%%%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:view_model:method:property_logic_add:description&nouser&nolink&nodate}}

===== Signature =====

<code>
property_logic_add @beta {
    params {
        @in property: string;
        @in get: EflUiViewModelPropertyGet;
        @in set: EflUiViewModelPropertySet;
        @in binded: iterator<string>;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_ui_view_model_property_logic_add(Eo *obj, const char *property, EflUiViewModelPropertyGet get, EflUiViewModelPropertySet set, Eina_Iterator *binded);
</code>

===== Parameters =====

  * **property** //(in)// - %%The property to bind on to.%%
  * **get** //(in)// - %%Define the get callback called when the %%[[:develop:api:efl:model:property:property|Efl.Model.property.get]]%% is called with the above property name.%%
  * **set** //(in)// - %%Define the set callback called when the %%[[:develop:api:efl:model:property:property|Efl.Model.property.set]]%% is called with the above property name.%%
  * **binded** //(in)// - %%Iterator of property name to bind with this defined property see %%[[:develop:api:efl:ui:view_model:method:property_bind|Efl.Ui.View_Model.property_bind]]%%.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:view_model:method:property_logic_add|Efl.Ui.View_Model.property_logic_add]]

