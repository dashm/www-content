~~Title: Efl.Ui.View_Model.children_bind~~
====== Efl.Ui.View_Model.children_bind ======

===== Description =====

%%Define if we will intercept all children object reference and bind them through the ViewModel with the same property logic as this one. Be careful of recursivity.%%

%%This can only be applied at construction time.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:view_model:property:children_bind:description&nouser&nolink&nodate}}

===== Values =====

  * **enable** - %%Do you automatically bind children. Default to true.%%

===== Signature =====

<code>
@property children_bind {
    get {}
    set {}
    values {
        enable: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_view_model_children_bind_get(const Eo *obj);
void efl_ui_view_model_children_bind_set(Eo *obj, Eina_Bool enable);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:view_model:property:children_bind|Efl.Ui.View_Model.children_bind]]

