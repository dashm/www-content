~~Title: Efl.Ui.Scrollable.step_size~~
====== Efl.Ui.Scrollable.step_size ======

===== Description =====

%%Amount to scroll in response to cursor key presses.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:property:step_size:description&nouser&nolink&nodate}}

===== Values =====

  * **step** - %%The step size in pixels.%%

===== Signature =====

<code>
@property step_size @pure_virtual {
    get {}
    set {}
    values {
        step: Eina.Position2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Position2D efl_ui_scrollable_step_size_get(const Eo *obj);
void efl_ui_scrollable_step_size_set(Eo *obj, Eina_Position2D step);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:step_size|Efl.Ui.Scrollable.step_size]]
  * [[:develop:api:efl:ui:scroll:manager:property:step_size|Efl.Ui.Scroll.Manager.step_size]]

