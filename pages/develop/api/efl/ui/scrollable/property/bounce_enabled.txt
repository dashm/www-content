~~Title: Efl.Ui.Scrollable.bounce_enabled~~
====== Efl.Ui.Scrollable.bounce_enabled ======

===== Description =====

%%When scrolling, the scroller may "bounce" when reaching the edge of the content object. This is a visual way to indicate the end has been reached. This is enabled by default for both axes. This property determines if bouncing is enabled in each axis. When bouncing is disabled, scrolling just stops upon reaching the end of the content.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:property:bounce_enabled:description&nouser&nolink&nodate}}

===== Values =====

  * **horiz** - %%Horizontal bouncing is enabled.%%
  * **vert** - %%Vertical bouncing is enabled.%%

===== Signature =====

<code>
@property bounce_enabled @pure_virtual {
    get {}
    set {}
    values {
        horiz: bool;
        vert: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_scrollable_bounce_enabled_get(const Eo *obj, Eina_Bool *horiz, Eina_Bool *vert);
void efl_ui_scrollable_bounce_enabled_set(Eo *obj, Eina_Bool horiz, Eina_Bool vert);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:bounce_enabled|Efl.Ui.Scrollable.bounce_enabled]]
  * [[:develop:api:efl:ui:scroll:manager:property:bounce_enabled|Efl.Ui.Scroll.Manager.bounce_enabled]]

