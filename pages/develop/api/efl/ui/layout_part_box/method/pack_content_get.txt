~~Title: Efl.Ui.Layout_Part_Box.pack_content_get~~
====== Efl.Ui.Layout_Part_Box.pack_content_get ======

===== Description =====

%%Sub-object at a given %%''index''%% in this container.%%

%%%%''index''%% ranges from %%''-count''%% to %%''count-1''%%, where positive numbers go from first sub-object (%%''0''%%) to last (%%''count-1''%%), and negative numbers go from last sub-object (%%''-1''%%) to first (%%''-count''%%). %%''count''%% is the number of sub-objects currently in the container as returned by %%[[:develop:api:efl:container:method:content_count|Efl.Container.content_count]]%%.%%

%%If %%''index''%% is less than %%''-count''%%, it will return the first sub-object whereas %%''index''%% greater than %%''count-1''%% will return the last sub-object.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_part_box:method:pack_content_get:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_content_get|Efl.Pack_Linear.pack_content_get]].//===== Signature =====

<code>
pack_content_get @pure_virtual {
    params {
        @in index: int;
    }
    return: Efl.Gfx.Entity;
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_pack_content_get(Eo *obj, int index);
</code>

===== Parameters =====

  * **index** //(in)// - %%Index of the existing sub-object to retrieve. Valid range is %%''-count''%% to %%''count-1''%%.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_content_get|Efl.Pack_Linear.pack_content_get]]
  * [[:develop:api:efl:ui:flip:method:pack_content_get|Efl.Ui.Flip.pack_content_get]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_content_get|Efl.Canvas.Layout_Part_Box.pack_content_get]]
  * [[:develop:api:efl:ui:box:method:pack_content_get|Efl.Ui.Box.pack_content_get]]
  * [[:develop:api:efl:ui:group_item:method:pack_content_get|Efl.Ui.Group_Item.pack_content_get]]
  * [[:develop:api:efl:ui:collection:method:pack_content_get|Efl.Ui.Collection.pack_content_get]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_content_get|Efl.Ui.Layout_Part_Box.pack_content_get]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_content_get|Efl.Canvas.Layout_Part_Invalid.pack_content_get]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_content_get|Efl.Ui.Spotlight.Container.pack_content_get]]

