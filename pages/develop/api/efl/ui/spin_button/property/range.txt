~~Title: Efl.Ui.Spin_Button.range~~
====== Efl.Ui.Spin_Button.range ======

===== Values =====

  * **lower_limit** - %%Lower limit of the range%%
  * **upper_limit** - %%Upper limit of the range%%
  * **description** - %%Description of the range%%


\\ {{page>:develop:api-include:efl:ui:spin_button:property:range:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:access:value:property:range|Efl.Access.Value.range]] **(get)**.//===== Signature =====

<code>
@property range @beta @pure_virtual {
    get @protected {}
    values {
        lower_limit: double;
        upper_limit: double;
        description: string;
    }
}
</code>

===== C signature =====

<code c>
void efl_access_value_range_get(const Eo *obj, double *lower_limit, double *upper_limit, const char **description);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:value:property:range|Efl.Access.Value.range]]
  * [[:develop:api:efl:ui:slider:property:range|Efl.Ui.Slider.range]]
  * [[:develop:api:efl:ui:spin_button:property:range|Efl.Ui.Spin_Button.range]]
  * [[:develop:api:efl:ui:slider_interval:property:range|Efl.Ui.Slider_Interval.range]]

