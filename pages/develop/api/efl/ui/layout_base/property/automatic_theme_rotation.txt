~~Title: Efl.Ui.Layout_Base.automatic_theme_rotation~~
====== Efl.Ui.Layout_Base.automatic_theme_rotation ======

===== Description =====

%%This flag tells if this object will automatically mirror the rotation changes of the window to this object.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:property:automatic_theme_rotation:description&nouser&nolink&nodate}}

===== Values =====

  * **automatic** - %%%%''true''%% to mirror orientation changes to the theme %%''false''%% otherwise%%

===== Signature =====

<code>
@property automatic_theme_rotation @beta {
    get {}
    set {}
    values {
        automatic: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_layout_automatic_theme_rotation_get(const Eo *obj);
void efl_ui_layout_automatic_theme_rotation_set(Eo *obj, Eina_Bool automatic);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:layout_base:property:automatic_theme_rotation|Efl.Ui.Layout_Base.automatic_theme_rotation]]

