~~Title: Efl.Ui.Layout_Base.group_size_max~~
====== Efl.Ui.Layout_Base.group_size_max ======

===== Values =====

  * **max** - %%The maximum size as set in EDC.%%


\\ {{page>:develop:api-include:efl:ui:layout_base:property:group_size_max:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:group:property:group_size_max|Efl.Layout.Group.group_size_max]] **(get)**.//===== Signature =====

<code>
@property group_size_max @pure_virtual {
    get {}
    values {
        max: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_layout_group_size_max_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:group:property:group_size_max|Efl.Layout.Group.group_size_max]]
  * [[:develop:api:efl:canvas:layout:property:group_size_max|Efl.Canvas.Layout.group_size_max]]
  * [[:develop:api:efl:ui:image:property:group_size_max|Efl.Ui.Image.group_size_max]]
  * [[:develop:api:efl:ui:image_zoomable:property:group_size_max|Efl.Ui.Image_Zoomable.group_size_max]]
  * [[:develop:api:efl:ui:layout_base:property:group_size_max|Efl.Ui.Layout_Base.group_size_max]]

