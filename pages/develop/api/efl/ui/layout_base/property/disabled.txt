~~Title: Efl.Ui.Layout_Base.disabled~~
====== Efl.Ui.Layout_Base.disabled ======

===== Description =====

%%Whether the widget is enabled (accepts and reacts to user inputs).%%

%%Each widget may handle the disabled state differently, but overall disabled widgets shall not respond to any input events. This is %%''false''%% by default, meaning the widget is enabled.%%

%%Disabling a widget will disable all its children recursively, but only this widget will be marked as disabled internally.%%

%%This will return %%''true''%% if any widget in the parent hierarchy is disabled. Re-enabling that parent may in turn change the disabled state of this widget.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:property:disabled:description&nouser&nolink&nodate}}

===== Values =====

  * **disabled** - %%%%''true''%% if the widget is disabled.%%

//Overridden from [[:develop:api:efl:ui:widget:property:disabled|Efl.Ui.Widget.disabled]] **(set)**.//===== Signature =====

<code>
@property disabled {
    get {}
    set {}
    values {
        disabled: bool (false);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_disabled_get(const Eo *obj);
void efl_ui_widget_disabled_set(Eo *obj, Eina_Bool disabled);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:disabled|Efl.Ui.Widget.disabled]]
  * [[:develop:api:efl:ui:layout_base:property:disabled|Efl.Ui.Layout_Base.disabled]]
  * [[:develop:api:efl:ui:text:property:disabled|Efl.Ui.Text.disabled]]
  * [[:develop:api:efl:ui:panel:property:disabled|Efl.Ui.Panel.disabled]]

