~~Title: Efl.Ui.Layout_Base.calc_freeze~~
====== Efl.Ui.Layout_Base.calc_freeze ======

===== Description =====

%%Freezes the layout object.%%

%%This function puts all changes on hold. Successive freezes will nest, requiring an equal number of thaws.%%

%%See also %%[[:develop:api:efl:layout:calc:method:calc_thaw|Efl.Layout.Calc.calc_thaw]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:method:calc_freeze:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:calc:method:calc_freeze|Efl.Layout.Calc.calc_freeze]].//===== Signature =====

<code>
calc_freeze @pure_virtual {
    return: int;
}
</code>

===== C signature =====

<code c>
int efl_layout_calc_freeze(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:calc:method:calc_freeze|Efl.Layout.Calc.calc_freeze]]
  * [[:develop:api:efl:canvas:layout:method:calc_freeze|Efl.Canvas.Layout.calc_freeze]]
  * [[:develop:api:efl:ui:image:method:calc_freeze|Efl.Ui.Image.calc_freeze]]
  * [[:develop:api:efl:ui:layout_base:method:calc_freeze|Efl.Ui.Layout_Base.calc_freeze]]

