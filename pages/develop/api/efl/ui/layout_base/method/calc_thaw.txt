~~Title: Efl.Ui.Layout_Base.calc_thaw~~
====== Efl.Ui.Layout_Base.calc_thaw ======

===== Description =====

%%Thaws the layout object.%%

%%This function thaws (in other words "unfreezes") the given layout object.%%

<note>
%%If successive freezes were done, an equal number of thaws will be required.%%
</note>

%%See also %%[[:develop:api:efl:layout:calc:method:calc_freeze|Efl.Layout.Calc.calc_freeze]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:method:calc_thaw:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:calc:method:calc_thaw|Efl.Layout.Calc.calc_thaw]].//===== Signature =====

<code>
calc_thaw @pure_virtual {
    return: int;
}
</code>

===== C signature =====

<code c>
int efl_layout_calc_thaw(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:calc:method:calc_thaw|Efl.Layout.Calc.calc_thaw]]
  * [[:develop:api:efl:canvas:layout:method:calc_thaw|Efl.Canvas.Layout.calc_thaw]]
  * [[:develop:api:efl:ui:image:method:calc_thaw|Efl.Ui.Image.calc_thaw]]
  * [[:develop:api:efl:ui:layout_base:method:calc_thaw|Efl.Ui.Layout_Base.calc_thaw]]

