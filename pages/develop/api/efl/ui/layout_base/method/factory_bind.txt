~~Title: Efl.Ui.Layout_Base.factory_bind~~
====== Efl.Ui.Layout_Base.factory_bind ======

===== Description =====

%%bind the factory with the given key string. when the data is ready or changed, factory create the object and bind the data to the key action and process promised work. Note: the input %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%% need to be %%[[:develop:api:efl:ui:property_bind:method:property_bind|Efl.Ui.Property_Bind.property_bind]]%% at least once.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_base:method:factory_bind:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:factory_bind:method:factory_bind|Efl.Ui.Factory_Bind.factory_bind]].//===== Signature =====

<code>
factory_bind @pure_virtual {
    params {
        @in key: string;
        @in factory: Efl.Ui.Factory;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_ui_factory_bind(Eo *obj, const char *key, Efl_Ui_Factory *factory);
</code>

===== Parameters =====

  * **key** //(in)// - %%Key string for bind model property data%%
  * **factory** //(in)// - %%%%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%% for create and bind model property data%%

===== Implemented by =====

  * [[:develop:api:efl:ui:factory_bind:method:factory_bind|Efl.Ui.Factory_Bind.factory_bind]]
  * [[:develop:api:efl:ui:widget_factory:method:factory_bind|Efl.Ui.Widget_Factory.factory_bind]]
  * [[:develop:api:efl:ui:layout_factory:method:factory_bind|Efl.Ui.Layout_Factory.factory_bind]]
  * [[:develop:api:efl:ui:layout_base:method:factory_bind|Efl.Ui.Layout_Base.factory_bind]]

