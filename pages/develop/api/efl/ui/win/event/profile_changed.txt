~~Title: Efl.Ui.Win: profile,changed~~

===== Description =====

%%Called when profile is changed%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:win:event:profile_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
profile,changed @beta;
</code>

===== C information =====

<code c>
EFL_UI_WIN_EVENT_PROFILE_CHANGED(void, @beta)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_win_event_profile_changed(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_WIN_EVENT_PROFILE_CHANGED, on_efl_ui_win_event_profile_changed, d);
}

</code>
