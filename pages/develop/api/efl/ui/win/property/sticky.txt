~~Title: Efl.Ui.Win.sticky~~
====== Efl.Ui.Win.sticky ======

===== Description =====

%%The sticky state of the window.%%

%%Hints the Window Manager that the window in %%''obj''%% should be left fixed at its position even when the virtual desktop it's on moves or changes.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:sticky:description&nouser&nolink&nodate}}

===== Values =====

  * **sticky** - %%If %%''true''%%, the window's sticky state is enabled.%%

===== Signature =====

<code>
@property sticky @beta {
    get {}
    set {}
    values {
        sticky: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_sticky_get(const Eo *obj);
void efl_ui_win_sticky_set(Eo *obj, Eina_Bool sticky);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:sticky|Efl.Ui.Win.sticky]]

