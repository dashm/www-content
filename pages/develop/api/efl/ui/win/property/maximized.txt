~~Title: Efl.Ui.Win.maximized~~
====== Efl.Ui.Win.maximized ======

===== Description =====

%%The maximized state of a window.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:maximized:description&nouser&nolink&nodate}}

===== Values =====

  * **maximized** - %%If %%''true''%%, the window is maximized.%%

===== Signature =====

<code>
@property maximized {
    get {}
    set {}
    values {
        maximized: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_maximized_get(const Eo *obj);
void efl_ui_win_maximized_set(Eo *obj, Eina_Bool maximized);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:maximized|Efl.Ui.Win.maximized]]

