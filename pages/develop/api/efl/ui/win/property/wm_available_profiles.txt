~~Title: Efl.Ui.Win.wm_available_profiles~~
====== Efl.Ui.Win.wm_available_profiles ======

===== Description =====

%%Available profiles on a window.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:wm_available_profiles:description&nouser&nolink&nodate}}

===== Values =====

  * **profiles** - %%A list of profiles.%%

===== Signature =====

<code>
@property wm_available_profiles @beta {
    get {}
    set {}
    values {
        profiles: const(array<string>);
    }
}
</code>

===== C signature =====

<code c>
const Eina_Array *efl_ui_win_wm_available_profiles_get(const Eo *obj);
void efl_ui_win_wm_available_profiles_set(Eo *obj, const Eina_Array *profiles);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:wm_available_profiles|Efl.Ui.Win.wm_available_profiles]]

