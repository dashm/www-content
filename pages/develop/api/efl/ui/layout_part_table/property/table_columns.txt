~~Title: Efl.Ui.Layout_Part_Table.table_columns~~
====== Efl.Ui.Layout_Part_Table.table_columns ======

===== Description =====

%%Specifies the amount of columns the table will have when the fill direction is horizontal. If it is vertical, the amount of columns depends on the amount of cells added and %%[[:develop:api:efl:pack_table:property:table_rows|Efl.Pack_Table.table_rows]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_part_table:property:table_columns:description&nouser&nolink&nodate}}

===== Values =====

  * **cols** - %%Amount of columns.%%

//Overridden from [[:develop:api:efl:pack_table:property:table_columns|Efl.Pack_Table.table_columns]] **(get, set)**.//===== Signature =====

<code>
@property table_columns @pure_virtual {
    get {}
    set {}
    values {
        cols: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_pack_table_columns_get(const Eo *obj);
void efl_pack_table_columns_set(Eo *obj, int cols);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack_table:property:table_columns|Efl.Pack_Table.table_columns]]
  * [[:develop:api:efl:ui:table:property:table_columns|Efl.Ui.Table.table_columns]]
  * [[:develop:api:efl:canvas:layout_part_table:property:table_columns|Efl.Canvas.Layout_Part_Table.table_columns]]
  * [[:develop:api:efl:ui:layout_part_table:property:table_columns|Efl.Ui.Layout_Part_Table.table_columns]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:table_columns|Efl.Canvas.Layout_Part_Invalid.table_columns]]

