~~Title: Efl.Ui.Layout_Orientation~~

===== Description =====

%%Orientation for UI objects and layouts that can have multiple configurations.%%

%%Select among %%''horizontal''%% or %%''vertical''%% orientations (or use %%''default''%% to let the object decide). Additionally, %%''inverted''%% can be added to reverse the direction along the selected axis.%%

%%Not to be confused with %%[[:develop:api:efl:gfx:image_orientation|Efl.Gfx.Image_Orientation]]%% which is for images and canvases. This enum is used to define how widgets should expand and orient themselves, not to rotate images.%%

%%See also %%[[:develop:api:efl:ui:layout_orientable|Efl.Ui.Layout_Orientable]]%%.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:layout_orientation:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:layout_orientation:fields&nouser&nolink&nodate}}

  * **default** - %%Default direction. Each widget may have a different default.%%
  * **horizontal** - %%Horizontal direction, along the X axis. Usually left-to-right, but can be inverted.%%
  * **vertical** - %%Vertical direction, along the Y axis. Usually downwards but can be inverted.%%
  * **axis_bitmask** - %%This bitmask can be used to isolate the axis value from the rest of bits.%%
  * **inverted** - %%Add this value to make the object invert its default direction along the selected axis.%%

===== Signature =====

<code>
enum Efl.Ui.Layout_Orientation {
    default: 0,
    horizontal: 1,
    vertical: 2,
    axis_bitmask: 3,
    inverted: 4
}
</code>

===== C signature =====

<code c>
typedef enum {
    EFL_UI_LAYOUT_ORIENTATION_DEFAULT = 0,
    EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL = 1,
    EFL_UI_LAYOUT_ORIENTATION_VERTICAL = 2,
    EFL_UI_LAYOUT_ORIENTATION_AXIS_BITMASK = 3,
    EFL_UI_LAYOUT_ORIENTATION_INVERTED = 4
} Efl_Ui_Layout_Orientation;
</code>
