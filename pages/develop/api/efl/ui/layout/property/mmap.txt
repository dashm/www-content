~~Title: Efl.Ui.Layout.mmap~~
====== Efl.Ui.Layout.mmap ======

===== Description =====

%%The mmaped file from where an object will fetch the real data (it must be an %%[[:develop:api:eina:file|Eina.File]]%%).%%

%%If mmap is set during object construction, the object will automatically call %%[[:develop:api:efl:file:method:load|Efl.File.load]]%% during the finalize phase of construction.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout:property:mmap:description&nouser&nolink&nodate}}

===== Values =====

  * **f** - %%The handle to the %%[[:develop:api:eina:file|Eina.File]]%% that will be used%%

//Overridden from [[:develop:api:efl:file:property:mmap|Efl.File.mmap]] **(get, set)**.//===== Signature =====

<code>
@property mmap {
    get {}
    set {
        return: Eina.Error;
    }
    values {
        f: const(Eina.File);
    }
}
</code>

===== C signature =====

<code c>
const Eina_File efl_file_mmap_get(const Eo *obj);
Eina_Error efl_file_mmap_set(Eo *obj, const Eina_File f);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:property:mmap|Efl.File.mmap]]
  * [[:develop:api:efl:ui:layout:property:mmap|Efl.Ui.Layout.mmap]]
  * [[:develop:api:efl:ui:widget_part_bg:property:mmap|Efl.Ui.Widget_Part_Bg.mmap]]
  * [[:develop:api:efl:ui:bg:property:mmap|Efl.Ui.Bg.mmap]]

