~~Title: Efl.Ui.Slider.increment~~
====== Efl.Ui.Slider.increment ======

===== Values =====

  * **increment** - %%Minimal incrementation value%%


\\ {{page>:develop:api-include:efl:ui:slider:property:increment:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:access:value:property:increment|Efl.Access.Value.increment]] **(get)**.//===== Signature =====

<code>
@property increment @beta @pure_virtual {
    get @protected {}
    values {
        increment: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_access_value_increment_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:value:property:increment|Efl.Access.Value.increment]]
  * [[:develop:api:efl:ui:slider:property:increment|Efl.Ui.Slider.increment]]
  * [[:develop:api:efl:ui:spin_button:property:increment|Efl.Ui.Spin_Button.increment]]
  * [[:develop:api:efl:ui:slider_interval:property:increment|Efl.Ui.Slider_Interval.increment]]

