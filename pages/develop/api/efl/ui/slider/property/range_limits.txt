~~Title: Efl.Ui.Slider.range_limits~~
====== Efl.Ui.Slider.range_limits ======

===== Description =====

%%Set the minimum and maximum values for given range widget.%%

%%If the current value is less than %%''min''%%, it will be updated to %%''min''%%. If it is bigger then %%''max''%%, will be updated to %%''max''%%. The resulting value can be obtained with %%[[:develop:api:efl:ui:range_display:property:range_value|Efl.Ui.Range_Display.range_value.get]]%%.%%

%%The default minimum and maximum values may be different for each class.%%

<note>
%%maximum must be greater than minimum, otherwise behavior is undefined.%%
</note>

//Since 1.23//
{{page>:develop:api-include:efl:ui:slider:property:range_limits:description&nouser&nolink&nodate}}

===== Values =====

  * **min** - %%The minimum value.%%
  * **max** - %%The maximum value.%%

//Overridden from [[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]] **(get, set)**.//===== Signature =====

<code>
@property range_limits @pure_virtual {
    get {}
    set {}
    values {
        min: double;
        max: double;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_range_limits_get(const Eo *obj, double *min, double *max);
void efl_ui_range_limits_set(Eo *obj, double min, double max);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]
  * [[:develop:api:efl:ui:spin:property:range_limits|Efl.Ui.Spin.range_limits]]
  * [[:develop:api:efl:ui:progressbar_part:property:range_limits|Efl.Ui.Progressbar_Part.range_limits]]
  * [[:develop:api:efl:ui:slider:property:range_limits|Efl.Ui.Slider.range_limits]]
  * [[:develop:api:efl:ui:slider_interval:property:range_limits|Efl.Ui.Slider_Interval.range_limits]]
  * [[:develop:api:efl:ui:progressbar:property:range_limits|Efl.Ui.Progressbar.range_limits]]

