~~Title: Efl.Ui.Layout_Part_Box~~
====== Efl.Ui.Layout_Part_Box (class) ======

===== Description =====

%%Represents a Box created as part of a layout.%%

%%Cannot be deleted. This is only a representation of an internal object of an EFL layout.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:layout_part_box:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:object|Efl.Object]] //(class)//
  * [[:develop:api:efl:pack_linear|Efl.Pack_Linear]] //(interface)//
    * [[:develop:api:efl:pack|Efl.Pack]] //(interface)//
      * [[:develop:api:efl:container|Efl.Container]] //(interface)//
  * [[:develop:api:efl:ui:layout_orientable_readonly|Efl.Ui.Layout_Orientable_Readonly]] //(mixin)//
    * [[:develop:api:efl:ui:layout_orientable|Efl.Ui.Layout_Orientable]] //(interface)//


++++
===== Members =====

**[[:develop:api:efl:ui:layout_part_box:method:content_count|content_count]]**// [Overridden from [[:develop:api:efl:container|Efl.Container]]]//\\
> %%Returns the number of contained sub-objects.%%
<code c>
int efl_content_count(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:content_iterate|content_iterate]]**// [Overridden from [[:develop:api:efl:container|Efl.Container]]]//\\
> %%Begin iterating over this object's contents.%%
<code c>
Eina_Iterator *efl_content_iterate(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide deinitialization code for your object if you need it.%%
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:property:orientation|orientation]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:ui:layout_orientable|Efl.Ui.Layout_Orientable]]]//\\
> %%This will always print an error saying that this is a read-only object.%%
<code c>
Efl_Ui_Layout_Orientation efl_ui_layout_orientation_get(const Eo *obj);
void efl_ui_layout_orientation_set(Eo *obj, Efl_Ui_Layout_Orientation dir);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack|pack]]**// [Overridden from [[:develop:api:efl:pack|Efl.Pack]]]//\\
> %%Adds a sub-object to this container.%%
<code c>
Eina_Bool efl_pack(Eo *obj, Efl_Gfx_Entity *subobj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_after|pack_after]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Append an object after the %%''existing''%% sub-object.%%
<code c>
Eina_Bool efl_pack_after(Eo *obj, Efl_Gfx_Entity *subobj, const Efl_Gfx_Entity *existing);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_at|pack_at]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Inserts %%''subobj''%% BEFORE the sub-object at position %%''index''%%.%%
<code c>
Eina_Bool efl_pack_at(Eo *obj, Efl_Gfx_Entity *subobj, int index);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_before|pack_before]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Prepend an object before the %%''existing''%% sub-object.%%
<code c>
Eina_Bool efl_pack_before(Eo *obj, Efl_Gfx_Entity *subobj, const Efl_Gfx_Entity *existing);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_begin|pack_begin]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Prepend an object at the beginning of this container.%%
<code c>
Eina_Bool efl_pack_begin(Eo *obj, Efl_Gfx_Entity *subobj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_clear|pack_clear]]**// [Overridden from [[:develop:api:efl:pack|Efl.Pack]]]//\\
> %%Removes all packed sub-objects and unreferences them.%%
<code c>
Eina_Bool efl_pack_clear(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_content_get|pack_content_get]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Sub-object at a given %%''index''%% in this container.%%
<code c>
Efl_Gfx_Entity *efl_pack_content_get(Eo *obj, int index);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_end|pack_end]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Append object at the end of this container.%%
<code c>
Eina_Bool efl_pack_end(Eo *obj, Efl_Gfx_Entity *subobj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_index_get|pack_index_get]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Get the index of a sub-object in this container.%%
<code c>
int efl_pack_index_get(Eo *obj, const Efl_Gfx_Entity *subobj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:pack_unpack_at|pack_unpack_at]]**// [Overridden from [[:develop:api:efl:pack_linear|Efl.Pack_Linear]]]//\\
> %%Pop out (remove) the sub-object at the specified %%''index''%%.%%
<code c>
Efl_Gfx_Entity *efl_pack_unpack_at(Eo *obj, int index);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:unpack|unpack]]**// [Overridden from [[:develop:api:efl:pack|Efl.Pack]]]//\\
> %%Removes an existing sub-object from the container without deleting it.%%
<code c>
Eina_Bool efl_pack_unpack(Eo *obj, Efl_Gfx_Entity *subobj);
</code>
\\
**[[:develop:api:efl:ui:layout_part_box:method:unpack_all|unpack_all]]**// [Overridden from [[:develop:api:efl:pack|Efl.Pack]]]//\\
> %%Removes all packed sub-objects without unreferencing them.%%
<code c>
Eina_Bool efl_pack_unpack_all(Eo *obj);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:constructor|constructor]]** | %%Implement this method to provide optional initialization code for your object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:method:finalize|finalize]]** | %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:method:invalidate|invalidate]]** | %%Implement this method to perform special actions when your object loses its parent, if you need to.%% |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:property:parent|parent]]** //**(get, set)**// | %%The parent of an object.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |

===== Events =====

==== Inherited ====

^ [[:develop:api:efl:container|Efl.Container]] ^^^
|  | **[[:develop:api:efl:container:event:content_added|content,added]]** | %%Sent after a new sub-object was added.%% |
|  | **[[:develop:api:efl:container:event:content_removed|content,removed]]** | %%Sent after a sub-object was removed, before unref.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
