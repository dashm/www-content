~~Title: Efl.Ui.Table.content_padding~~
====== Efl.Ui.Table.content_padding ======

===== Description =====

%%This property determines the space between a container's content items.%%

%%It is different than the %%[[:develop:api:efl:gfx:hint:property:hint_margin|Efl.Gfx.Hint.hint_margin]]%% property in that it is applied to each content item within the container instead of a single item. The calculation for these two properties is cumulative.%%

%%See also %%[[:develop:api:efl:gfx:hint:property:hint_margin|Efl.Gfx.Hint.hint_margin]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:property:content_padding:description&nouser&nolink&nodate}}

===== Values =====

  * **pad_horiz** - %%Horizontal padding.%%
  * **pad_vert** - %%Vertical padding.%%
  * **scalable** - %%%%''true''%% if scalable.%%

//Overridden from [[:develop:api:efl:gfx:arrangement:property:content_padding|Efl.Gfx.Arrangement.content_padding]] **(get, set)**.//===== Signature =====

<code>
@property content_padding @beta @pure_virtual {
    get {}
    set {}
    values {
        pad_horiz: double (0.000000);
        pad_vert: double (0.000000);
        scalable: bool (false);
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_arrangement_content_padding_get(const Eo *obj, double *pad_horiz, double *pad_vert, Eina_Bool *scalable);
void efl_gfx_arrangement_content_padding_set(Eo *obj, double pad_horiz, double pad_vert, Eina_Bool scalable);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:arrangement:property:content_padding|Efl.Gfx.Arrangement.content_padding]]
  * [[:develop:api:efl:ui:table:property:content_padding|Efl.Ui.Table.content_padding]]
  * [[:develop:api:efl:ui:box:property:content_padding|Efl.Ui.Box.content_padding]]

