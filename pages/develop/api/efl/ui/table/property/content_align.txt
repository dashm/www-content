~~Title: Efl.Ui.Table.content_align~~
====== Efl.Ui.Table.content_align ======

===== Description =====

%%This property determines how contents will be aligned within a container if there is unused space.%%

%%It is different than the %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%% property in that it affects the position of all the contents within the container. For example, if a box widget has extra space on the horizontal axis, this property can be used to align the box's contents to the left or the right side.%%

%%See also %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:property:content_align:description&nouser&nolink&nodate}}

===== Values =====

  * **align_horiz** - %%Double, ranging from 0.0 to 1.0, where 0.0 is at the start of the horizontal axis and 1.0 is at the end.%%
  * **align_vert** - %%Double, ranging from 0.0 to 1.0, where 0.0 is at the start of the vertical axis and 1.0 is at the end.%%

//Overridden from [[:develop:api:efl:gfx:arrangement:property:content_align|Efl.Gfx.Arrangement.content_align]] **(get, set)**.//===== Signature =====

<code>
@property content_align @pure_virtual {
    get {}
    set {}
    values {
        align_horiz: double (0.500000);
        align_vert: double (0.500000);
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_arrangement_content_align_get(const Eo *obj, double *align_horiz, double *align_vert);
void efl_gfx_arrangement_content_align_set(Eo *obj, double align_horiz, double align_vert);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:arrangement:property:content_align|Efl.Gfx.Arrangement.content_align]]
  * [[:develop:api:efl:ui:table:property:content_align|Efl.Ui.Table.content_align]]
  * [[:develop:api:efl:ui:box:property:content_align|Efl.Ui.Box.content_align]]
  * [[:develop:api:efl:ui:image:property:content_align|Efl.Ui.Image.content_align]]

