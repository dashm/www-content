~~Title: Efl.Ui.Table.table_cell_column~~
====== Efl.Ui.Table.table_cell_column ======

===== Description =====

%%column of the %%''subobj''%% in this container.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:property:table_cell_column:description&nouser&nolink&nodate}}

===== Keys =====

  * **subobj** - %%Child object%%
===== Values =====

  * **col** - %%Column number%%
  * **colspan** - %%Column span%%

//Overridden from [[:develop:api:efl:pack_table:property:table_cell_column|Efl.Pack_Table.table_cell_column]] **(get, set)**.//===== Signature =====

<code>
@property table_cell_column @pure_virtual {
    get {
        return: bool;
    }
    set {}
    keys {
        subobj: Efl.Gfx.Entity;
    }
    values {
        col: int;
        colspan: int;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_table_cell_column_get(const Eo *obj, Efl_Gfx_Entity *subobj, int *col, int *colspan);
void efl_pack_table_cell_column_set(Eo *obj, Efl_Gfx_Entity *subobj, int col, int colspan);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack_table:property:table_cell_column|Efl.Pack_Table.table_cell_column]]
  * [[:develop:api:efl:ui:table:property:table_cell_column|Efl.Ui.Table.table_cell_column]]
  * [[:develop:api:efl:canvas:layout_part_table:property:table_cell_column|Efl.Canvas.Layout_Part_Table.table_cell_column]]
  * [[:develop:api:efl:ui:layout_part_table:property:table_cell_column|Efl.Ui.Layout_Part_Table.table_cell_column]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:table_cell_column|Efl.Canvas.Layout_Part_Invalid.table_cell_column]]

