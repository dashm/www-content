~~Title: Efl.Ui.Table.unpack~~
====== Efl.Ui.Table.unpack ======

===== Description =====

%%Removes an existing sub-object from the container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:method:unpack:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]].//===== Signature =====

<code>
unpack @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_unpack(Eo *obj, Efl_Gfx_Entity *subobj);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%The sub-object to unpack.%%

===== Implemented by =====

  * [[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]
  * [[:develop:api:efl:ui:tab_bar:method:unpack|Efl.Ui.Tab_Bar.unpack]]
  * [[:develop:api:efl:ui:table:method:unpack|Efl.Ui.Table.unpack]]
  * [[:develop:api:efl:canvas:layout_part_table:method:unpack|Efl.Canvas.Layout_Part_Table.unpack]]
  * [[:develop:api:efl:ui:layout_part_table:method:unpack|Efl.Ui.Layout_Part_Table.unpack]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:unpack|Efl.Canvas.Layout_Part_Invalid.unpack]]
  * [[:develop:api:efl:ui:flip:method:unpack|Efl.Ui.Flip.unpack]]
  * [[:develop:api:efl:canvas:layout_part_box:method:unpack|Efl.Canvas.Layout_Part_Box.unpack]]
  * [[:develop:api:efl:ui:box:method:unpack|Efl.Ui.Box.unpack]]
  * [[:develop:api:efl:ui:radio_box:method:unpack|Efl.Ui.Radio_Box.unpack]]
  * [[:develop:api:efl:ui:group_item:method:unpack|Efl.Ui.Group_Item.unpack]]
  * [[:develop:api:efl:ui:collection:method:unpack|Efl.Ui.Collection.unpack]]
  * [[:develop:api:efl:ui:layout_part_box:method:unpack|Efl.Ui.Layout_Part_Box.unpack]]
  * [[:develop:api:efl:ui:spotlight:container:method:unpack|Efl.Ui.Spotlight.Container.unpack]]
  * [[:develop:api:efl:ui:tab_pager:method:unpack|Efl.Ui.Tab_Pager.unpack]]
  * [[:develop:api:efl:ui:relative_layout:method:unpack|Efl.Ui.Relative_Layout.unpack]]

