~~Title: Efl.Ui.Table.pack~~
====== Efl.Ui.Table.pack ======

===== Description =====

%%Adds a sub-object to this container.%%

%%Depending on the container this will either fill in the default spot, replacing any already existing element or append to the end of the container if there is no default part.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:method:pack:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack:method:pack|Efl.Pack.pack]].//===== Signature =====

<code>
pack @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack(Eo *obj, Efl_Gfx_Entity *subobj);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%The object to pack.%%

===== Implemented by =====

  * [[:develop:api:efl:pack:method:pack|Efl.Pack.pack]]
  * [[:develop:api:efl:ui:tab_bar:method:pack|Efl.Ui.Tab_Bar.pack]]
  * [[:develop:api:efl:ui:table:method:pack|Efl.Ui.Table.pack]]
  * [[:develop:api:efl:ui:layout_part_table:method:pack|Efl.Ui.Layout_Part_Table.pack]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack|Efl.Canvas.Layout_Part_Invalid.pack]]
  * [[:develop:api:efl:ui:flip:method:pack|Efl.Ui.Flip.pack]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack|Efl.Canvas.Layout_Part_Box.pack]]
  * [[:develop:api:efl:ui:box:method:pack|Efl.Ui.Box.pack]]
  * [[:develop:api:efl:ui:radio_box:method:pack|Efl.Ui.Radio_Box.pack]]
  * [[:develop:api:efl:ui:group_item:method:pack|Efl.Ui.Group_Item.pack]]
  * [[:develop:api:efl:ui:collection:method:pack|Efl.Ui.Collection.pack]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack|Efl.Ui.Layout_Part_Box.pack]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack|Efl.Ui.Spotlight.Container.pack]]
  * [[:develop:api:efl:ui:relative_layout:method:pack|Efl.Ui.Relative_Layout.pack]]

