~~Title: Efl.Ui.Format.format_func~~
====== Efl.Ui.Format.format_func ======

===== Description =====

%%User-provided function which takes care of converting an %%[[:develop:api:eina:value|Eina.Value]]%% into a text string. The user is then completely in control of how the string is generated, but it is the most cumbersome method to use. If the conversion fails the other mechanisms will be tried, according to their priorities.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:format:property:format_func:description&nouser&nolink&nodate}}

===== Values =====

  * **func** - %%User-provided formatting function.%%

===== Signature =====

<code>
@property format_func {
    get {}
    set {}
    values {
        func: Efl.Ui.Format_Func;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Format_Func efl_ui_format_func_get(const Eo *obj);
void efl_ui_format_func_set(Eo *obj, Efl_Ui_Format_Func func);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]

