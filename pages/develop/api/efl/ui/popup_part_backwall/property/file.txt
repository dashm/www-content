~~Title: Efl.Ui.Popup_Part_Backwall.file~~
====== Efl.Ui.Popup_Part_Backwall.file ======

===== Description =====

%%The file path from where an object will fetch the data.%%

%%If file is set during object construction, the object will automatically call %%[[:develop:api:efl:file:method:load|Efl.File.load]]%% during the finalize phase of construction.%%

%%You must not modify the strings on the returned pointers.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:popup_part_backwall:property:file:description&nouser&nolink&nodate}}

===== Values =====

  * **file** - %%The file path.%%

//Overridden from [[:develop:api:efl:file:property:file|Efl.File.file]] **(get)**.//===== Signature =====

<code>
@property file {
    get {}
    set {
        return: Eina.Error;
    }
    values {
        file: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_file_get(const Eo *obj);
Eina_Error efl_file_set(Eo *obj, const char *file);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:property:file|Efl.File.file]]
  * [[:develop:api:efl:ui:win_part:property:file|Efl.Ui.Win_Part.file]]
  * [[:develop:api:efl:canvas:video:property:file|Efl.Canvas.Video.file]]
  * [[:develop:api:efl:ui:text:property:file|Efl.Ui.Text.file]]
  * [[:develop:api:efl:ui:layout:property:file|Efl.Ui.Layout.file]]
  * [[:develop:api:efl:ui:image_zoomable:property:file|Efl.Ui.Image_Zoomable.file]]
  * [[:develop:api:efl:canvas:vg:object:property:file|Efl.Canvas.Vg.Object.file]]
  * [[:develop:api:efl:ui:widget_part_bg:property:file|Efl.Ui.Widget_Part_Bg.file]]
  * [[:develop:api:efl:ui:popup_part_backwall:property:file|Efl.Ui.Popup_Part_Backwall.file]]
  * [[:develop:api:efl:ui:bg:property:file|Efl.Ui.Bg.file]]

