~~Title: Efl.Ui.Alert_Popup_Button_Clicked_Event~~

===== Description =====

%%Information for %%[[:develop:api:efl:ui:alert_popup:event:button,clicked|Efl.Ui.Alert_Popup.button,clicked]]%% event.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:alert_popup_button_clicked_event:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:alert_popup_button_clicked_event:fields&nouser&nolink&nodate}}

  * **button_type** - %%Clicked button type.%%

===== Signature =====

<code>
struct Efl.Ui.Alert_Popup_Button_Clicked_Event {
    button_type: Efl.Ui.Alert_Popup_Button;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Ui_Alert_Popup_Button_Clicked_Event {
    Efl_Ui_Alert_Popup_Button button_type;
} Efl_Ui_Alert_Popup_Button_Clicked_Event;
</code>
