~~Title: Efl.Ui.Image_Zoomable.playback_progress~~
====== Efl.Ui.Image_Zoomable.playback_progress ======

===== Values =====

  * **progress** - %%The progress within the [0, 1] range.%%


\\ {{page>:develop:api-include:efl:ui:image_zoomable:property:playback_progress:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:image:property:playback_progress|Efl.Player.playback_progress]] **(get)**.//===== Signature =====

<code>
@property playback_progress @pure_virtual {
    get {}
    values {
        progress: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_player_playback_progress_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:player:property:playback_progress|Efl.Player.playback_progress]]
  * [[:develop:api:efl:canvas:video:property:playback_progress|Efl.Canvas.Video.playback_progress]]
  * [[:develop:api:efl:canvas:animation_player:property:playback_progress|Efl.Canvas.Animation_Player.playback_progress]]
  * [[:develop:api:efl:ui:image:property:playback_progress|Efl.Ui.Image.playback_progress]]
  * [[:develop:api:efl:ui:image_zoomable:property:playback_progress|Efl.Ui.Image_Zoomable.playback_progress]]

