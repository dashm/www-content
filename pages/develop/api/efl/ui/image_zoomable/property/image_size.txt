~~Title: Efl.Ui.Image_Zoomable.image_size~~
====== Efl.Ui.Image_Zoomable.image_size ======

===== Values =====

  * **size** - %%The size in pixels. The default value is the size of the image's internal buffer.%%


\\ {{page>:develop:api-include:efl:ui:image_zoomable:property:image_size:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:image:property:image_size|Efl.Gfx.Image.image_size]] **(get)**.//===== Signature =====

<code>
@property image_size @pure_virtual {
    get {}
    values {
        size: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_image_size_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:image_size|Efl.Gfx.Image.image_size]]
  * [[:develop:api:efl:canvas:image_internal:property:image_size|Efl.Canvas.Image_Internal.image_size]]
  * [[:develop:api:efl:ui:image:property:image_size|Efl.Ui.Image.image_size]]
  * [[:develop:api:efl:ui:image_zoomable:property:image_size|Efl.Ui.Image_Zoomable.image_size]]

