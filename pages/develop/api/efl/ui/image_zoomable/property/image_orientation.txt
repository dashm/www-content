~~Title: Efl.Ui.Image_Zoomable.image_orientation~~
====== Efl.Ui.Image_Zoomable.image_orientation ======

===== Description =====

%%Control the orientation (rotation and flipping) of a visual object.%%

%%This can be used to set the rotation on an image or a window, for instance.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image_zoomable:property:image_orientation:description&nouser&nolink&nodate}}

===== Values =====

  * **dir** - %%The final orientation of the object.%%

//Overridden from [[:develop:api:efl:ui:image:property:image_orientation|Efl.Gfx.Image_Orientable.image_orientation]] **(get, set)**.//===== Signature =====

<code>
@property image_orientation @pure_virtual {
    get {}
    set {}
    values {
        dir: Efl.Gfx.Image_Orientation (Efl.Gfx.Image_Orientation.none);
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Image_Orientation efl_gfx_image_orientation_get(const Eo *obj);
void efl_gfx_image_orientation_set(Eo *obj, Efl_Gfx_Image_Orientation dir);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image_orientable:property:image_orientation|Efl.Gfx.Image_Orientable.image_orientation]]
  * [[:develop:api:efl:canvas:image_internal:property:image_orientation|Efl.Canvas.Image_Internal.image_orientation]]
  * [[:develop:api:efl:ui:image:property:image_orientation|Efl.Ui.Image.image_orientation]]
  * [[:develop:api:efl:ui:image_zoomable:property:image_orientation|Efl.Ui.Image_Zoomable.image_orientation]]

