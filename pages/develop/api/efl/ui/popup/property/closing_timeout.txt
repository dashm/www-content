~~Title: Efl.Ui.Popup.closing_timeout~~
====== Efl.Ui.Popup.closing_timeout ======

===== Description =====

%%The closing_timeout property is the time after which the Popup widget will be automatically deleted.%%

%%The timer is initiated at the time when the popup is shown. If the value is changed prior to the timer expiring, the existing timer will be deleted. If the new value is greater than $0, a new timer will be created.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:popup:property:closing_timeout:description&nouser&nolink&nodate}}

===== Values =====

  * **time** - %%If greater than $0, the Popup will close automatically after the value in seconds. The default is to not automatically delete the Popup.%%

===== Signature =====

<code>
@property closing_timeout {
    get {}
    set {}
    values {
        time: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_ui_popup_closing_timeout_get(const Eo *obj);
void efl_ui_popup_closing_timeout_set(Eo *obj, double time);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:popup:property:closing_timeout|Efl.Ui.Popup.closing_timeout]]

