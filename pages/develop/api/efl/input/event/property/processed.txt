~~Title: Efl.Input.Event.processed~~
====== Efl.Input.Event.processed ======

===== Description =====

%%%%''true''%% if %%[[:develop:api:efl:input:event:property:event_flags|Efl.Input.Event.event_flags]]%% indicates the event is on hold.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:event:property:processed:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%%%''true''%% if the event is on hold, %%''false''%% otherwise%%

===== Signature =====

<code>
@property processed {
    get {}
    set {}
    values {
        val: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_input_processed_get(const Eo *obj);
void efl_input_processed_set(Eo *obj, Eina_Bool val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:event:property:processed|Efl.Input.Event.processed]]

