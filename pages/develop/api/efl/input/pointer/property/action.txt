~~Title: Efl.Input.Pointer.action~~
====== Efl.Input.Pointer.action ======

===== Description =====

%%The action represented by this event.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:pointer:property:action:description&nouser&nolink&nodate}}

===== Values =====

  * **act** - %%Event action%%

===== Signature =====

<code>
@property action {
    get {}
    set {}
    values {
        act: Efl.Pointer.Action;
    }
}
</code>

===== C signature =====

<code c>
Efl_Pointer_Action efl_input_pointer_action_get(const Eo *obj);
void efl_input_pointer_action_set(Eo *obj, Efl_Pointer_Action act);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:pointer:property:action|Efl.Input.Pointer.action]]

