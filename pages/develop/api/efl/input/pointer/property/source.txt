~~Title: Efl.Input.Pointer.source~~
====== Efl.Input.Pointer.source ======

===== Description =====

%%The object where this event first originated, in case of propagation or repetition of the event.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:pointer:property:source:description&nouser&nolink&nodate}}

===== Values =====

  * **src** - %%Source object: %%[[:develop:api:efl:gfx:entity|Efl.Gfx.Entity]]%%%%

===== Signature =====

<code>
@property source {
    get {}
    set {}
    values {
        src: Efl.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_input_pointer_source_get(const Eo *obj);
void efl_input_pointer_source_set(Eo *obj, Efl_Object *src);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:pointer:property:source|Efl.Input.Pointer.source]]

