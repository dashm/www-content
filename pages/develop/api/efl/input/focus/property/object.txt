~~Title: Efl.Input.Focus.object~~
====== Efl.Input.Focus.object ======

===== Description =====

%%The focused object.%%

//Since 1.22//
{{page>:develop:api-include:efl:input:focus:property:object:description&nouser&nolink&nodate}}

===== Values =====

  * **object** - %%The focused object, or %%''NULL''%% if the event comes from the canvas.%%

===== Signature =====

<code>
@property object {
    get {}
    set {}
    values {
        object: Efl.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_input_focus_object_get(const Eo *obj);
void efl_input_focus_object_set(Eo *obj, Efl_Object *object);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:focus:property:object|Efl.Input.Focus.object]]

