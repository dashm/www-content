~~Title: Efl.Input.Device.device_type~~
====== Efl.Input.Device.device_type ======

===== Description =====

%%Device type property%%

//Since 1.23//
{{page>:develop:api-include:efl:input:device:property:device_type:description&nouser&nolink&nodate}}

===== Values =====

  * **klass** - %%Input device class%%

===== Signature =====

<code>
@property device_type {
    get {}
    set {}
    values {
        klass: Efl.Input.Device_Type;
    }
}
</code>

===== C signature =====

<code c>
Efl_Input_Device_Type efl_input_device_type_get(const Eo *obj);
void efl_input_device_type_set(Eo *obj, Efl_Input_Device_Type klass);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:device:property:device_type|Efl.Input.Device.device_type]]

