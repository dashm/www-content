~~Title: Efl.Input.Device.seat~~
====== Efl.Input.Device.seat ======

===== Values =====

  * **seat** - %%The seat this device belongs to.%%


\\ {{page>:develop:api-include:efl:input:device:property:seat:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property seat {
    get {}
    values {
        seat: Efl.Input.Device;
    }
}
</code>

===== C signature =====

<code c>
Efl_Input_Device *efl_input_device_seat_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:device:property:seat|Efl.Input.Device.seat]]

