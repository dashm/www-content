~~Title: Efl.Input.Key.key_name~~
====== Efl.Input.Key.key_name ======

===== Description =====

%%Name string of the physical key that produced this event.%%

%%This typically matches what is printed on the key. For example, "1" or "a". Note that both "a" and "A" are obtained with the same physical key, so both events will have the same %%[[:develop:api:efl:input:key:property:key_name|Efl.Input.Key.key_name]]%% "a" but different %%[[:develop:api:efl:input:key:property:key_sym|Efl.Input.Key.key_sym]]%%.%%

%%Commonly used in keyboard remapping menus to uniquely identify a physical key.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:key:property:key_name:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%Name of the key that produced this event.%%

===== Signature =====

<code>
@property key_name {
    get {}
    set {}
    values {
        val: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_input_key_name_get(const Eo *obj);
void efl_input_key_name_set(Eo *obj, const char *val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:key:property:key_name|Efl.Input.Key.key_name]]

