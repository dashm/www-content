~~Title: Efl.Input.Key.string~~
====== Efl.Input.Key.string ======

===== Description =====

%%A UTF8 string if this keystroke has produced a visible string to be added.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:key:property:string:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%Visible string produced by this key event, in UTF8.%%

===== Signature =====

<code>
@property string {
    get {}
    set {}
    values {
        val: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_input_key_string_get(const Eo *obj);
void efl_input_key_string_set(Eo *obj, const char *val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:key:property:string|Efl.Input.Key.string]]

