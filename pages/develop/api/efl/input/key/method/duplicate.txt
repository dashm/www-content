~~Title: Efl.Input.Key.duplicate~~
====== Efl.Input.Key.duplicate ======

===== Description =====

%%Creates a copy of this event. %%[[:develop:api:efl:input:event:property:fake|Efl.Input.Event.fake]]%% is %%''true''%%.%%

%%The returned event object is similar to the given object in most ways except that %%[[:develop:api:efl:input:event:property:fake|Efl.Input.Event.fake]]%% will be %%''true''%%.%%

<note>
%%A reference is given to the caller. In order to avoid leaks the C API users should call %%''efl_unref''%%() after use.%%
</note>

//Since 1.23//
{{page>:develop:api-include:efl:input:key:method:duplicate:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:duplicate:method:duplicate|Efl.Duplicate.duplicate]].//===== Signature =====

<code>
duplicate @const @pure_virtual {
    return: Efl.Duplicate;
}
</code>

===== C signature =====

<code c>
Efl_Duplicate *efl_duplicate(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:duplicate:method:duplicate|Efl.Duplicate.duplicate]]
  * [[:develop:api:efl:canvas:vg:node:method:duplicate|Efl.Canvas.Vg.Node.duplicate]]
  * [[:develop:api:efl:canvas:vg:shape:method:duplicate|Efl.Canvas.Vg.Shape.duplicate]]
  * [[:develop:api:efl:canvas:vg:container:method:duplicate|Efl.Canvas.Vg.Container.duplicate]]
  * [[:develop:api:efl:canvas:vg:gradient:method:duplicate|Efl.Canvas.Vg.Gradient.duplicate]]
  * [[:develop:api:efl:canvas:vg:gradient_linear:method:duplicate|Efl.Canvas.Vg.Gradient_Linear.duplicate]]
  * [[:develop:api:efl:canvas:vg:gradient_radial:method:duplicate|Efl.Canvas.Vg.Gradient_Radial.duplicate]]
  * [[:develop:api:efl:input:pointer:method:duplicate|Efl.Input.Pointer.duplicate]]
  * [[:develop:api:efl:input:key:method:duplicate|Efl.Input.Key.duplicate]]
  * [[:develop:api:efl:input:hold:method:duplicate|Efl.Input.Hold.duplicate]]
  * [[:develop:api:efl:input:focus:method:duplicate|Efl.Input.Focus.duplicate]]
  * [[:develop:api:efl:core:env:method:duplicate|Efl.Core.Env.duplicate]]
  * [[:develop:api:efl:core:proc_env:method:duplicate|Efl.Core.Proc_Env.duplicate]]

