~~Title: Efl.Player.playback_position~~
====== Efl.Player.playback_position ======

===== Description =====

%%Position in the media file.%%

%%This property sets the current position of the media file to %%''sec''%% seconds since the beginning of the media file. This only works on seekable streams. Setting the position doesn't change the %%[[:develop:api:efl:player:property:playing|Efl.Player.playing]]%% or %%[[:develop:api:efl:player:property:paused|Efl.Player.paused]]%% states of the media file.%%

//Since 1.23//
{{page>:develop:api-include:efl:player:property:playback_position:description&nouser&nolink&nodate}}

===== Values =====

  * **sec** - %%The position (in seconds).%%

===== Signature =====

<code>
@property playback_position @pure_virtual {
    get {}
    set {}
    values {
        sec: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_player_playback_position_get(const Eo *obj);
void efl_player_playback_position_set(Eo *obj, double sec);
</code>

===== Implemented by =====

  * [[:develop:api:efl:player:property:playback_position|Efl.Player.playback_position]]
  * [[:develop:api:efl:canvas:video:property:playback_position|Efl.Canvas.Video.playback_position]]
  * [[:develop:api:efl:canvas:animation_player:property:playback_position|Efl.Canvas.Animation_Player.playback_position]]
  * [[:develop:api:efl:ui:image:property:playback_position|Efl.Ui.Image.playback_position]]
  * [[:develop:api:efl:ui:image_zoomable:property:playback_position|Efl.Ui.Image_Zoomable.playback_position]]

