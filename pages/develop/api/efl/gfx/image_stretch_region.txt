~~Title: Efl.Gfx.Image_Stretch_Region~~

===== Description =====

%%This struct holds the description of a stretchable region in one dimension (vertical or horizontal). Used when scaling an image.%%

%%%%''offset''%% + %%''length''%% should be smaller than image size in that dimension.%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:image_stretch_region:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:gfx:image_stretch_region:fields&nouser&nolink&nodate}}

  * **offset** - %%First pixel of the stretchable region, starting at 0.%%
  * **length** - %%Length of the stretchable region in pixels.%%

===== Signature =====

<code>
struct Efl.Gfx.Image_Stretch_Region {
    offset: uint;
    length: uint;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Gfx_Image_Stretch_Region {
    unsigned int offset;
    unsigned int length;
} Efl_Gfx_Image_Stretch_Region;
</code>
