~~Title: Efl.Gfx.Hint.hint_fill~~
====== Efl.Gfx.Hint.hint_fill ======

===== Description =====

%%Hints for an object's fill property that used to specify "justify" or "fill" by some users. %%[[:develop:api:efl:gfx:hint:property:hint_fill|Efl.Gfx.Hint.hint_fill]]%% specify whether to fill the space inside the boundaries of a container/manager.%%

%%Maximum hints should be enforced with higher priority, if they are set. Also, any %%[[:develop:api:efl:gfx:hint:property:hint_margin|Efl.Gfx.Hint.hint_margin]]%% set on objects should add up to the object space on the final scene composition.%%

%%See documentation of possible users: in Evas, they are the %%[[:develop:api:efl:ui:box|Efl.Ui.Box]]%% "box" and %%[[:develop:api:efl:ui:table|Efl.Ui.Table]]%% "table" smart objects.%%

%%This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate.%%

<note>
%%Default fill hint values are true, for both axes.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:gfx:hint:property:hint_fill:description&nouser&nolink&nodate}}

===== Values =====

  * **x** - %%%%''true''%% if to fill the object space, %%''false''%% otherwise, to use as horizontal fill hint.%%
  * **y** - %%%%''true''%% if to fill the object space, %%''false''%% otherwise, to use as vertical fill hint.%%

===== Signature =====

<code>
@property hint_fill @pure_virtual {
    get {}
    set {}
    values {
        x: bool;
        y: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_hint_fill_get(const Eo *obj, Eina_Bool *x, Eina_Bool *y);
void efl_gfx_hint_fill_set(Eo *obj, Eina_Bool x, Eina_Bool y);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_fill|Efl.Gfx.Hint.hint_fill]]
  * [[:develop:api:efl:canvas:object:property:hint_fill|Efl.Canvas.Object.hint_fill]]

