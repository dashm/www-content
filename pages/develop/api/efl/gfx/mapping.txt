~~Title: Efl.Gfx.Mapping~~
====== Efl.Gfx.Mapping (mixin) ======

===== Description =====

%%Texture UV mapping for all objects (rotation, perspective, 3d, ...).%%

%%Evas allows different transformations to be applied to all kinds of objects. These are applied by means of UV mapping.%%

%%With UV mapping, one maps points in the source object to a 3D space positioning at target. This allows rotation, perspective, scale and lots of other effects, depending on the map that is used.%%

%%Each map point may carry a multiplier color. If properly calculated, these can do shading effects on the object, producing 3D effects.%%

%%At the moment of writing, maps can only have 4 points (no more, no less).%%

//Since 1.22//

{{page>:develop:api-include:efl:gfx:mapping:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:gfx:mapping:method:constructor|constructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> 
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> 
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:lighting_3d|lighting_3d]]**\\
> %%Apply a lighting effect on the object.%%
<code c>
void efl_gfx_mapping_lighting_3d(Eo *obj, const Efl_Gfx_Entity *pivot, double lx, double ly, double lz, int lr, int lg, int lb, int ar, int ag, int ab);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:lighting_3d_absolute|lighting_3d_absolute]]**\\
> %%Apply a lighting effect to the object.%%
<code c>
void efl_gfx_mapping_lighting_3d_absolute(Eo *obj, double lx, double ly, double lz, int lr, int lg, int lb, int ar, int ag, int ab);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_alpha|mapping_alpha]]** //**(get, set)**//\\
> %%Alpha flag for map rendering.%%
<code c>
Eina_Bool efl_gfx_mapping_alpha_get(const Eo *obj);
void efl_gfx_mapping_alpha_set(Eo *obj, Eina_Bool alpha);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_clockwise|mapping_clockwise]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_gfx_mapping_clockwise_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_color|mapping_color]]** //**(get, set)**//\\
> %%Color of a vertex in the map.%%
<code c>
void efl_gfx_mapping_color_get(const Eo *obj, int idx, int *r, int *g, int *b, int *a);
void efl_gfx_mapping_color_set(Eo *obj, int idx, int r, int g, int b, int a);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_coord_absolute|mapping_coord_absolute]]** //**(get, set)**//\\
> %%A point's absolute coordinate on the canvas.%%
<code c>
void efl_gfx_mapping_coord_absolute_get(const Eo *obj, int idx, double *x, double *y, double *z);
void efl_gfx_mapping_coord_absolute_set(Eo *obj, int idx, double x, double y, double z);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:mapping_has|mapping_has]]**\\
> %%Read-only property indicating whether an object is mapped.%%
<code c>
Eina_Bool efl_gfx_mapping_has(Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_point_count|mapping_point_count]]** //**(get, set)**//\\
> %%Number of points of a map.%%
<code c>
int efl_gfx_mapping_point_count_get(const Eo *obj);
void efl_gfx_mapping_point_count_set(Eo *obj, int count);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:mapping_reset|mapping_reset]]**\\
> %%Resets the map transformation to its default state.%%
<code c>
void efl_gfx_mapping_reset(Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_smooth|mapping_smooth]]** //**(get, set)**//\\
> %%Smoothing state for map rendering.%%
<code c>
Eina_Bool efl_gfx_mapping_smooth_get(const Eo *obj);
void efl_gfx_mapping_smooth_set(Eo *obj, Eina_Bool smooth);
</code>
\\
**[[:develop:api:efl:gfx:mapping:property:mapping_uv|mapping_uv]]** //**(get, set)**//\\
> %%Map point's U and V texture source point.%%
<code c>
void efl_gfx_mapping_uv_get(const Eo *obj, int idx, double *u, double *v);
void efl_gfx_mapping_uv_set(Eo *obj, int idx, double u, double v);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:perspective_3d|perspective_3d]]**\\
> %%Apply a perspective transform to the map%%
<code c>
void efl_gfx_mapping_perspective_3d(Eo *obj, const Efl_Gfx_Entity *pivot, double px, double py, double z0, double foc);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:perspective_3d_absolute|perspective_3d_absolute]]**\\
> %%Apply a perspective transform to the map%%
<code c>
void efl_gfx_mapping_perspective_3d_absolute(Eo *obj, double px, double py, double z0, double foc);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate|rotate]]**\\
> %%Apply a rotation to the object.%%
<code c>
void efl_gfx_mapping_rotate(Eo *obj, double degrees, const Efl_Gfx_Entity *pivot, double cx, double cy);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate_3d|rotate_3d]]**\\
> %%Rotate the object around 3 axes in 3D.%%
<code c>
void efl_gfx_mapping_rotate_3d(Eo *obj, double dx, double dy, double dz, const Efl_Gfx_Entity *pivot, double cx, double cy, double cz);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate_3d_absolute|rotate_3d_absolute]]**\\
> %%Rotate the object around 3 axes in 3D, using absolute coordinates.%%
<code c>
void efl_gfx_mapping_rotate_3d_absolute(Eo *obj, double dx, double dy, double dz, double cx, double cy, double cz);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate_absolute|rotate_absolute]]**\\
> %%Apply a rotation to the object, using absolute coordinates.%%
<code c>
void efl_gfx_mapping_rotate_absolute(Eo *obj, double degrees, double cx, double cy);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate_quat|rotate_quat]]**\\
> %%Rotate the object in 3D using a unit quaternion.%%
<code c>
void efl_gfx_mapping_rotate_quat(Eo *obj, double qx, double qy, double qz, double qw, const Efl_Gfx_Entity *pivot, double cx, double cy, double cz);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:rotate_quat_absolute|rotate_quat_absolute]]**\\
> %%Rotate the object in 3D using a unit quaternion, using absolute coordinates.%%
<code c>
void efl_gfx_mapping_rotate_quat_absolute(Eo *obj, double qx, double qy, double qz, double qw, double cx, double cy, double cz);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:translate|translate]]**\\
> %%Apply a translation to the object using map.%%
<code c>
void efl_gfx_mapping_translate(Eo *obj, double dx, double dy, double dz);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:zoom|zoom]]**\\
> %%Apply a zoom to the object.%%
<code c>
void efl_gfx_mapping_zoom(Eo *obj, double zoomx, double zoomy, const Efl_Gfx_Entity *pivot, double cx, double cy);
</code>
\\
**[[:develop:api:efl:gfx:mapping:method:zoom_absolute|zoom_absolute]]**\\
> %%Apply a zoom to the object, using absolute coordinates.%%
<code c>
void efl_gfx_mapping_zoom_absolute(Eo *obj, double zoomx, double zoomy, double cx, double cy);
</code>
\\

===== Events =====

