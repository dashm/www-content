~~Title: Efl.Gfx.Mapping.mapping_alpha~~
====== Efl.Gfx.Mapping.mapping_alpha ======

===== Description =====

%%Alpha flag for map rendering.%%

%%This sets alpha flag for map rendering. If the object is a type that has its own alpha settings, then this will take precedence. Only image objects support this currently (%%[[:develop:api:efl:canvas:image|Efl.Canvas.Image]]%% and its friends). Setting this to off stops alpha blending of the map area, and is useful if you know the object and/or all sub-objects is 100% solid.%%

%%Note that this may conflict with %%[[:develop:api:efl:gfx:mapping:property:mapping_smooth|Efl.Gfx.Mapping.mapping_smooth]]%% depending on which algorithm is used for anti-aliasing.%%

//Since 1.22//
{{page>:develop:api-include:efl:gfx:mapping:property:mapping_alpha:description&nouser&nolink&nodate}}

===== Values =====

  * **alpha** - %%%%''true''%% by default.%%

===== Signature =====

<code>
@property mapping_alpha {
    get {}
    set {}
    values {
        alpha: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_mapping_alpha_get(const Eo *obj);
void efl_gfx_mapping_alpha_set(Eo *obj, Eina_Bool alpha);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:mapping:property:mapping_alpha|Efl.Gfx.Mapping.mapping_alpha]]

