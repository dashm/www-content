~~Title: Efl.Gfx.Image~~
====== Efl.Gfx.Image (interface) ======

===== Description =====

%%This interface defines a set of common APIs which should be implemented by image objects.%%

%%These APIs provide the ability to manipulate how images will be rendered, e.g., determining whether to allow upscaling and downscaling at render time, as well as functionality for detecting errors during the loading process.%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:image:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:gfx:image:property:border_insets|border_insets]]** //**(get, set)**//\\
> %%Dimensions of this image's border, a region that does not scale with the center area.%%
<code c>
void efl_gfx_image_border_insets_get(const Eo *obj, int *l, int *r, int *t, int *b);
void efl_gfx_image_border_insets_set(Eo *obj, int l, int r, int t, int b);
</code>
\\
**[[:develop:api:efl:gfx:image:property:border_insets_scale|border_insets_scale]]** //**(get, set)**//\\
> %%Scaling factor applied to the image borders.%%
<code c>
double efl_gfx_image_border_insets_scale_get(const Eo *obj);
void efl_gfx_image_border_insets_scale_set(Eo *obj, double scale);
</code>
\\
**[[:develop:api:efl:gfx:image:property:can_downscale|can_downscale]]** //**(get, set)**//\\
> %%If %%''true''%%, the image may be scaled to a smaller size. If %%''false''%%, the image will never be resized smaller than its native size.%%
<code c>
Eina_Bool efl_gfx_image_can_downscale_get(const Eo *obj);
void efl_gfx_image_can_downscale_set(Eo *obj, Eina_Bool downscale);
</code>
\\
**[[:develop:api:efl:gfx:image:property:can_upscale|can_upscale]]** //**(get, set)**//\\
> %%If %%''true''%%, the image may be scaled to a larger size. If %%''false''%%, the image will never be resized larger than its native size.%%
<code c>
Eina_Bool efl_gfx_image_can_upscale_get(const Eo *obj);
void efl_gfx_image_can_upscale_set(Eo *obj, Eina_Bool upscale);
</code>
\\
**[[:develop:api:efl:gfx:image:property:center_fill_mode|center_fill_mode]]** //**(get, set)**//\\
> %%Specifies how the center part of the object (not the borders) should be drawn when EFL is rendering it.%%
<code c>
Efl_Gfx_Center_Fill_Mode efl_gfx_image_center_fill_mode_get(const Eo *obj);
void efl_gfx_image_center_fill_mode_set(Eo *obj, Efl_Gfx_Center_Fill_Mode fill);
</code>
\\
**[[:develop:api:efl:gfx:image:property:content_hint|content_hint]]** //**(get, set)**//\\
> %%Content hint setting for the image. These hints might be used by EFL to enable optimizations.%%
<code c>
Efl_Gfx_Image_Content_Hint efl_gfx_image_content_hint_get(const Eo *obj);
void efl_gfx_image_content_hint_set(Eo *obj, Efl_Gfx_Image_Content_Hint hint);
</code>
\\
**[[:develop:api:efl:gfx:image:property:content_region|content_region]]** //**(get)**//\\
> 
<code c>
Eina_Rect efl_gfx_image_content_region_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:image:property:image_load_error|image_load_error]]** //**(get)**//\\
> 
<code c>
Eina_Error efl_gfx_image_load_error_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:image:property:image_size|image_size]]** //**(get)**//\\
> 
<code c>
Eina_Size2D efl_gfx_image_size_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:image:property:ratio|ratio]]** //**(get)**//\\
> 
<code c>
double efl_gfx_image_ratio_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:image:property:scale_hint|scale_hint]]** //**(get, set)**//\\
> %%The scale hint of a given image of the canvas.%%
<code c>
Efl_Gfx_Image_Scale_Hint efl_gfx_image_scale_hint_get(const Eo *obj);
void efl_gfx_image_scale_hint_set(Eo *obj, Efl_Gfx_Image_Scale_Hint hint);
</code>
\\
**[[:develop:api:efl:gfx:image:property:scale_method|scale_method]]** //**(get, set)**//\\
> %%Determine how the image is scaled at render time.%%
<code c>
Efl_Gfx_Image_Scale_Method efl_gfx_image_scale_method_get(const Eo *obj);
void efl_gfx_image_scale_method_set(Eo *obj, Efl_Gfx_Image_Scale_Method scale_method);
</code>
\\
**[[:develop:api:efl:gfx:image:property:smooth_scale|smooth_scale]]** //**(get, set)**//\\
> %%Whether to use high-quality image scaling algorithm for this image.%%
<code c>
Eina_Bool efl_gfx_image_smooth_scale_get(const Eo *obj);
void efl_gfx_image_smooth_scale_set(Eo *obj, Eina_Bool smooth_scale);
</code>
\\
**[[:develop:api:efl:gfx:image:property:stretch_region|stretch_region]]** //**(get, set)**//\\
> %%This property defines the stretchable pixels region of an image.%%
<code c>
void efl_gfx_image_stretch_region_get(const Eo *obj, Eina_Iterator **horizontal, Eina_Iterator **vertical);
Eina_Error efl_gfx_image_stretch_region_set(Eo *obj, Eina_Iterator *horizontal, Eina_Iterator *vertical);
</code>
\\

===== Events =====

**[[:develop:api:efl:gfx:image:event:image_preload_state_changed|image,preload_state,changed]]**\\
> %%If %%''true''%%, image data has been preloaded and can be displayed. If %%''false''%%, the image data has been unloaded and can no longer be displayed.%%
<code c>
EFL_GFX_IMAGE_EVENT_IMAGE_PRELOAD_STATE_CHANGED(Eina_Bool)
</code>
\\ **[[:develop:api:efl:gfx:image:event:image_resized|image,resized]]**\\
> %%Image was resized (its pixel data). The event data is the image's new size.%%
<code c>
EFL_GFX_IMAGE_EVENT_IMAGE_RESIZED(Eina_Size2D)
</code>
\\ 