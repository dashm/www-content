~~Title: Efl.Generic_Model.properties~~
====== Efl.Generic_Model.properties ======

===== Values =====

  * **properties** - %%Array of current properties%%


\\ {{page>:develop:api-include:efl:generic_model:property:properties:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:model:property:properties|Efl.Model.properties]] **(get)**.//===== Signature =====

<code>
@property properties @pure_virtual {
    get {}
    values {
        properties: iterator<string>;
    }
}
</code>

===== C signature =====

<code c>
Eina_Iterator *efl_model_properties_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:model:property:properties|Efl.Model.properties]]
  * [[:develop:api:efl:mono_model_internal_child:property:properties|Efl.Mono_Model_Internal_Child.properties]]
  * [[:develop:api:efl:mono_model_internal:property:properties|Efl.Mono_Model_Internal.properties]]
  * [[:develop:api:eldbus:model:property:properties|Eldbus.Model.properties]]
  * [[:develop:api:eldbus:model:proxy:property:properties|Eldbus.Model.Proxy.properties]]
  * [[:develop:api:eldbus:model:arguments:property:properties|Eldbus.Model.Arguments.properties]]
  * [[:develop:api:efl:composite_model:property:properties|Efl.Composite_Model.properties]]
  * [[:develop:api:efl:boolean_model:property:properties|Efl.Boolean_Model.properties]]
  * [[:develop:api:efl:ui:select_model:property:properties|Efl.Ui.Select_Model.properties]]
  * [[:develop:api:efl:container_model:property:properties|Efl.Container_Model.properties]]
  * [[:develop:api:efl:ui:view_model:property:properties|Efl.Ui.View_Model.properties]]
  * [[:develop:api:efl:ui:size_model:property:properties|Efl.Ui.Size_Model.properties]]
  * [[:develop:api:efl:io:model:property:properties|Efl.Io.Model.properties]]
  * [[:develop:api:efl:generic_model:property:properties|Efl.Generic_Model.properties]]

