~~Title: EFL Reference~~
====== EFL Reference ======

{{page>:develop:api-include:reference:general&nouser&nolink&nodate}}

===== efl =====

^ Classes ^ Brief description ^
| [[:develop:api:efl:app|Efl.App]] | %%Object representing the application itself.%% |
| [[:develop:api:efl:class|Efl.Class]] | %%Abstract Efl class%% |
| [[:develop:api:efl:composite_model|Efl.Composite_Model]] | %%Efl model for all composite class which provide a unified API to set source of data.%% |
| [[:develop:api:efl:generic_model|Efl.Generic_Model]] | %%Generic model that allows any property to be manually set. Also children of the same type can be added and deleted.%% |
| [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] | %%An %%[[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]]%% is a class which requires one of the parents to provide an %%[[:develop:api:efl:loop|Efl.Loop]]%% interface when performing %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%. It will enforce this by only allowing parents which provide such an interface or %%''NULL''%%.%% |
| [[:develop:api:efl:loop_model|Efl.Loop_Model]] | %%The Efl Loop Model class%% |
| [[:develop:api:efl:loop_timer|Efl.Loop_Timer]] | %%Timers are objects that will call a given callback at some point in the future and repeat that tick at a given interval.%% |
| [[:develop:api:efl:loop|Efl.Loop]] | %%The Efl Main Loop%% |
| [[:develop:api:efl:object|Efl.Object]] | %%Abstract EFL object class.%% |
| [[:develop:api:efl:task|Efl.Task]] | %%EFL's abstraction for a task (process).%% |
^ Interfaces ^ Brief description ^
| [[:develop:api:efl:container|Efl.Container]] | %%Common interface for objects (containers) that can have multiple contents (sub-objects).%% |
| [[:develop:api:efl:content|Efl.Content]] | %%Common interface for objects that have a single sub-object as content.%% |
| [[:develop:api:efl:file_save|Efl.File_Save]] | %%Efl file saving interface%% |
| [[:develop:api:efl:model|Efl.Model]] | %%Basic Model abstraction.%% |
| [[:develop:api:efl:pack_layout|Efl.Pack_Layout]] | %%Low-level APIs for objects that can lay their children out.%% |
| [[:develop:api:efl:pack_linear|Efl.Pack_Linear]] | %%Common interface for objects (containers) with multiple contents (sub-objects) which can be added and removed at runtime in a linear fashion.%% |
| [[:develop:api:efl:pack_table|Efl.Pack_Table]] | %%Interface for 2D containers which arrange their elements on a table with rows and columns.%% |
| [[:develop:api:efl:pack|Efl.Pack]] | %%Common interface for objects (containers) with multiple contents (sub-objects) which can be added and removed at runtime.%% |
| [[:develop:api:efl:part|Efl.Part]] | %%Interface for objects supporting named parts.%% |
| [[:develop:api:efl:player|Efl.Player]] | %%Efl media player interface%% |
| [[:develop:api:efl:screen|Efl.Screen]] | %%Efl screen interface%% |
| [[:develop:api:efl:text|Efl.Text]] | %%Efl text interface%% |
^ Mixins ^ Brief description ^
| [[:develop:api:efl:file|Efl.File]] | %%Efl file interface%% |
^ Aliases ^ Brief description ^
| [[:develop:api:efl:callback_priority|Efl.Callback_Priority]] | %%Callback priority. Range is -32k - 32k. The lower the number, the higher the priority.%% |
^ Structures ^ Brief description ^
| [[:develop:api:efl:event_animator_tick|Efl.Event_Animator_Tick]] | %%EFL event animator tick data structure%% |
| [[:develop:api:efl:event_description|Efl.Event_Description]] | %%This struct holds the description of a specific event.%% |
| [[:develop:api:efl:file_save_info|Efl.File_Save_Info]] | %%Info used to determine various attributes when saving a file.%% |
| [[:develop:api:efl:loop_arguments|Efl.Loop_Arguments]] | %%EFL loop arguments data structure%% |
| [[:develop:api:efl:model_changed_event|Efl.Model_Changed_Event]] | %%Every time the model is changed on the object.%% |
| [[:develop:api:efl:model_property_event|Efl.Model_Property_Event]] | %%EFL model property event data structure%% |
| [[:develop:api:efl:time|Efl.Time]] | %%This type is a alias for struct tm. It is intended to be a standard way to reference it in .eo files.%% |
| [[:develop:api:efl:version|Efl.Version]] | %%This type describes the version of EFL with an optional variant.%% |
^ Enums ^ Brief description ^
| [[:develop:api:efl:task_flags|Efl.Task_Flags]] | %%Flags to further customize task's behavior.%% |
| [[:develop:api:efl:task_priority|Efl.Task_Priority]] | %%How much processor time will this task get compared to other tasks running on the same processor.%% |
| [[:develop:api:efl:text_bidirectional_type|Efl.Text_Bidirectional_Type]] | %%Bidirectionaltext type%% |
^ Constants ^ Brief description ^
| [[:develop:api:efl:callback_priority_after|Efl.Callback_Priority_After]] | %%Slightly less prioritized than default.%% |
| [[:develop:api:efl:callback_priority_before|Efl.Callback_Priority_Before]] | %%Slightly more prioritized than default.%% |
| [[:develop:api:efl:callback_priority_default|Efl.Callback_Priority_Default]] | %%Default priority.%% |

===== efl.canvas =====

^ Classes ^ Brief description ^
| [[:develop:api:efl:canvas:group|Efl.Canvas.Group]] | %%A group object is a container for other canvas objects. Its children move along their parent and are often clipped with a common clipper. This is part of the legacy smart object concept.%% |
| [[:develop:api:efl:canvas:object|Efl.Canvas.Object]] | %%Efl canvas object abstract class%% |
^ Interfaces ^ Brief description ^
| [[:develop:api:efl:canvas:pointer|Efl.Canvas.Pointer]] | %%Efl Canvas Pointer interface%% |
| [[:develop:api:efl:canvas:scene|Efl.Canvas.Scene]] | %%Interface containing basic canvas-related methods and events.%% |

===== efl.gfx =====

^ Interfaces ^ Brief description ^
| [[:develop:api:efl:gfx:entity|Efl.Gfx.Entity]] | %%Efl graphics interface%% |
| [[:develop:api:efl:gfx:hint|Efl.Gfx.Hint]] | %%Efl graphics hint interface%% |
| [[:develop:api:efl:gfx:image_load_controller|Efl.Gfx.Image_Load_Controller]] | %%Common APIs for all loadable 2D images.%% |
| [[:develop:api:efl:gfx:image_orientable|Efl.Gfx.Image_Orientable]] | %%Interface for images which can be rotated or flipped (mirrored).%% |
| [[:develop:api:efl:gfx:image|Efl.Gfx.Image]] | %%This interface defines a set of common APIs which should be implemented by image objects.%% |
| [[:develop:api:efl:gfx:stack|Efl.Gfx.Stack]] | %%Efl graphics stack interface%% |
^ Mixins ^ Brief description ^
| [[:develop:api:efl:gfx:color|Efl.Gfx.Color]] | %%Efl Gfx Color mixin class%% |
| [[:develop:api:efl:gfx:mapping|Efl.Gfx.Mapping]] | %%Texture UV mapping for all objects (rotation, perspective, 3d, ...).%% |
^ Structures ^ Brief description ^
| [[:develop:api:efl:gfx:dash|Efl.Gfx.Dash]] | %%Type describing dash. %%[[:develop:api:efl:gfx:shape:property:stroke_dash|Efl.Gfx.Shape.stroke_dash.set]]%%%% |
| [[:develop:api:efl:gfx:event:render_post|Efl.Gfx.Event.Render_Post]] | %%Data sent along a "render,post" event, after a frame has been rendered.%% |
| [[:develop:api:efl:gfx:gradient_stop|Efl.Gfx.Gradient_Stop]] | %%Type defining gradient stops. Describes the location and color of a transition point in a gradient.%% |
| [[:develop:api:efl:gfx:image_stretch_region|Efl.Gfx.Image_Stretch_Region]] | %%This struct holds the description of a stretchable region in one dimension (vertical or horizontal). Used when scaling an image.%% |
^ Enums ^ Brief description ^
| [[:develop:api:efl:gfx:cap|Efl.Gfx.Cap]] | %%These values determine how the end of opened sub-paths are rendered in a stroke. %%[[:develop:api:efl:gfx:shape:property:stroke_cap|Efl.Gfx.Shape.stroke_cap.set]]%%%% |
| [[:develop:api:efl:gfx:center_fill_mode|Efl.Gfx.Center_Fill_Mode]] | %%How an image's center region (the complement to the border region) should be rendered by EFL%% |
| [[:develop:api:efl:gfx:change_flag|Efl.Gfx.Change_Flag]] | %%What property got changed for this object%% |
| [[:develop:api:efl:gfx:gradient_spread|Efl.Gfx.Gradient_Spread]] | %%Specifies how the area outside the gradient area should be filled. %%[[:develop:api:efl:gfx:gradient:property:spread|Efl.Gfx.Gradient.spread.set]]%%%% |
| [[:develop:api:efl:gfx:hint_aspect|Efl.Gfx.Hint_Aspect]] | %%Aspect types/policies for scaling size hints.%% |
| [[:develop:api:efl:gfx:image_content_hint|Efl.Gfx.Image_Content_Hint]] | %%How an image's data is to be treated by EFL, for optimization.%% |
| [[:develop:api:efl:gfx:image_orientation|Efl.Gfx.Image_Orientation]] | %%An orientation type, to rotate and flip images.%% |
| [[:develop:api:efl:gfx:image_scale_hint|Efl.Gfx.Image_Scale_Hint]] | %%How an image's data is to be treated by EFL, with regard to scaling cache.%% |
| [[:develop:api:efl:gfx:image_scale_method|Efl.Gfx.Image_Scale_Method]] | %%Enumeration that defines scaling methods to be used when rendering an image.%% |
| [[:develop:api:efl:gfx:join|Efl.Gfx.Join]] | %%These values determine how two joining lines are rendered in a stroker. %%[[:develop:api:efl:gfx:shape:property:stroke_join|Efl.Gfx.Shape.stroke_join.set]]%%%% |
| [[:develop:api:efl:gfx:path_command_type|Efl.Gfx.Path_Command_Type]] | %%These values determine how the points are interpreted in a stream of points.%% |
| [[:develop:api:efl:gfx:render_op|Efl.Gfx.Render_Op]] | %%Graphics render operation mode%% |
^ Constants ^ Brief description ^
| [[:develop:api:efl:gfx:hint_align_bottom|Efl.Gfx.Hint_Align_Bottom]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%% |
| [[:develop:api:efl:gfx:hint_align_center|Efl.Gfx.Hint_Align_Center]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%% |
| [[:develop:api:efl:gfx:hint_align_left|Efl.Gfx.Hint_Align_Left]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%% |
| [[:develop:api:efl:gfx:hint_align_right|Efl.Gfx.Hint_Align_Right]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%% |
| [[:develop:api:efl:gfx:hint_align_top|Efl.Gfx.Hint_Align_Top]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%% |
| [[:develop:api:efl:gfx:hint_expand|Efl.Gfx.Hint_Expand]] | %%Use with %%[[:develop:api:efl:gfx:hint:property:hint_weight|Efl.Gfx.Hint.hint_weight]]%%.%% |
| [[:develop:api:efl:gfx:stack_layer_max|Efl.Gfx.Stack_Layer_Max]] | %%top-most layer number%% |
| [[:develop:api:efl:gfx:stack_layer_min|Efl.Gfx.Stack_Layer_Min]] | %%bottom-most layer number%% |

===== efl.input =====

^ Classes ^ Brief description ^
| [[:develop:api:efl:input:device|Efl.Input.Device]] | %%Represents a pointing device such as a touch finger, pen or mouse.%% |
| [[:develop:api:efl:input:focus|Efl.Input.Focus]] | %%Represents a focus event.%% |
| [[:develop:api:efl:input:hold|Efl.Input.Hold]] | %%Event data sent when inputs are put on hold or resumed.%% |
| [[:develop:api:efl:input:key|Efl.Input.Key]] | %%Represents a single key event from a keyboard or similar device.%% |
| [[:develop:api:efl:input:pointer|Efl.Input.Pointer]] | %%Event data carried over with any pointer event (mouse, touch, pen, ...)%% |
^ Interfaces ^ Brief description ^
| [[:develop:api:efl:input:interface|Efl.Input.Interface]] | %%An object implementing this interface can send pointer events.%% |
| [[:develop:api:efl:input:state|Efl.Input.State]] | %%Efl input state interface.%% |
^ Mixins ^ Brief description ^
| [[:develop:api:efl:input:clickable|Efl.Input.Clickable]] | %%Efl input clickable interface.%% |
| [[:develop:api:efl:input:event|Efl.Input.Event]] | %%Represents a generic event data.%% |
^ Structures ^ Brief description ^
| [[:develop:api:efl:input:clickable_clicked|Efl.Input.Clickable_Clicked]] | %%A struct that expresses a click in elementary.%% |
^ Enums ^ Brief description ^
| [[:develop:api:efl:input:device_type|Efl.Input.Device_Type]] | %%General type of input device.%% |
| [[:develop:api:efl:input:flags|Efl.Input.Flags]] | %%Special flags set during an input event propagation.%% |
| [[:develop:api:efl:input:lock|Efl.Input.Lock]] | %%Key locks such as Num Lock, Scroll Lock and Caps Lock.%% |
| [[:develop:api:efl:input:modifier|Efl.Input.Modifier]] | %%Key modifiers such as Control, Alt, etc...%% |

===== efl.io =====

^ Interfaces ^ Brief description ^
| [[:develop:api:efl:io:closer|Efl.Io.Closer]] | %%Generic interface for objects that can close themselves.%% |
| [[:develop:api:efl:io:reader|Efl.Io.Reader]] | %%Generic interface for objects that can read data into a provided memory.%% |
| [[:develop:api:efl:io:writer|Efl.Io.Writer]] | %%Generic interface for objects that can write data from a provided memory.%% |

===== efl.layout =====

^ Interfaces ^ Brief description ^
| [[:develop:api:efl:layout:calc|Efl.Layout.Calc]] | %%This interface defines a common set of APIs used to trigger calculations with layout objects.%% |
| [[:develop:api:efl:layout:group|Efl.Layout.Group]] | %%APIs representing static data from a group in an edje file.%% |
| [[:develop:api:efl:layout:signal|Efl.Layout.Signal]] | %%Layouts asynchronous messaging and signaling interface.%% |

===== efl.ui =====

^ Classes ^ Brief description ^
| [[:develop:api:efl:ui:alert_popup|Efl.Ui.Alert_Popup]] | %%A variant of %%[[:develop:api:efl:ui:popup|Efl.Ui.Popup]]%% which uses a layout containing a content object and a variable number of buttons (up to 3 total).%% |
| [[:develop:api:efl:ui:bg|Efl.Ui.Bg]] | %%The bg (background) widget is used for setting (solid) background decorations for a window (unless it has transparency enabled) or for any container object. It works just like an image, but has some properties useful for backgrounds, such as setting it to tiled, centered, scaled or stretched.%% |
| [[:develop:api:efl:ui:box_flow|Efl.Ui.Box_Flow]] | %%A Flow Box is a customized type of %%[[:develop:api:efl:ui:box|Efl.Ui.Box]]%%. It will fill along the axis selected with %%[[:develop:api:efl:ui:layout_orientable:property:orientation|Efl.Ui.Layout_Orientable.orientation]]%% (which defaults to Horizontal), until items will no longer fit in the available space, at which point it will begin filling items in a new row/column after the current one. This is useful if an application wants to e.g., present a group of items and wrap them onto subsequent lines when the number of items grows too large to fit on the screen. Adding or removing items in the middle re-arrange the rest of the items as expected.%% |
| [[:develop:api:efl:ui:box_stack|Efl.Ui.Box_Stack]] | %%A custom layout engine for %%[[:develop:api:efl:ui:box|Efl.Ui.Box]]%% that stacks items.%% |
| [[:develop:api:efl:ui:box|Efl.Ui.Box]] | %%A container that arranges children widgets in a vertical or horizontal fashion.%% |
| [[:develop:api:efl:ui:button|Efl.Ui.Button]] | %%Push-button widget%% |
| [[:develop:api:efl:ui:check|Efl.Ui.Check]] | %%Check widget.%% |
| [[:develop:api:efl:ui:collection_view|Efl.Ui.Collection_View]] | %%This widget displays a list of items in an arrangement controlled by an external %%[[:develop:api:efl:ui:collection_view:property:position_manager|Efl.Ui.Collection_View.position_manager]]%% object. By using different %%[[:develop:api:efl:ui:collection_view:property:position_manager|Efl.Ui.Collection_View.position_manager]]%% objects this widget can show unidimensional lists or two-dimensional grids of items, for example.%% |
| [[:develop:api:efl:ui:collection|Efl.Ui.Collection]] | %%This widget displays a list of items in an arrangement controlled by an external %%[[:develop:api:efl:ui:collection:property:position_manager|Efl.Ui.Collection.position_manager]]%% object. By using different %%[[:develop:api:efl:ui:collection:property:position_manager|Efl.Ui.Collection.position_manager]]%% objects this widget can show unidimensional lists or two-dimensional grids of items, for example.%% |
| [[:develop:api:efl:ui:datepicker|Efl.Ui.Datepicker]] | %%Datepicker widget%% |
| [[:develop:api:efl:ui:default_item|Efl.Ui.Default_Item]] | %%Default Item Class.%% |
| [[:develop:api:efl:ui:grid_default_item|Efl.Ui.Grid_Default_Item]] | %%Default Item class to be used inside %%[[:develop:api:efl:ui:grid|Efl.Ui.Grid]]%% containers. The %%''icon''%% part is in the middle, the %%''extra''%% part overlaps it on its upper-right corner. The %%''text''%% part is centered below the %%''icon''%%. Theming can change this arrangement.%% |
| [[:develop:api:efl:ui:grid|Efl.Ui.Grid]] | %%A scrollable grid of %%[[:develop:api:efl:ui:item|Efl.Ui.Item]]%% objects, typically %%[[:develop:api:efl:ui:grid_default_item|Efl.Ui.Grid_Default_Item]]%% objects.%% |
| [[:develop:api:efl:ui:image_zoomable|Efl.Ui.Image_Zoomable]] | %%Elementary Image Zoomable class%% |
| [[:develop:api:efl:ui:image|Efl.Ui.Image]] | %%Efl UI image class%% |
| [[:develop:api:efl:ui:item|Efl.Ui.Item]] | %%Selectable Item abstraction.%% |
| [[:develop:api:efl:ui:layout_base|Efl.Ui.Layout_Base]] | %%EFL layout widget abstract.%% |
| [[:develop:api:efl:ui:layout_part_bg|Efl.Ui.Layout_Part_Bg]] | %%Elementary layout internal part background class%% |
| [[:develop:api:efl:ui:layout_part_box|Efl.Ui.Layout_Part_Box]] | %%Represents a Box created as part of a layout.%% |
| [[:develop:api:efl:ui:layout_part_content|Efl.Ui.Layout_Part_Content]] | %%Elementary layout internal part class%% |
| [[:develop:api:efl:ui:layout_part_table|Efl.Ui.Layout_Part_Table]] | %%Represents a Table created as part of a layout.%% |
| [[:develop:api:efl:ui:layout_part_text|Efl.Ui.Layout_Part_Text]] | %%Elementary layout internal part class%% |
| [[:develop:api:efl:ui:layout_part|Efl.Ui.Layout_Part]] | %%Elementary layout internal part class%% |
| [[:develop:api:efl:ui:layout|Efl.Ui.Layout]] | %%EFL layout widget class.%% |
| [[:develop:api:efl:ui:list_default_item|Efl.Ui.List_Default_Item]] | %%Default Item class to be used inside %%[[:develop:api:efl:ui:list|Efl.Ui.List]]%% containers. It displays the three parts in horizontal order: %%''icon''%%, %%''text''%% and %%''extra''%%. Theming can change this arrangement.%% |
| [[:develop:api:efl:ui:list|Efl.Ui.List]] | %%A scrollable list of %%[[:develop:api:efl:ui:item|Efl.Ui.Item]]%% objects, typically %%[[:develop:api:efl:ui:list_default_item|Efl.Ui.List_Default_Item]]%% objects.%% |
| [[:develop:api:efl:ui:popup_part_backwall|Efl.Ui.Popup_Part_Backwall]] | %%A Popup backwall is the background object for an %%[[:develop:api:efl:ui:popup|Efl.Ui.Popup]]%% widget. It can be returned from a given Popup widget by using the %%[[:develop:api:efl:part|Efl.Part]]%% API to fetch the "backwall" part.%% |
| [[:develop:api:efl:ui:popup|Efl.Ui.Popup]] | %%A styled container widget which overlays a window's contents.%% |
| [[:develop:api:efl:ui:radio_group_impl|Efl.Ui.Radio_Group_Impl]] | %%Object with the default implementation for %%[[:develop:api:efl:ui:radio_group|Efl.Ui.Radio_Group]]%%.%% |
| [[:develop:api:efl:ui:radio|Efl.Ui.Radio]] | %%Elementary radio button class.%% |
| [[:develop:api:efl:ui:scroller|Efl.Ui.Scroller]] | %%Widget container that allows objects bigger than itself to be put inside it, and provides scrolling functionality so the whole content is visible.%% |
| [[:develop:api:efl:ui:slider|Efl.Ui.Slider]] | %%Elementary slider class%% |
| [[:develop:api:efl:ui:spin_button|Efl.Ui.Spin_Button]] | %%A Button Spin.%% |
| [[:develop:api:efl:ui:spin|Efl.Ui.Spin]] | %%A Spin.%% |
| [[:develop:api:efl:ui:table|Efl.Ui.Table]] | %%Widget container that arranges its elements in a grid.%% |
| [[:develop:api:efl:ui:timepicker|Efl.Ui.Timepicker]] | %%Timepicker widget%% |
| [[:develop:api:efl:ui:view_model|Efl.Ui.View_Model]] | %%Efl model providing helpers for custom properties used when linking a model to a view and you need to generate/adapt values for display.%% |
| [[:develop:api:efl:ui:widget_factory|Efl.Ui.Widget_Factory]] | %%Efl Ui Factory that provides %%[[:develop:api:efl:ui:widget|Efl.Ui.Widget]]%%.%% |
| [[:develop:api:efl:ui:widget_part_bg|Efl.Ui.Widget_Part_Bg]] | %%Elementary widget internal part background class%% |
| [[:develop:api:efl:ui:widget_part_shadow|Efl.Ui.Widget_Part_Shadow]] | %%A drop-shadow or glow effect around any widget.%% |
| [[:develop:api:efl:ui:widget_part|Efl.Ui.Widget_Part]] | %%This is the base class for all "Part" handles in Efl.Ui widgets.%% |
| [[:develop:api:efl:ui:widget|Efl.Ui.Widget]] | %%Base class for all Efl.Ui.* widgets%% |
| [[:develop:api:efl:ui:win|Efl.Ui.Win]] | %%Efl UI window class.%% |
^ Interfaces ^ Brief description ^
| [[:develop:api:efl:ui:autorepeat|Efl.Ui.Autorepeat]] | %%Interface for autorepeating clicks.%% |
| [[:develop:api:efl:ui:factory_bind|Efl.Ui.Factory_Bind]] | %%Efl UI Property interface. view object can have %%[[:develop:api:efl:model|Efl.Model]]%% and need to set cotent with those model stored data. the interface can help binding the factory to create object with model property data. see %%[[:develop:api:efl:model|Efl.Model]]%% see %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%%%% |
| [[:develop:api:efl:ui:factory|Efl.Ui.Factory]] | %%Interface for factory-pattern object creation.%% |
| [[:develop:api:efl:ui:focus:manager_window_root|Efl.Ui.Focus.Manager_Window_Root]] | %%An interface to indicate the end of a focus chain.%% |
| [[:develop:api:efl:ui:focus:manager|Efl.Ui.Focus.Manager]] | %%Interface for managing focus objects.%% |
| [[:develop:api:efl:ui:layout_orientable|Efl.Ui.Layout_Orientable]] | %%Interface for UI objects which can have more than one orientation.%% |
| [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]] | %%Efl UI Property_Bind interface. view object can have %%[[:develop:api:efl:model|Efl.Model]]%% to manage the data, the interface can help loading and tracking child data from the model property. see %%[[:develop:api:efl:model|Efl.Model]]%% see %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%%%% |
| [[:develop:api:efl:ui:radio_group|Efl.Ui.Radio_Group]] | %%Interface for manually handling a group of %%[[:develop:api:efl:ui:radio|Efl.Ui.Radio]]%% buttons.%% |
| [[:develop:api:efl:ui:range_display|Efl.Ui.Range_Display]] | %%Interface that contains properties regarding the displaying of a value within a range.%% |
| [[:develop:api:efl:ui:range_interactive|Efl.Ui.Range_Interactive]] | %%Interface that extends the normal displaying properties with usage properties.%% |
| [[:develop:api:efl:ui:scrollable|Efl.Ui.Scrollable]] | %%Interface for widgets capable of displaying content through a viewport, which might be smaller than the actual content. This interface does not control how the content is added. This is typically done through %%[[:develop:api:efl:content|Efl.Content]]%%.%% |
| [[:develop:api:efl:ui:scrollbar|Efl.Ui.Scrollbar]] | %%Interface used by widgets which can display scrollbars, enabling them to hold more content than actually visible through the viewport. A scrollbar contains a draggable part (thumb) which allows the user to move the viewport around the content. The size of the thumb relates to the size of the viewport compared to the whole content.%% |
| [[:develop:api:efl:ui:selectable|Efl.Ui.Selectable]] | %%Selectable interface for UI objects%% |
| [[:develop:api:efl:ui:single_selectable|Efl.Ui.Single_Selectable]] | %%Interface for getting access to a single selected item in the implementor.%% |
| [[:develop:api:efl:ui:view|Efl.Ui.View]] | %%Efl UI view interface.%% |
^ Mixins ^ Brief description ^
| [[:develop:api:efl:ui:focus:object|Efl.Ui.Focus.Object]] | %%Functions of focusable objects.%% |
| [[:develop:api:efl:ui:format|Efl.Ui.Format]] | %%Helper mixin that simplifies converting numerical values to text.%% |
| [[:develop:api:efl:ui:widget_focus_manager|Efl.Ui.Widget_Focus_Manager]] | %%Helper mixin for widgets which also can act as focus managers.%% |
^ Aliases ^ Brief description ^
| [[:develop:api:efl:ui:format_func|Efl.Ui.Format_Func]] | %%A function taking an %%[[:develop:api:eina:value|Eina.Value]]%% and producing its textual representation. See %%[[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]%%.%% |
^ Structures ^ Brief description ^
| [[:develop:api:efl:ui:alert_popup_button_clicked_event|Efl.Ui.Alert_Popup_Button_Clicked_Event]] | %%Information for %%[[:develop:api:efl:ui:alert_popup:event:button,clicked|Efl.Ui.Alert_Popup.button,clicked]]%% event.%% |
| [[:develop:api:efl:ui:factory_item_created_event|Efl.Ui.Factory_Item_Created_Event]] | %%EFL UI Factory event structure provided when an item was just created.%% |
| [[:develop:api:efl:ui:focus:manager_logical_end_detail|Efl.Ui.Focus.Manager_Logical_End_Detail]] | %%Structure holding the focus object with extra information on logical end.%% |
| [[:develop:api:efl:ui:format_value|Efl.Ui.Format_Value]] | %%A value which should always be displayed as a specific text string. See %%[[:develop:api:efl:ui:format:property:format_values|Efl.Ui.Format.format_values]]%%.%% |
| [[:develop:api:efl:ui:property_event|Efl.Ui.Property_Event]] | %%EFL Ui property event data structure triggered when an object property change due to the interaction on the object.%% |
| [[:develop:api:efl:ui:widget_focus_state|Efl.Ui.Widget_Focus_State]] | %%All relevant fields needed for the current state of focus registration%% |
^ Enums ^ Brief description ^
| [[:develop:api:efl:ui:alert_popup_button|Efl.Ui.Alert_Popup_Button]] | %%Defines the type of the alert button.%% |
| [[:develop:api:efl:ui:focus:direction|Efl.Ui.Focus.Direction]] | %%Focus directions.%% |
| [[:develop:api:efl:ui:focus:move_policy|Efl.Ui.Focus.Move_Policy]] | %%Focus Movement Policy.%% |
| [[:develop:api:efl:ui:format_string_type|Efl.Ui.Format_String_Type]] | %%Type of formatting string.%% |
| [[:develop:api:efl:ui:layout_orientation|Efl.Ui.Layout_Orientation]] | %%Orientation for UI objects and layouts that can have multiple configurations.%% |
| [[:develop:api:efl:ui:popup_align|Efl.Ui.Popup_Align]] | %%This is the alignment method for positioning Popup widgets.%% |
| [[:develop:api:efl:ui:scrollbar_mode|Efl.Ui.Scrollbar_Mode]] | %%When should the scrollbar be shown.%% |
| [[:develop:api:efl:ui:win_indicator_mode|Efl.Ui.Win_Indicator_Mode]] | %%Defines the type indicator that can be shown.%% |
| [[:develop:api:efl:ui:win_move_resize_mode|Efl.Ui.Win_Move_Resize_Mode]] | %%Define the move or resize mode of a window.%% |

