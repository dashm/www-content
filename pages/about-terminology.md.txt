---
~~Title: About Terminology~~
~~NOCACHE~~
---

# Terminology #

[Download Terminology Here](/download)

[![](https://hosted.weblate.org/widgets/terminology/-/svg-badge.svg)](https://hosted.weblate.org/engage/terminology/)

![Terminology Icon](/_media/icon-terminology.png)

Terminology is a terminal emulator for Linux/BSD/UNIX systems that uses EFL. It has a whole bunch of bells and whistles. Use it as your regular vt100 terminal emulator with all the usual features, such as 256 color support. Terminology is designed to emulate Xterm as closely as possible in most respects.

![](/_media/aa/shot-2021-12-21_13-39-03.png)

As it uses EFL, Terminology works in X11, under a Wayland compositor and even directly in the framebuffer on Linux. This allows you to replace your boring text-mode VT with a graphical one which requires no display system.

Terminology also has config panels. Simply click the right mouse button or hold the left one down for about a second to bring up the configuration menu. You can even customize the colors to your own liking or just use the ones which come with your default theme.

![](/_media/aa/shot-2021-12-21_13-40-12.png)

Run all your regular terminal apps such as *top*, *htop*, *ls*, *emacs*, *vim* and *mc* as always and enjoy one of the fastest terminal emulators around in terms of  handling I/O. 

Terminology will keep scrollback in RAM, not on any file on disk, so as to keep your data secure. Scrollback is also compressed on the fly to save space. It can even use OpenGL to render if you have configured the acceleration preferences for *Enlightenment Foundation Libraries (EFL)*.

Terminology displays an easy to see bell to let you know something is wrong, as well as a sound to get your attention. This can be turned off by muting alerts.

![](/_media/aa/shot-2021-12-21_13-40-53.png)

Terminology understands full file paths, URL links and email addresses and will underline them on mouse-over. Click to get more details such as gravatar information for an e-mail address or to download the file from a URL. If the URL is a link to a video or animated gif, Terminology will play it. Otherwise the data will just be displayed.

![](/_media/aa/shot-2021-12-21_13-42-34.png)

Terminology also displays a progress bar to let you know how downloads are going.

![](/_media/aa/shot-2021-12-21_13-43-56.png)

Local files are displayed instantly inside the Terminal. This saves you the trouble of opening another GUI application if you only want a quick preview of a file or URL. That said, Terminology can open files with external application helpers if you wish.

![](/_media/aa/shot-2021-12-21_13-46-17.png)

Terminology lets you see cats being silly in animated gifs when no web browser is available. You can even use tycat, a special cat tool that provides metadata for Terminology via escape sequences, to literally ``cat`` content inline in the terminal itself. The utility remembers files in scrollback, even videos with sound can be replayed, searched and paused.

![](/_media/aa/shot-2021-12-21_13-48-16.png)

Terminology can also display image files in all their alpha channel splendor. Even SVG, PDF and PS files will display and scale correctly. If you have [Libreoffice](https://www.libreoffice.org/) installed, you can
even ``cat`` PPT, ODP, DOC and even XLS files if you wish.

![](/_media/aa/shot-2021-12-21_13-50-24.png)

You don't have to just click links or use *typop+ or *tycat* to open files - you can set them as backgrounds too. Use ``tybg`` to set a background using any file you like, from simple PNGs or JPGs, to SVGs or MP4 video files. Terminology also supports animated GIFs and PPT files. If, for instance, you want to reminisce over a particularly good presentation you created on market innovation, set it as your background and enjoy it every day. Similarly if you have a favorite video of cats falling over, this can serve as your background too. 

![](/_media/aa/shot-2021-12-21_13-52-32.png)

Terminology also supports translucency, so you can see what's going on behind the Terminal. Set this to 0% if you wish to get rid of the background entirely.

![](/_media/aa/shot-2021-12-21_13-53-39.png)

Terminology also supports tabs. You can arrange them across the top of the Terminal with individual titles in the usual way. However if you've chosen a video background, it will still play across tabs. Videos in hidden tabs will be "paused" (actually, entirely evicted from memory) until you reopen the tab again. 

![](/_media/aa/shot-2021-12-21_14-18-48.png)

Click the 4 boxes at the top-right of a terminal, or hit CTRL+SHIFT+Home to enter *Tab switcher* mode. A grid of terminals will appear. You can navigate and select them with your mouse or keyboard. All the mini versions of each Terminal window are live and show all current content complete with background, even if this is a video. 

![](/_media/aa/shot-2021-12-21_13-56-07.png)

Terminology can split the terminal into vertical or horizontal panes. Splits can nest, so you can divide panes as many times as you like. They are each resizable and can hold tabs. Feel free to slice and dice your terminal any way you wish.

![](/_media/aa/shot-2021-12-21_13-58-41.png)

Split Terminology panes act in the same way as using Terminology in a single window. You can play videos, display wallpapers and update your content as normal.

![](/_media/aa/shot-2021-12-21_14-00-32.png)

Terminology has many configuration options. Simply right click or hold down the left mouse button to open up *Settings*. From here, you can customize the theme, background, font, sizing and more. Terminology will automatically save your changes unless you choose *Temporary*.

![](/_media/aa/shot-2021-12-21_14-01-09.png)

See our video below for more of Terminology's features.

Note that Terminology is in constant development, so the best way to see everything Terminology has to offer is to download it and try it for yourself:

Terminology has been translated into several languages already. If your language is missing or incomplete, please use [Weblate](https://hosted.weblate.org/engage/terminology/) to help translate Terminology.

Code quality - [Coverity scan status](https://scan.coverity.com/projects/889)
