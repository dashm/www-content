=== Enlightenment 0.25.1 Release ===
  * //2022-01-03 - by Carsten Haitzler//

This is the a bugfix release of Enlightenment

Hilights:
  * windows - netxcloud app - fix constant show/hide cycles
  * gesture - vm (vbox) detect hack to work around xorg no display bug
  * border list - fix list to filter volume out

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.25.1.tar.xz | enlightenment-0.25.1.tar.xz ]] | 2cf05fe3d96ef35e823619dbc0ac513ecabcae2186800ecd804924a637112444 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README | EFL]]
  - libexif
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the everything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.25.1 depends on EFL **v1.26.0** or newer.

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
