=== Terminology 1.8.0 Release ===
  * //2020-07-26 - by Boris Faure//

> “It does not matter how slowly you go as long as you do not stop.” ― Confucius

Latest release was only two months ago but there is already some new material to be enjoyed!
The detailed change log lists them below:

== Additions ==
    * Small framework to add unit tests
    * Display tooltips when hovering color descriptions
    * Handle ''OSC 10/11'' to change/get background and foreground colors

== Improvements ==
    * Reworked build system for testing and fuzzing
    * Use of switch-case constructs when home-made binary search was not efficient
    * Support EFL 1-22 or newer only
    * Larger list of word separators when doing word-selection
    * Reworked the Settings panel to add one panel on Mouse interactions
    * Handle Emoji characters as double-width, following Unicode 13.0

== Fixes ==
    * Fix issues detected by UndefinedBehavior Sanitizer

== Download ==

^ ** LINK ** ^ ** SHA256 ** ^
| [[ https://download.enlightenment.org/rel/apps/terminology/terminology-1.8.0.tar.xz | Terminology 1.8.0 XZ]]  | ''c6f5b003412f25507277702cabe1a11d7190971343c1d6030aa7d3fe5b45765f'' |

== In action ==
The color tooltips can be seen below:
{{ :news:color_tooltip.gif |}}
The change on which character is rendered on double-width is best illustrated with smileys about love:
{{ :news:tylove.png |}}

== Social Media ==

If you want to know more about what's going on with Terminology, follow us on [[https://twitter.com/_Terminology_|twitter @_Terminology_]]

There is also a dedicated [[https://www.youtube.com/channel/UCZ2iBYbbxvcZfcUmnz-rmlQ|Youtube channel about Terminology]].

The latest video shows all features around links in Terminology:
{{youtube>Q3uoFsWQuFQ}}

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~