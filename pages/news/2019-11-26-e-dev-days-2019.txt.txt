=== Enlightenment Developer Days 2019 ===
  * //2019-11-28 - by Xavi Artigas//

On November 23 part of the Enlightenment community gathered for two days in Barcelona, Spain, to hold the Enlightenment Developer Days.

Except for the hiatus in 2018, this tradition has been honored uninterruptedly since 2012, when it was also held in Barcelona.

Over the weekend, 14 people from 7 countries presented their latest work, discussed their current problems, and talked about what lies ahead.

Attendees included Enlightenment Desktop users, application developers, and core maintainers, producing a very interesting mixture. The outcome was varied: doubts were dispelled, action items for the upcoming months were laid out and, most importantly, the community was strengthened. For those of us who attended for the first time, it was a pleasure to put a face to the IRC nicks!

I had the honor of being the host this time, and I enjoyed the opportunity to show my enlightened fellows around the city and take them out to have some Spanish tapas for dinner. Thanks a lot for coming!

Thanks also to the Pompeu Fabra University who allowed us to use their facilities free of charge.

Finally, a big thank you to Stefan Schmidt for organizing everything and getting Samsung to sponsor the meals for both days.

See you all, and a bunch more, next year!

{{ :news:edd2019_pic1.jpg?800,direct | Attendees hard at work!}}
<html><center></html>
Left, front to back, and then back to front:
[[https://phab.enlightenment.org/p/escwyp|escwyp]],
[[https://phab.enlightenment.org/p/netstar|netstar]],
[[https://phab.enlightenment.org/p/rafspiny|rafspiny]],
[[https://phab.enlightenment.org/p/bu5hm4n|bu5hm4n]],
[[https://phab.enlightenment.org/p/cedric|cedric]],
[[https://phab.enlightenment.org/p/herb|herb]],
[[https://phab.enlightenment.org/p/raster|raster]],
[[https://phab.enlightenment.org/p/q66|q66]],
[[https://phab.enlightenment.org/p/michael.bouchaud|yoz]],
[[https://phab.enlightenment.org/p/vtorri|vtorri]],
[[https://phab.enlightenment.org/p/peter2121|peter2121]],
[[https://phab.enlightenment.org/p/SanghyeonLee|SanghyeonLee]],
[[https://phab.enlightenment.org/p/stefan_schmidt|stefan]]. 
Picture: [[https://phab.enlightenment.org/p/SegFaultXavi|Yours truly]].
<html></center></html>

{{ :news:edd2019_pic2.jpg?800,direct | Tapas!}}
<html><center></html>Tapas!<html></center></html>

{{ :news:edd2019_pic3.jpg?800,direct | And some light supper for the meat lovers.}}
<html><center></html>And some light supper for the meat lovers.<html></center></html>

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~

