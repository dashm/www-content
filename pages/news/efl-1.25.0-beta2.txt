=== EFL 1.25.0 beta 2 ===
  * //2020-09-07 - by Stefan Schmidt//

This beta 2 release contains more fixes build up from our last weeks beta. So far no release stopper showed up, which means we might see the final release already next week. Fingers crossed.

**Fixes:**
   * evas_textblock: remove style padding from native width and formatted height
   * elm icon/thumb - fix handling of a wider range of video extns
   * edje - calc - handle swallow disappearing mid-calc due to callabcks
   * ecore drm - increase timeout for flips from 0.05 to 2.0 sec

**Download**

|LINK|	SHA256|
| http://download.enlightenment.org/rel/libs/efl/efl-1.25.0-beta2.tar.xz | 76684fdd011915f0af80a2df3466c32f482fabd9a0723cac74cd14aed7aef260 |


{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
