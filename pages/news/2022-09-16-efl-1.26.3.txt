=== Enlightenment Foundation Libraries 1.26.3 Release ===
  * //2022-09-16 - by Carsten Haitzler//

This is a bug-fix release.

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.26.3.tar.xz | efl-1.26.3.tar.xz ]] | d9f83aa0fd9334f44deeb4e4952dc0e5144683afac786feebce6030951617d15 |

----

== What's New ==

  * Fix theme issues
  * Fix edje calc internal return values in some cases
  * Fix ecore-x netwm moveresize request handling
  * Fix ecore-x xkb handling to not leak
  * Fix new xkb event handling
  * Fix evas smart object to null out fields for safety
  * Fix elm null cnp object handling to not crash in odd cases
  * Fix ecore evas x11 dnd handling to not null out xdnd props
  * Fix data checkme file so relocation at runtime works again
  * Fix eina bt output to always have spaces between fields
  * Fix png loader on arm to not have rgb values when a is 0
  * Fix textgrid to guard against null glyphs
  * Fix ecore-x vblank to use current time not vlbank time
  * Fix emile to handle invalid compress types more gracefully
  * Fix compile warnings
  * Fix evas psd loader crash
  * Fix osx sample compile conf script options
  * Fix wl vsync timer to use current time not vblank time
  * Work around xwayland issue where synthetic events are not sent

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
