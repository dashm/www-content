=== Ephoto 1.6 Release ===
  * //2021-12-26 - by Carsten Haitzler//

This is the 1.6 release of Ephoto - a photo gallery app using EFL.

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/apps/ephoto/ephoto-1.6.0.tar.xz | ephoto-1.6.0.tar.xz ]] | 36bc73f3ce1bf9c606630d0031d1629a115911e78b796fd1f90322b5886670d3 |

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~

