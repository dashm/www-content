=== EFL 1.22.6 release ===
  * //2019-10-04 - by Mike Blumenkrantz//

==Fixes:==

   * ecore/signal: also use nonblock for writing side of signal pipe
   * ector engine: +null checking.
   * ecore-x: fix null derefs in window prop code
 
==Download:==

^ ** LINK ** ^ ** SHA256 ** ^
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.22.6.tar.xz  | efl-1.22.6.tar.xz ]] | 5f282b88e95f77b453de5ff4e4c5a86482a29397e01f284419a9c427e52faa06 |