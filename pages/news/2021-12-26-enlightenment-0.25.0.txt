=== Enlightenment 0.25.0 Release ===
  * //2021-12-16 - by Carsten Haitzler//

This is the latest release of Enlightenment. A lot has happened since
the last release.

Hilights:
      * Lots of minor bug fixes (see git log)
      * Flat look to match new flat theme
      * New much higher quality default wallpaper set
      * Optimized XKB keymap change handling
      * Init splash is now always on (theme can remove it if desired)
      * Added screen setup menu entry in quick settings menu
      * Redid a lot of screen dimming/blanking/timeout code to fix cruft
      * IBox iconify gadget has window previews on mouse-over now
      * New gesture recognition bindings for touchpads (using Elput)
      * On loss or re-plug in of screen now restore windows sensibly
      * Tasks has window preview on mouse-over now
      * FPS debug (ctl+alt+shift+f to toggle) has a lot more data now
      * BT devices in the Bluez gadget will show battery level if supported
      * Battery now has mouse-over popup with detailed battery information
      * Config saving moved to threads to minimize I/O stutter to core
      * Shot allows copy of screenshot to copy and paste selection too now
      * Temperature monitor now uses hwmon devices and multiple instances
      * New Procstats module can show mem/CPU usage in titlebar
      * Offline/presentation mode removed - use bindings + actions instead
      * Remove custom desklock command - built in is more reliable
      * Winlist (alt+tab) has large mode with window previews now
      * Pluggable device monitoring and applying of input config on plug
      * Added workaround broken Spotify cover art URLs
      * Scale settings will now modify xsettings DPI to make non-efl apps scale
      * Fonts now made to be consistent with EFL and other apps
      * Added binding action to switch profiles
      * EFM - sync more often when doing I/O ops like copy etc.
      * XDG_CURRENT_DESKTOP is now set too
      * EFM image preview will show EFIX dates for when image taken
      * Power plug/unplug now emit signals which theme can make sounds for
      * Mixer - have simple device to icon database text file to look nicer
      * Mixer - add VU meters for output and input audio
      * Mixer - Show icons for apps currently playing sounds and recording
      * Set _NET_WM_STATE_HIDDEN when windows are hidden to avoid rendering
      * Reduce power usage when screen is blanked by setting wins to iconic
      * Added "grow window in direction" actions you can bind
      * Added palette editor and selector tool to set up custom colors
      * Fingerprint (libFprint) support in desklock and new tool for settings
      * EFM - add recent files menu to access recently opened files
      * Force stdout/error log now to ~/.e-log.log
      * Added settings for animation multiplier to speed up/down transitions
      * No more edge bindings by default

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.25.0.tar.xz | enlightenment-0.25.0.tar.xz ]] | 7d6e72ceed5aca135b7a49c2a1c1eb9d8fde1318613517401d0418e0e5f6df06 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README | EFL]]
  - libexif
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the everything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.25.0 depends on EFL **v1.26.0** or newer.

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
