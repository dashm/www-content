=== EFL 1.24.0 beta 2 ===
  * //2020-04-15 - by Stefan Schmidt//

Beta 2 is out for our upcoming 1.24 release. If you have bugs that are not addressed now, its about time to let us know.

**Fixes:**
   * elm_cnp: fix memory leaks

**Download**

|LINK|	SHA256|
| http://download.enlightenment.org/rel/libs/efl/efl-1.24.0-beta2.tar.xz | 25f82e1f2da6a5b86973ab37370477ec6964a3b57adc5c74929977b8b4d6fccd |


{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
