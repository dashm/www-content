=== Enlightenment Foundation Libraries 1.26.2 Release ===
  * //2022-02-07 - by Carsten Haitzler//

This is a bug-fix release.

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.26.2.tar.xz | efl-1.26.2.tar.xz ]] | 2979cfbc728a1a1f72ad86c2467d861ed91e664d3f17ef03190fb5c5f405301c |

----

== What's New ==

  * Fix crash when doing wierd combinations of eet_write and eet_read
  * Fix elementary tests to pass
  * Fix portability of putenv/setenv for niche OS's
  * Fix ecore input to build on BSD
  * Fix svg decoding crashes for vector decode
  * Fix theme - ibar label overlay
  * Fix theme - pointer scaling and sizing
  * Fix ecore x - ensure pointer on screen when setting bounds

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
