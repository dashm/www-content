=== Enlightenment DR 0.24.0 Release ===
  * //2020-05-17 - by Carsten Haitzler//

{{ :e-flat-sample.jpg?640&direct | Sample cropped screenshot of Enlightenment }}

Hilights:
      * New and improved shot module with editor and cropper
      * Reduced number of setuid tools (merged many into single system tool)
      * External monitor backlight and brightness controls via (lib)ddctil
      * Improved resolution of EFM thumbnails to 256x256 by default
      * New and improved crash handling guru meditation
      * Restarts are now seamless with fade in and out and zero glitches
      * Wallpaper import generates multiple resolutions for better efficiency
      * Regularly malloc_trim if available to keep mem down
      * All restarts are now handled by enlightenment_start, not e itself
      * Enforce pointer lock to screen in X to stop pointer out-of-bounds
      * Pager plain is gone - use the regular "miniature preview" pager
      * Music control auto-runs your selected media player if not there
      * Handle exception for steam games to find the right desktop file
      * Polkit auth agent support as new core module - no extra daemons
      * Drop comp fast effects - Should be edje transition factor + theme tags
      * Easier config of specific desktop wallpaper straight from pager
      * Startup should be smoother with IO prefetch thread
      * New special blanking timeout for when locked that can be shorter
      * Bluez4 gone now as Bluez5 is done and working fine
      * Down to zero outstanding coverity issues
      * The usual batches of bug fixes and minor improvements

== Download ==

^ ** LINK ** ^ ** SHA256 ** ^
| [[ http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.24.0.tar.xz | Enlightenment DR 0.24.0 XZ]]  | e7bb0d6b2a9661bd0b2ca15c6bca20ec654a01aff8865f9f594efada3a1c0504 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README                     | EFL]]
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the evrything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.24.0 depends on EFL **v1.24.1** or newer.

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
