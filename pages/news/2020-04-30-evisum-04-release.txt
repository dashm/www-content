=== Evisum 0.4 Release ===
  * //2020-04-30 - by Alastair Poole//

Hi all,

With the downtime due to this global pandemic there has been some time to work on Evisum. I’ve simplified some of the “tabs” as I want them to display well in our default theme and derivatives. When we move to flat, I’ll hope to make them prettier. Also Vincent Torri is working on a graph API I’m hoping to use.

== Some of the “highlights” ==

  * Use a generic list for process listing (pretty).
  * Add process menu.
  * Set application priority (nice).
  * UI improvements.
  * Offer kvm_openfiles means of obtaining PID list on FreeBSD-based platforms.
  * Save settings upon exit. Sorting and window size.
  * Reduce object creation and deletion.
  * Improve display of memory and storage sizes.
  * Use object caching where applicable.
  * Remove redundant code.
  * FreeBSD et al. Get process arguments akin to Linux.
  * macOS improvements for process information.
  * Process command-line (new field).
  * Added translation support (with French and Italian).
  * Other stuff.

== Screenshot ==
{{news:evisum-04.jpg?240|}}

== Download ==

https://download.enlightenment.org/rel/apps/evisum/evisum-0.4.0.tar.xz

Eventually the aim will be to provide E with a means of recording system statistics over time on all platforms we support. That’s not quite now though.

As usual supporting Linux, macOS, FreeBSD (and derivatives) and OpenBSD.

Thanks to Peter2121 for relentless testing and bug-finding and ApBBB for his daily complaints :D

Hope this message greets you all well.

Alastair (netstar)

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~

