=== Enlightenment Foundation Libraries 1.25 Release ===
  * //2020-09-22 - by Stefan Schmidt//

In comparison 1.25 was a very calm release cycle. In the last five months we saw 567 commits from 38 contributors:

Carsten Haitzler, Stefan Schmidt, Marcel Hollerbach, JunsuChoi, Vincent Torri, Ali Alzyod, Daniel Kolesa, Mike Blumenkrantz, Elyes HAOUAS, Hermet Park, Christopher Michael, Xavi Artigas, Subhransu Mohanty, Shinwoo Kim, Taehyub Kim, Woochanlee, Myoungwoon Roy, Kim, Alastair Poole, Jaehyun Cho, Wonki Kim, João Paulo Taylor Ienczak Zanette, AbdullehGhujeh, Wander Lairson Costa, WooHyun Jung, Yeongjong Lee, Bowon Ryu, Hosang Kim, Ali, Boris Faure, Felipe Magno de Almeida, SangHyeon Jade Lee, Youngbok Shin, Joao Antonio Cardoso, Lucas, Lucas Cavalcante de Sousa, ali, ali198724, nerdopolis


== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.25.0.tar.xz  | efl-1.25.0.tar.xz ]] | 53941f910daf5d1b5162bfdb0dc66f1a04e55a0f2dd7760da0ad63713370956e |

----

== What's New ==

This time around we do not have bigger changes to cover here. For the full list please look at the NEWS file, if you are interested in the details. As usual we have been working on fixing bugs, optimising our code for speed and memory footprint and adding new features.


----

== Statistics ==

(git log --pretty=oneline v1.24.0..v1.25.0 | wc -l) \\
Number of commits in 1.25: 567 \\
Number of commits in 1.24: 1885 \\

(git shortlog -ns v1.24.0..v1.25.0 | wc -l) \\
Number of authors in 1.25: 38 \\
Number of authors in 1.24: 54 \\

(git diff --stat v1.24.0..v1.25.0 | tail -1) \\
1020 files changed, 12644 insertions(+), 10471 deletions(-) in 1.25 \\
2073 files changed, 88305 insertions(+), 152470 deletions(-) in 1.24 \\

----

== Building and Dependencies ==

https://git.enlightenment.org/core/efl.git/tree/INSTALL

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
