=== Enlightenment 0.26.0 Release ===
  * //2023-12-23 - by Carsten Haitzler//

This is the latest release of Enlightenment. This has a lot of fixes
mostly with some new features.

Hilights:
      * Lots of minor bug fixes (see git log)
      * Add DDC option to backlight settings
      * Bigger task previews
      * Added watermark to wl mode so you know E is in experimental wl mode
      * Support new eet disk sync API to ensure config it stored
      * Support action desktop files in EFM to add file actions
      * Added org.freedesktop.ScreenSaver inhibit support
      * Added support for logind's lock/unlock dbus apis
      * Added and enabled watchdog thread by default to detect mainloop hangs
      * Added API to play sound samples and support in notifications
      * Add option to Randr X11 support to use xrandr cmdline not direct API
      * Be agressive about forcing E's blank settings if apps override them
      * Add params to mixer actions to allow to set amount up/down
      * Added option for input settings for flat accel + hires scrolling
      * Added option to set hidden state in netwm or not

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.26.0.tar.xz | enlightenment-0.26.0.tar.xz ]] | 11b6ef0671be5fead688bf554c30a2a1c683493ad10c5fe3115ffb4655424e84 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README | EFL]]
  - libexif
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the everything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.26.0 depends on EFL **v1.27.0** or newer.

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
