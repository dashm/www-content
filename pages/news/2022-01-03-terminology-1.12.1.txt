
=== Terminology 1.12.1 release ===
  * //2022-01-03 - by Boris Faure//

> “Oops” ― Boris Faure

Yesterday's release was not done correctly.  This is now fixed, as seen in the
change log below:

== Fixes ==
    * Build and install Default colorscheme
    * Correctly set the version


== Download ==
^ ** LINK ** ^ ** SHA256 ** ^
| [[ https://download.enlightenment.org/rel/apps/terminology/terminology-1.12.1.tar.xz | Terminology 1.12.1 XZ]]  | ''f8ced9584c2e9ae87452ce7425fd25b2d3e122c7489785d2917890215c6b5aa9'' |


{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~