=== Rage Video and Media Player 0.4.0 Release ===
  * //2021-12-26 - by Raster//

This is a new release of [[/about-rage|Rage]] 0.4.0.

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/apps/rage/rage-0.4.0.tar.xz | rage-0.4.0.tar.xz ]] | 7ce58419aa5197aa6c33f2e3f9eb9d78ff379cae863d5fa114fd1428d5a1ca0f |

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~

