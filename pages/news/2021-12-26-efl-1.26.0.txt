=== Enlightenment Foundation Libraries 1.26 Release ===
  * //2021-12-26 - by Carsten Haitzler//

After a while since the last release of efl, this one has a lot of
changes which are also easily visible to users. The biggest change is
the new flat theme which is re-colorable with user editable palettes.

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.26.0.tar.xz | efl-1.26.0.tar.xz ]] | a4a9bce45fd27f8541874e44a130f64550bee1f2f72feaa6c8a758d92eaf204c |

----

== What's New ==

      * Lots of minor bug fixes (too many to summarize here - see git log)
      * Evas GL dithers by default for better quality (but it's a bit slower)
            * To disable: export EVAS_GL_RENDER_DISABLE_DITHER=1
      * Evas HEIF format loaded (need to remove from disabled set)
      * Default theme is now the flat theme
      * New color palette files and API
      * Improved speed of Entry appending
      * Improved performance by not rendering invisible windows

----

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
