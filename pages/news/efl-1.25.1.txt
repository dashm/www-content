=== EFL 1.25.1 release ===
  * //2020-10-08 - by Stefan Schmidt//

First stable update release for our new 1.25.x series has landed.

Fixes:
   * evas: sw font draw - protect against null pointer access
   * efreet - windows - still call stat but skip lstat/readlink

==Download:==

^ ** LINK ** ^ ** SHA256 ** ^
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.25.1.tar.xz  | efl-1.25.1.tar.xz ]] | 351ca0211ca000234527a503585f039f985607ec9439e34b49d8b8bbf35a7e6b |

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~