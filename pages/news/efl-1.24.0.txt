=== Enlightenment Foundation Libraries 1.24 Release ===
  * //2020-04-29 - by Stefan Schmidt//

After eight months of development work we are proud to announce the release of
version 1.24 of the Enlightenment Foundation Libraries. In these eight months we got almost 1900 commits from 54 authors. Great job everyone!
 
Alastair Poole, Ali, Ali Alzyod, Bartlomiej Grzelewski, Bo Anderson, Boris Faure, Bowon Ryu,
Bruno da Silva Belo, Carsten Haitzler, Cedric BAIL, Christopher Michael, ChunEon Park,
Daniel Kolesa, Daniel Zaoui, Davide Andreoli, Felipe Magno de Almeida, Hermet Park,
Hosang Kim, Jaehyun Cho, Jihoon Kim, Jongmin Lee, João Paulo Taylor Ienczak Zanette,
JunsuChoi, Jérémy Zurcher, Lauro Moura, Lucas Cavalcante de Sousa, Marcel Hollerbach,
Massimo Maiurana, Mateusz Denys, Mike Blumenkrantz, Myoungwoon Roy, Kim, Proht,
ProhtMeyhet, Ross Vandegrift, SangHyeon Jade Lee, Shilpa Singh, Shinwoo Kim,
Simon Tischer, Stefan Schmidt, Subodh Kumar, Taehyub Kim, Tom Callaway, Vincent Torri,
Wonki Kim, WooHyun Jung, Woochanlee, Xavi Artigas, Yeongjong Lee, YoungBok Shin,
a.srour, abdulleh Ghujeh, ali, rafspiny, thierry1970,

== Download ==

| LINK | SHA256 |
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.24.0.tar.xz  | efl-1.24.0.tar.xz ]] | b3b96e443c5e36a93e5fe443304ecb661a4707ef921385bf7d9ff24c9e980cfa |

----

== What's New ==

We only cover some of the bigger changes here. For the full list please look at the NEWS file,
if you are interested in the details. As usual we have been working on fixing bugs,
optimising our code for speed and memory footprint and adding new features.

==  EO memory optimization ==
EO is our object abstraction. The object abstraction stores the implemented functions in something called [[ https://en.wikipedia.org/wiki/Virtual_method_table | vtable ]]. These vtables are one of the most memory intensive parts of efl. Before this release, every object was capable of storing every function which was defined until the point of the declaration of the class. This was a little bit wasteful. With the new approach, the vtables only contain a little bit more than required to store the functions required. Optimizing this memory layout is a small degree between performance and memory usage. With this new way we are using 200Kb less memory than before.
Details can be found [[ https://sourceforge.net/p/enlightenment/mailman/message/36926746/ | here ]].

== Copy and Paste rework ==
The old implementation has been located in elementary. For the new implementation all this has been split up into 3 different layers, eina, ecore-evas, and elementary.
In eina we now have a container that abstracts a slice of bytes with a mimetype, these containers can be transformed to different types (as long as there is a conversion method).
Since Copy & Paste and Drag & Drop is mostly about display protocol we have a huge amount of platform depending protocol code, this code is placed in the ecore evas modules of the corresponding platform.
In elementary we have the code around to glue the ecore evas abstraction to elm code. As an example for what this code does: When ecore-evas notifies you that the selection of something has changed, this event needs to be translated to the widget that have registered a event for it. Also, when there is a mouse over while doing a dragging operation, these move events have to be delivered to the widgets that have asked for them.

== Ecore_Wl2 work ==
In this release, the Ecore_Wl2 library has gone through an extensive stabilization phase. The API has been fully documented, and test cases have been provided to verify functionality. In addition, some API functions have been removed while others have been added. A brief summary of the API additions is listed below:

  * Added function to find a window by a given surface
  * Added function to find the connected display by name
  * Added function to get the compositor object from a given display
  * Added functions to get various window properties such as Aspect, Title, Class, Role, Type, Opaque region, Input region
  * Added function to get the default input of a given display
  * Added function to set the keyboard repeat rate
  * Added function to set the mouse cursor based on cursor name

Although this release has seen many changes to the Ecore_Wl2 library, it should be noted that it is still considered to be in Beta and is subject to change in future releases.

== Documentation work ==

As usual, continuous improvement of the current documentation and addition of a few missing bits.

== Removals ==
In this release cycle we finally cleaned out some corners of our libraries which have been in need for it.
We de-duplicated some media backend support and removed beta libraries which never came beyond this stage and libraries which are depending on deprecated or now unsupported components.

  * emotion & evas: remove gstreamer 0.10 support (gstreamer 1.x support is stable)
  * escape: remove library from tree (no known reamaining user of EFL on PS3 OS)
  * ecore_x: remove support XGesture extension (deprecated, distros do no longer ship it)
  * elocation: remove beta library (never out of beta)
  * eeze: remove tizen module support from eeze sensor (unused, sensor API stays)
  * evas3d: remove support for evas 3D which was a beta all along (never out of beta)
  * Xine and vlc supportin emotion (gstreamer 1.x support is stable)

== Known ABI break ==
During the ongoing APi documentation effort a symbol was found which made us all scratch our head for a while.
eina_prime_table was an external public symbol which contained a *few* prime numbers, not the first few though and only 21 in total.
We had no internal use of it and a very hard time to come up with any valid use case at all.
If you are hit by this break as your application uses this symbol come and talk to us. We are willing to revert if there could really be a use case of this table.

----

== Statistics ==

(git log --pretty=oneline v1.23.0..v1.24.0 | wc -l) \\
Number of commits in 1.24: 1885 \\
Number of commits in 1.23: 2110 \\

(git shortlog -ns v1.23.0..v1.24.0 | wc -l) \\
Number of authors in 1.24: 54 \\
Number of authors in 1.23: 48 \\

(git diff --stat v1.23.0..v1.24.0 | tail -1) \\
2073 files changed, 88305 insertions(+), 152470 deletions(-) in 1.24 \\
2296 files changed, 90115 insertions(+), 85554 deletions(-) in 1.23 \\

----

== Building and Dependencies ==

https://git.enlightenment.org/core/efl.git/tree/INSTALL

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~

