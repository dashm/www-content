=== Enlightenment DR 0.24.2 Release ===
  * //2020-07-26 - by Carsten Haitzler//

{{ :e-flat-sample.jpg?640&direct | Sample cropped screenshot of Enlightenment }}

Hilights:
      * Fixed null exec command running
      * Fixed handling of screen with no edid
      * Fixed signal.h, execinfo.h include for some libc's
      * Sped up pager thumb loading significant;y
      * Handle zone changes during startup
      * Longer efreetd timeout for update event
      * Fixed e's xsettings support on 64bit to use the right types
      * Fixed preloading of icon
      * Fixed e's wl backlight/dimming logic to be correct and reliable
      * Fixed noisy logging on invalid exec handle found
      * Fixed shot blurriness if size didn't quite match
      * Fixed battery on openbsd to use right constants
      * Fixed cpu freq setting on freebsd etc.
      * Handle error exits from dependency libs as an error and bring up alert
      * E system - isolate stdio so it doesn't affect ipc on stdin/out
      * Polkit & Askpass password - handle escaping right
      * Fix tiling issue with float/unfloat
      * Fixed shell autohide when clock data up
      * Handle x io error with proper exit code and exit

== Download ==

^ ** LINK ** ^ ** SHA256 ** ^
| [[
http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.24.2.tar.xz | Enlightenment DR 0.24.2 XZ]]  | be18e2f18d6c0b058f633e769863d3cbc4c07b629058ae670dec74cd7906dff1 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README                     | EFL]]
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the evrything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.24.2 depends on EFL **v1.24.1** or newer.

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
