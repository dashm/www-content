=== EFL 1.24.4 release ===
  * //2020-09-28 - by Stefan Schmidt//

We are happy to release our last stable update for our 1.24.x series. With the new 1.25.x series started we we are doing this last maintenance release before closing down this series.

Fixes:
   * efreetd - cache - add more statinfo work around 0 mtime distros
   * edje - fix edje_cc mis-re-iding images in image sets
   * evas - render - dont crash is private data is null
   * eina vpath - fix windows ~username handling to only error when used
   * efl ui image - fix scal to fill - broken with non-rect src images
   * edje - calce - really respect max image size for img sets
   * evas - fix deadlock with sw async rendering calling cbs in post flush
   * ecore - don't do anything with heap between fork and exec

==Download:==

^ ** LINK ** ^ ** SHA256 ** ^
| [[http://download.enlightenment.org/rel/libs/efl/efl-1.24.4.tar.xz  | efl-1.24.4.tar.xz ]] | 617e6b29d2478259acac72e2867d1908249c01a323ff3284948e999d4787f901 |

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~